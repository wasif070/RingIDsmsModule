package module.ringid.voice.scheduler;

import module.ringid.sms.messages.MessagesDTO;
import module.ringid.sms.utils.MyAppError;
import module.ringid.voice.db.VoiceDAO;

public class VoiceTaskScheduler {

    private static VoiceTaskScheduler scheduler;

    public static VoiceTaskScheduler getInstance() {
        if (scheduler == null) {
            scheduler = new VoiceTaskScheduler();
        }
        return scheduler;
    }

    public MyAppError addVoiceLog(MessagesDTO messagesDTO) {
        VoiceDAO voiceDAO = new VoiceDAO();
        return voiceDAO.addMessage(messagesDTO);
    }

    //Called when a message is verified by voice
    public MyAppError updateSuccessfulVoiceVerificationStatus(MessagesDTO dto) {
        VoiceDAO voiceDAO = new VoiceDAO();
        return voiceDAO.updateSuccessfulVoiceVerificationStatus(dto);
    }
}
