package module.ringid.voice.db;

import module.ringid.sms.messages.*;
import java.sql.SQLException;
import java.text.ParseException;
import org.apache.log4j.Logger;
import module.ringid.sms.BaseDAO;
import module.ringid.sms.smsBrands.BrandsTaskScheduler;
import module.ringid.sms.utils.MyAppError;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import module.ringid.sms.country.CountryTaskScheduler;
import module.ringid.sms.dto.SearchDTO;
import module.ringid.sms.dto.SummaryDTO;
import module.ringid.sms.utils.Constants;

public class VoiceDAO extends BaseDAO {

    static Logger logger = Logger.getLogger("com.ringid.voice");

    public MyAppError addMessage(MessagesDTO messagesDTO) {
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            String query = "select * from voice where sendDate='" + messagesDTO.getDate() + "' and responseId='" + messagesDTO.getResponseId() + "' AND brandId=" + messagesDTO.getBrandId();
            stmt = db.connection.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                error.setERROR_TYPE(MyAppError.OTHERERROR);
                error.setErrorMessage("Duplicate content!");
                return error;
            }

            query = "INSERT INTO voice(brandId,mobileNo,message,brandResponse,smsStatus,sendDate,vCode,verifiedSend,responseId,countryId,delivaryStatus) VALUES(?,?,?,?,?,?,?,?,?,?,?);";
            ps = db.connection.prepareStatement(query);
            int index = 1;

            ps.setInt(index++, messagesDTO.getBrandId());
            ps.setString(index++, messagesDTO.getMobileNo());
            ps.setString(index++, messagesDTO.getMessage());
            ps.setString(index++, messagesDTO.getBrandResponse());
            ps.setString(index++, messagesDTO.getSmsStatus());
            ps.setString(index++, messagesDTO.getDate());
            ps.setString(index++, messagesDTO.getVerificationCode());
            ps.setInt(index++, messagesDTO.getVerifiedSend());
            ps.setString(index++, messagesDTO.getResponseId());
            ps.setInt(index++, messagesDTO.getCountryId());
            ps.setInt(index++, messagesDTO.getDlStatus());

            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.error("error in MessageDAO" + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.error("error in MessageDAO" + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError updateSuccessfulVoiceVerificationStatus(MessagesDTO dto) {
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE voice SET verifiedSend = 1,responseDate=? WHERE mobileNo =? AND vCode =? AND verifiedSend=0";

            ps = db.connection.prepareStatement(query);
            ps.setString(1, dto.getReceivedDate());
            ps.setString(2, dto.getMobileNo());
            ps.setString(3, dto.getVerificationCode());

            ps.executeUpdate();
            logger.debug("verifiedSend field updated successfully for --> " + dto.getMobileNo());
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("varified update failed" + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("verified update failed" + e);
        } finally {
            close();
        }

        return error;
    }

    public MyAppError updateDeliverySMSStatus(MessagesDTO mdto) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE voice SET delivaryStatus = ?, responseDate = ? WHERE responseId =? AND delivaryStatus=0 AND brandId = ?";

            logger.debug("rcvDate --> " + mdto.getReceivedDate() + " --> getResponseId --> " + mdto.getResponseId() + " --> getBrandId --> " + mdto.getBrandId());
            ps = db.connection.prepareStatement(query);
            ps.setInt(1, mdto.getDlStatus());
            ps.setString(2, mdto.getReceivedDate() != null ? mdto.getReceivedDate() : sdf.format(new Date()));
            ps.setString(3, mdto.getResponseId());
            ps.setInt(4, mdto.getBrandId());

            int num = ps.executeUpdate();
            logger.debug("updateDeliverySMSStatus --> update success --> " + num);
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.error("varified update failed" + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.error("varified update failed" + e);
        } finally {
            close();
        }
        return error;
    }

    public String verificationCode(String mobileNo) {
        String vCode = "";
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT vCode FROM voice WHERE mobileNo='" + mobileNo + "' ORDER BY sendDate DESC LIMIT 1;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                try {
                    vCode = rs.getString("vCode");
                } catch (NullPointerException e) {
                    logger.error("execption in MessageDAO of getMessageList " + e);
                } catch (SQLException e) {
                    logger.error("execption in MessageDAO of getMessageList sqlException " + e);
                } catch (Exception e) {
                    logger.error("execption in MessageDAO of getMessageList -->  " + e);
                }
            }
        } catch (Exception e) {
            logger.error("execption in MessageDAO of getMessageList " + e);
        } finally {
            close();
        }
        return vCode;
    }

    MessagesDTO getMessage(Integer brandId, String msgId) {
        MessagesDTO messageDTO = null;
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT * FROM voice WHERE responseId='" + msgId + "' and brandId = " + brandId + " ORDER BY id DESC LIMIT 1;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                try {
                    messageDTO = new MessagesDTO();
                    messageDTO.setId(rs.getInt("id"));
                    messageDTO.setBrandId(rs.getInt("brandId"));
                    messageDTO.setRoutesName(BrandsTaskScheduler.getInstance().getBrandNameById(rs.getInt("brandId")));
                    messageDTO.setMobileNo(rs.getString("mobileNo"));
                    messageDTO.setSms(rs.getString("message"));
                    messageDTO.setBrandResponse(rs.getString("brandResponse"));
                    messageDTO.setResponseId(rs.getString("responseId"));
                    messageDTO.setSmsStatus(rs.getString("smsStatus"));
                    messageDTO.setDate(rs.getString("sendDate"));
                    messageDTO.setVerifiedSend(rs.getInt("verifiedSend"));
                    messageDTO.setDlStatus(rs.getInt("delivaryStatus"));
                    messageDTO.setCountryId(rs.getInt("countryId"));
                } catch (NullPointerException e) {
                    logger.error("execption in MessageDAO of getMessage " + e);
                } catch (SQLException e) {
                    logger.error("execption in MessageDAO of getMessage sqlException " + e);
                } catch (Exception e) {
                    logger.error("execption in MessageDAO of getMessage -->  " + e);
                }
            }
        } catch (Exception e) {
            logger.error("execption in MessageDAO of getMessageList " + e);
        } finally {
            close();
        }
        return messageDTO;
    }

    public boolean isDelivered(String responseIdCommaSeperated) {
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id  FROM voice WHERE responseId in (" + responseIdCommaSeperated + ") AND delivaryStatus=1";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            logger.error("execption in MessageDAO of getMessageList " + e);
        } finally {
            close();
        }
        return false;
    }

    private String getClause(String optionalSql) {
        return (optionalSql.length() < 1 ? " WHERE " : " AND ");
    }

    public Integer getMessagesCount(SearchDTO searchDTO) {
        Integer total = 0;
        try {
            String sql;
            String firstPartOfSql = "SELECT count(1) as total  FROM voice ";
            String optionalSql = "";

            if (searchDTO.isSearchByRoutes()) {
                optionalSql += getClause(optionalSql) + " brandId = " + searchDTO.getBrandId();
            }

            if (searchDTO.isSearchByStatus()) {
                optionalSql += getClause(optionalSql) + " smsStatus = " + searchDTO.getSmsStatus();
            }

            if (searchDTO.isSearchByCountryId()) {
                optionalSql += getClause(optionalSql) + " countryId = " + searchDTO.getCountryId();
            }

            if (searchDTO.isFailed()) {
                optionalSql += getClause(optionalSql) + " delivaryStatus < 1 AND verifiedSend < 1 ";
            }

            optionalSql = prepareDateLimitation(optionalSql, searchDTO);
            if (searchDTO.isSuccess()) {
                firstPartOfSql = "SELECT id  FROM voice ";
//                sql = "SELECT count(1) as total FROM ((" + firstPartOfSql + optionalSql + getClause(optionalSql) + " delivaryStatus = 1 ) UNION "
//                        + "(" + firstPartOfSql + optionalSql + getClause(optionalSql) + " verifiedSend = 1 )) t ";

                switch (searchDTO.getType()) {
                    case Constants.VERIFIED:
                        sql = "SELECT count(1) as total FROM voice " + optionalSql + getClause(optionalSql) + " verifiedSend = 1";
                        break;
                    case Constants.DELIVERED:
                        sql = "SELECT count(1) as total FROM voice " + optionalSql + getClause(optionalSql) + " delivaryStatus = 1";
                        break;
                    case Constants.BOTH:
                        sql = "SELECT count(1) as total FROM ((" + firstPartOfSql + optionalSql + getClause(optionalSql) + " delivaryStatus = 1 ) UNION "
                                + "(" + firstPartOfSql + optionalSql + getClause(optionalSql) + " verifiedSend = 1 )) t ";
                        break;
                    default:
                        sql = "SELECT count(1) as total FROM ((" + firstPartOfSql + optionalSql + getClause(optionalSql) + " delivaryStatus = 1 ) UNION "
                                + "(" + firstPartOfSql + optionalSql + getClause(optionalSql) + " verifiedSend = 1 )) t ";
                        break;
                }
            } else {
                sql = firstPartOfSql + optionalSql;
            }

            logger.debug("getMessagesCount : SQL [SUCS: " + searchDTO.isSuccess() + " --> FAILED: " + searchDTO.isFailed() + "] --> " + sql);

            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            rs = stmt.executeQuery(sql);
            if (searchDTO.isSuccess()) {
                while (rs.next()) {
                    total += rs.getInt("total");
                }
            } else {
                if (rs.next()) {
                    total = rs.getInt("total");
                }
            }
        } catch (Exception e) {
            logger.error("execption in MessageDAO of getMessageList " + e);
            return null;
        } finally {
            close();
        }
        return total;
    }

    public ArrayList<MessagesDTO> getMessagesList(long start, int limit, SearchDTO searchDTO) {
//        HashMap<Integer, MessagesDTO> messageListByDateMap = new HashMap<>();
        ArrayList<MessagesDTO> list = new ArrayList<>();
        try {
            String sql;
            String firstPartOfSql = "SELECT id,brandId,mobileNo,message,responseId,brandResponse,smsStatus,sendDate,verifiedSend,delivaryStatus,countryId  FROM voice ";
            String optionalSql = "";
            String lastPartOfSql = " order by id desc limit " + start + "," + limit;

            if (searchDTO.isSearchByRoutes()) {
                optionalSql += getClause(optionalSql) + " brandId = " + searchDTO.getBrandId();
            }

            if (searchDTO.isSearchByStatus()) {
                optionalSql += getClause(optionalSql) + " smsStatus = " + searchDTO.getSmsStatus();
            }

            if (searchDTO.isSearchByCountryId()) {
                optionalSql += getClause(optionalSql) + " countryId = " + searchDTO.getCountryId();
            }

            /*if (searchDTO.isSuccess()) {
             optionalSql += getClause(optionalSql) + " delivaryStatus = 1 OR verifiedSend = 1 ";
             } else 
             */
            if (searchDTO.isFailed()) {
                optionalSql += getClause(optionalSql) + " delivaryStatus < 1 AND verifiedSend < 1 ";
            }

            optionalSql = prepareDateLimitation(optionalSql, searchDTO);
            if (searchDTO.isSuccess()) {
//                sql = "SELECT * FROM ((" + firstPartOfSql + optionalSql + getClause(optionalSql) + " delivaryStatus = 1 ) UNION "
//                        + "(" + firstPartOfSql + optionalSql + getClause(optionalSql) + " verifiedSend = 1 )) t " + lastPartOfSql;

                switch (searchDTO.getType()) {
                    case Constants.VERIFIED:
                        sql = firstPartOfSql + optionalSql + getClause(optionalSql) + " verifiedSend = 1  " + lastPartOfSql;
                        break;
                    case Constants.DELIVERED:
                        sql = firstPartOfSql + optionalSql + getClause(optionalSql) + " delivaryStatus = 1  " + lastPartOfSql;
                        break;
                    case Constants.BOTH:
                        sql = "SELECT * FROM ((" + firstPartOfSql + optionalSql + getClause(optionalSql) + " delivaryStatus = 1 ) UNION "
                                + "(" + firstPartOfSql + optionalSql + getClause(optionalSql) + " verifiedSend = 1 )) t " + lastPartOfSql;
                        break;
                    default:
                        sql = "SELECT * FROM ((" + firstPartOfSql + optionalSql + getClause(optionalSql) + " delivaryStatus = 1 ) UNION "
                                + "(" + firstPartOfSql + optionalSql + getClause(optionalSql) + " verifiedSend = 1 )) t " + lastPartOfSql;
                        break;
                }
            } else {
                sql = firstPartOfSql + optionalSql + lastPartOfSql;
            }
            logger.debug("getMessagesList : SQL [SUCS: " + searchDTO.isSuccess() + " --> FAILED: " + searchDTO.isFailed() + "] --> " + sql);

            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                try {
                    MessagesDTO messageDTO = new MessagesDTO();
                    messageDTO.setId(rs.getInt("id"));
                    messageDTO.setBrandId(rs.getInt("brandId"));
                    messageDTO.setRoutesName(BrandsTaskScheduler.getInstance().getBrandNameById(rs.getInt("brandId")));
                    messageDTO.setMobileNo(rs.getString("mobileNo"));
                    messageDTO.setSms(rs.getString("message"));
                    messageDTO.setBrandResponse(rs.getString("brandResponse"));
                    messageDTO.setMessageId(rs.getString("responseId"));
                    messageDTO.setSmsStatus(rs.getString("smsStatus"));
                    messageDTO.setDate(rs.getString("sendDate"));
                    messageDTO.setVerifiedSend(rs.getInt("verifiedSend"));
                    messageDTO.setDlStatus(rs.getInt("delivaryStatus"));
                    messageDTO.setCountryId(rs.getInt("countryId"));

//                    messageListByDateMap.put(messageDTO.getId(), messageDTO);
                    list.add(messageDTO);
                } catch (NullPointerException e) {
                    logger.error("execption in MessageDAO of getMessageList " + e);

                } catch (Exception e) {
                    logger.error("execption in MessageDAO of getMessageList sqlException " + e);
                }
            }
        } catch (Exception e) {
            logger.error("execption in MessageDAO of getMessageList " + e);
        } finally {
            close();
        }
//        return new ArrayList<>(messageListByDateMap.values());
        return list;
    }

    public ArrayList<SummaryDTO> getNewSummaryList(SearchDTO searchDTO) {
        ArrayList<SummaryDTO> list = new ArrayList<>();
        HashMap<String, HashMap<String, SummaryDTO>> countryMapping = new HashMap<>();

        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String countryOptionalSql = "";
            String routeOptionalSql = "";

            if (searchDTO.isSearchByCountryId()) {
                countryOptionalSql = " and countryId = " + searchDTO.getCountryId();
            }

            if (searchDTO.isSearchByRoutes()) {
                routeOptionalSql = " and brandId = " + searchDTO.getBrandId();
            }

            String dateSpecificOptionalSql = prepareDateLimitation(searchDTO);

            //==== part - 01 =======
            String sql = "select countryId, brandId,count(1) as total_verified from voice where verifiedSend = 1 " + countryOptionalSql + routeOptionalSql + dateSpecificOptionalSql + " group by countryId, brandId;";
            logger.debug("part - 01 --> " + sql);
            rs = stmt.executeQuery(sql);
            String country;
            String route;

            while (rs.next()) {
                country = CountryTaskScheduler.getInstance().getCountryInfoById(rs.getInt("countryId")).getName();
                route = BrandsTaskScheduler.getInstance().getBrandNameById(rs.getInt("brandId"));
                if (countryMapping.containsKey(country)) {
                    HashMap<String, SummaryDTO> brandMappingLocal = countryMapping.get(country);
                    if (brandMappingLocal == null) {
                        SummaryDTO summaryDTO = new SummaryDTO();
                        summaryDTO.setCountryName(country);
                        summaryDTO.setRoutesName(route);
                        summaryDTO.setTotalVerified(rs.getInt("total_verified"));
                        brandMappingLocal = new HashMap<>();
                        brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                        countryMapping.put(country, brandMappingLocal);
                    } else {
                        if (brandMappingLocal.containsKey(route)) {
                            brandMappingLocal.get(route).setTotalVerified(rs.getInt("total_verified"));
                        } else {
                            SummaryDTO summaryDTO = new SummaryDTO();
                            summaryDTO.setCountryName(country);
                            summaryDTO.setRoutesName(route);
                            summaryDTO.setTotalVerified(rs.getInt("total_verified"));
                            brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                        }
                    }
                } else {
                    SummaryDTO summaryDTO = new SummaryDTO();
                    summaryDTO.setCountryName(country);
                    summaryDTO.setRoutesName(route);
                    summaryDTO.setTotalVerified(rs.getInt("total_verified"));

                    HashMap<String, SummaryDTO> brandMappingLocal = new HashMap<>();
                    brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                    countryMapping.put(country, brandMappingLocal);
                }
            }

            //===== part - 02 ======
            sql = "select countryId, brandId,count(1) as total_confirmed_by_operator from voice where delivaryStatus = 1" + countryOptionalSql + routeOptionalSql + dateSpecificOptionalSql + " group by countryId, brandId;";
            logger.debug("part - 02 --> " + sql);

            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                country = CountryTaskScheduler.getInstance().getCountryInfoById(rs.getInt("countryId")).getName();
                route = BrandsTaskScheduler.getInstance().getBrandNameById(rs.getInt("brandId"));
                if (countryMapping.containsKey(country)) {
                    HashMap<String, SummaryDTO> brandMappingLocal = countryMapping.get(country);
                    if (brandMappingLocal == null) {
                        SummaryDTO summaryDTO = new SummaryDTO();
                        summaryDTO.setCountryName(country);
                        summaryDTO.setRoutesName(route);
                        summaryDTO.setTotalConfirmedByOperator(rs.getInt("total_confirmed_by_operator"));
                        brandMappingLocal = new HashMap<>();
                        brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                        countryMapping.put(country, brandMappingLocal);
                    } else {
                        if (brandMappingLocal.containsKey(route)) {
                            brandMappingLocal.get(route).setTotalConfirmedByOperator(rs.getInt("total_confirmed_by_operator"));
                        } else {
                            SummaryDTO summaryDTO = new SummaryDTO();
                            summaryDTO.setCountryName(country);
                            summaryDTO.setRoutesName(route);
                            summaryDTO.setTotalConfirmedByOperator(rs.getInt("total_confirmed_by_operator"));
                            brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                        }
                    }
                } else {
                    SummaryDTO summaryDTO = new SummaryDTO();
                    summaryDTO.setCountryName(country);
                    summaryDTO.setRoutesName(route);
                    summaryDTO.setTotalConfirmedByOperator(rs.getInt("total_confirmed_by_operator"));

                    HashMap<String, SummaryDTO> brandMappingLocal = new HashMap<>();
                    brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                    countryMapping.put(country, brandMappingLocal);
                }
            }

            //===== part - 03 ======
            sql = "select countryId, brandId,count(1) as total_failed_by_operator from voice where delivaryStatus < 1 " + countryOptionalSql + routeOptionalSql + dateSpecificOptionalSql + " group by countryId, brandId;";
            logger.debug("part - 03 --> " + sql);

            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                country = CountryTaskScheduler.getInstance().getCountryInfoById(rs.getInt("countryId")).getName();
                route = BrandsTaskScheduler.getInstance().getBrandNameById(rs.getInt("brandId"));
                if (countryMapping.containsKey(country)) {
                    HashMap<String, SummaryDTO> brandMappingLocal = countryMapping.get(country);
                    if (brandMappingLocal == null) {
                        SummaryDTO summaryDTO = new SummaryDTO();
                        summaryDTO.setCountryName(country);
                        summaryDTO.setRoutesName(route);
                        summaryDTO.setTotalFailedByOperator(rs.getInt("total_failed_by_operator"));
                        brandMappingLocal = new HashMap<>();
                        brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                        countryMapping.put(country, brandMappingLocal);
                    } else {
                        if (brandMappingLocal.containsKey(route)) {
                            brandMappingLocal.get(route).setTotalFailedByOperator(rs.getInt("total_failed_by_operator"));
                        } else {
                            SummaryDTO summaryDTO = new SummaryDTO();
                            summaryDTO.setCountryName(country);
                            summaryDTO.setRoutesName(route);
                            summaryDTO.setTotalFailedByOperator(rs.getInt("total_failed_by_operator"));
                            brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                        }
                    }
                } else {
                    SummaryDTO summaryDTO = new SummaryDTO();
                    summaryDTO.setCountryName(country);
                    summaryDTO.setRoutesName(route);
                    summaryDTO.setTotalFailedByOperator(rs.getInt("total_failed_by_operator"));

                    HashMap<String, SummaryDTO> brandMappingLocal = new HashMap<>();
                    brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                    countryMapping.put(country, brandMappingLocal);
                }
            }

            //===== part - 04 ======
            sql = "select countryId, brandId,count(1) as total_sms from voice where id > -1  " + countryOptionalSql + routeOptionalSql + dateSpecificOptionalSql + " group by countryId, brandId;";
            logger.debug("part - 04 --> " + sql);

            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                country = CountryTaskScheduler.getInstance().getCountryInfoById(rs.getInt("countryId")).getName();
                route = BrandsTaskScheduler.getInstance().getBrandNameById(rs.getInt("brandId"));
                if (countryMapping.containsKey(country)) {
                    HashMap<String, SummaryDTO> brandMappingLocal = countryMapping.get(country);
                    if (brandMappingLocal == null) {
                        SummaryDTO summaryDTO = new SummaryDTO();
                        summaryDTO.setCountryName(country);
                        summaryDTO.setRoutesName(route);
                        summaryDTO.setTotalSms(rs.getInt("total_sms"));
                        brandMappingLocal = new HashMap<>();
                        brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                        countryMapping.put(country, brandMappingLocal);
                    } else {
                        if (brandMappingLocal.containsKey(route)) {
                            brandMappingLocal.get(route).setTotalSms(rs.getInt("total_sms"));
                        } else {
                            SummaryDTO summaryDTO = new SummaryDTO();
                            summaryDTO.setCountryName(country);
                            summaryDTO.setRoutesName(route);
                            summaryDTO.setTotalSms(rs.getInt("total_sms"));
                            brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                        }
                    }
                } else {
                    SummaryDTO summaryDTO = new SummaryDTO();
                    summaryDTO.setCountryName(country);
                    summaryDTO.setRoutesName(route);
                    summaryDTO.setTotalSms(rs.getInt("total_sms"));

                    HashMap<String, SummaryDTO> brandMappingLocal = new HashMap<>();
                    brandMappingLocal.put(summaryDTO.getRoutesName(), summaryDTO);
                    countryMapping.put(country, brandMappingLocal);
                }
            }

            Set set = countryMapping.entrySet();
            Iterator i = set.iterator();
            while (i.hasNext()) {
                Map.Entry me = (Map.Entry) i.next();
                HashMap<String, SummaryDTO> map = (HashMap<String, SummaryDTO>) me.getValue();

                Set internalSet = map.entrySet();
                Iterator internalI = internalSet.iterator();
                while (internalI.hasNext()) {
                    Map.Entry internalMe = (Map.Entry) internalI.next();
                    SummaryDTO summaryDTO = (SummaryDTO) internalMe.getValue();
                    list.add(summaryDTO);
                }

            }

        } catch (Exception e) {
            logger.error("execption in MessageDAO of getMessageList " + e);
            return null;
        } finally {
            close();
        }
        return list;

    }

    private String prepareDateLimitation(SearchDTO searchDTO) throws ParseException {
        String dateSpecificOptionalSql = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        Date maxToDate = cal.getTime();
        String maxToDateStr = sdf.format(maxToDate);
        cal.add(Calendar.DATE, -60);
        Date minFromDate = cal.getTime();
        String minFromDateStr = sdf.format(minFromDate);

        String fromDate, toDate;

        if (!searchDTO.isSerachByFromDate() || sdf.parse(searchDTO.getFromDate()).before(minFromDate)) {
            fromDate = minFromDateStr;
        } else {
            fromDate = searchDTO.getFromDate();
        }
        if (!searchDTO.isSerachByToDate()) {
            toDate = maxToDateStr;
        } else {
            toDate = searchDTO.getToDate();
        }

//        if (!searchDTO.isSerachByFromDate() && !searchDTO.isSerachByToDate()) {
//            dateSpecificOptionalSql = " and DATE(sendDate) BETWEEN '" + minFromDateStr + "' AND '" + maxToDateStr + "'";
//        } else if (searchDTO.isSerachByFromDate() && searchDTO.isSerachByToDate()) {
//            dateSpecificOptionalSql = " and DATE(sendDate) BETWEEN '" + fromDate + "' AND '" + toDate + "'";
//        } else if (searchDTO.isSerachByFromDate()) {
//            dateSpecificOptionalSql = " and DATE(sendDate) >= '" + fromDate + "' ";
//        } else if (searchDTO.isSerachByToDate()) {
//            dateSpecificOptionalSql = " and DATE(sendDate) <= '" + toDate + "'";
//        }
        minFromDateStr = minFromDateStr + " 00:00:00";
        maxToDateStr = maxToDateStr + " 23:59:59";
        fromDate = fromDate + " 00:00:00";
        toDate = toDate + " 23:59:59";
        if (!searchDTO.isSerachByFromDate() && !searchDTO.isSerachByToDate()) {
            dateSpecificOptionalSql = " and sendDate BETWEEN '" + minFromDateStr + "' AND '" + maxToDateStr + "'";
        } else if (searchDTO.isSerachByFromDate() && searchDTO.isSerachByToDate()) {
            dateSpecificOptionalSql = " and sendDate BETWEEN '" + fromDate + "' AND '" + toDate + "'";
        } else if (searchDTO.isSerachByFromDate()) {
            dateSpecificOptionalSql = " and sendDate >= '" + fromDate + "' ";
        } else if (searchDTO.isSerachByToDate()) {
            dateSpecificOptionalSql = " and sendDate <= '" + toDate + "'";
        }

        return dateSpecificOptionalSql;
    }

    private String prepareDateLimitation(String optionalSql, SearchDTO searchDTO) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        Date maxToDate = cal.getTime();
        String maxToDateStr = sdf.format(maxToDate);
        cal.add(Calendar.DATE, -60);
        Date minFromDate = cal.getTime();
        String minFromDateStr = sdf.format(minFromDate);

        String fromDate, toDate;

        if (!searchDTO.isSerachByFromDate() || sdf.parse(searchDTO.getFromDate()).before(minFromDate)) {
            fromDate = minFromDateStr;
        } else {
            fromDate = searchDTO.getFromDate();
        }
        if (!searchDTO.isSerachByToDate()) {
            toDate = maxToDateStr;
        } else {
            toDate = searchDTO.getToDate();
        }

//        if (!searchDTO.isSerachByFromDate() && !searchDTO.isSerachByToDate()) {
//            optionalSql += getClause(optionalSql) + " DATE(sendDate) BETWEEN '" + minFromDateStr + "' AND '" + maxToDateStr + "'";
//        } else if (searchDTO.isSerachByFromDate() && searchDTO.isSerachByToDate()) {
//            optionalSql += getClause(optionalSql) + " DATE(sendDate) BETWEEN '" + fromDate + "' AND '" + toDate + "'";
//        } else if (searchDTO.isSerachByFromDate()) {
//            optionalSql += getClause(optionalSql) + " DATE(sendDate) >= '" + fromDate + "' ";
//        } else if (searchDTO.isSerachByToDate()) {
//            optionalSql += getClause(optionalSql) + " DATE(sendDate) <= '" + toDate + "'";
//        }
        minFromDateStr = minFromDateStr + " 00:00:00";
        maxToDateStr = maxToDateStr + " 23:59:59";
        fromDate = fromDate + " 00:00:00";
        toDate = toDate + " 23:59:59";

        if (!searchDTO.isSerachByFromDate() && !searchDTO.isSerachByToDate()) {
            optionalSql += getClause(optionalSql) + " sendDate BETWEEN '" + minFromDateStr + "' AND '" + maxToDateStr + "'";
        } else if (searchDTO.isSerachByFromDate() && searchDTO.isSerachByToDate()) {
            optionalSql += getClause(optionalSql) + " sendDate BETWEEN '" + fromDate + "' AND '" + toDate + "'";
        } else if (searchDTO.isSerachByFromDate()) {
            optionalSql += getClause(optionalSql) + " sendDate >= '" + fromDate + "' ";
        } else if (searchDTO.isSerachByToDate()) {
            optionalSql += getClause(optionalSql) + " sendDate <= '" + toDate + "'";
        }
        return optionalSql;
    }
}
