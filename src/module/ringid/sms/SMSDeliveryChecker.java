/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package module.ringid.sms;

import org.apache.log4j.Logger;
import module.ringid.sms.messages.MessageScheduler;
import module.ringid.sms.messages.MessagesDTO;
import module.ringid.sms.routes.brands.ClickatellApi;
import module.ringid.sms.routes.brands.InfobipSMS;
import module.ringid.sms.routes.brands.RobiAxiataSMS;
import module.ringid.sms.routes.brands.SMSWaalaApi;
import module.ringid.sms.dto.SmsStatusDTO;
import module.ringid.sms.routes.brands.Wavecell;
import module.ringid.sms.utils.Constants;

/**
 *
 * @author reefat
 */
public class SMSDeliveryChecker extends Thread {

//    ArrayList<Integer> brandIdMap;
    String msgId;
    private static final Logger LOGGER = Logger.getLogger("com.ringid.sms");
//    RoutesController routesController;
    Integer brandId;

    public SMSDeliveryChecker(Integer brandId,String messageId) {
        this.brandId = brandId;
//        this.brandIdMap = brandIdMap;
        this.msgId = messageId;
//        this.routesController = routesController;
    }

    @Override
    public void run() {
        if (msgId.length() > 0) {
            boolean delivered;
            try {
                MessagesDTO mDTO = MessageScheduler.getInstance().getMessage(brandId, msgId);
                if (mDTO == null) {
                    Thread.sleep(5000);
                    mDTO = MessageScheduler.getInstance().getMessage(brandId, msgId);
                }
                delivered = getDeliveryStatus(mDTO);
                if (!delivered) {
                    LOGGER.error("Message sending failed After DeliveryStatus Checking --> brandId --> " + brandId);
                }
            } catch (InterruptedException ex) {
            }
        } else {
            LOGGER.error("MessageId empty");
        }
    }

    private boolean getDeliveryStatus(MessagesDTO msgDTO) throws InterruptedException {
        if (msgDTO == null) {
            LOGGER.error("msgDTO is null!");
            return false;
        }
        int maxTryCount = 3;
        int count = 1;
        boolean deliveryResultReceived = false;
        loop:
        while (count <= maxTryCount && !deliveryResultReceived) {
            Thread.sleep(5000 * count);//6 sec [total 5 + 10 + 15 = 30 sec wait]
            int brandIdOfMsg = msgDTO.getBrandId();
            switch (brandIdOfMsg) {
                case Constants.SMS_WAALA_API:
                    SMSWaalaApi sMSWaalaApi = new SMSWaalaApi(msgDTO.getMobileNo());
                    SmsStatusDTO smsStatusDTO = sMSWaalaApi.getDeliveryStatus(brandIdOfMsg, msgDTO.getResponseId());
                    if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.SUCCESS) {
                        msgDTO.setDlStatus(1);
                        deliveryResultReceived = true;
                        //db update
                        updateDB(msgDTO);

                        LOGGER.error("[Second phase successful]  [Confirmation received from ] brandId  : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                    } else if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.ERROR) {
                        msgDTO.setDlStatus(-1);
                        //db update
                        updateDB(msgDTO);
                        LOGGER.error("[Second phase failed] using brandId --> brandID : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                        break loop;
                    }
                    break;
                case Constants.CLICKATELL_API:
                    ClickatellApi clickatell = new ClickatellApi();
                    smsStatusDTO = clickatell.getDeliveryStatus(brandIdOfMsg, msgDTO.getResponseId());
                    if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.SUCCESS) {
                        msgDTO.setDlStatus(1);
                        deliveryResultReceived = true;
                        //db update
                        updateDB(msgDTO);
                        LOGGER.error("[Second phase successful]  [Confirmation received from ] brandId  : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                    } else if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.ERROR) {
                        msgDTO.setDlStatus(-1);
                        //db update
                        updateDB(msgDTO);
                        LOGGER.error("[Second phase failed] using brandId --> brandID : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                        break loop;
                    }
                    break;
                case Constants.INFOBIP_SMS:
                    InfobipSMS infobipSMS = new InfobipSMS();
                    smsStatusDTO = infobipSMS.getDeliveryStatus(brandIdOfMsg, msgDTO.getResponseId());
                    if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.SUCCESS) {
                        msgDTO.setDlStatus(1);
                        deliveryResultReceived = true;
                        //db update
                        updateDB(msgDTO);
                        LOGGER.error("[Second phase successful]  [Confirmation received from ] brandId  : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                    } else if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.ERROR) {
                        msgDTO.setDlStatus(-1);
                        //db update
                        updateDB(msgDTO);
                        LOGGER.error("[Second phase failed] using brandId --> brandID : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                        break loop;
                    }
                    break;
                case Constants.ROBIAXIATA_SMS:
                    RobiAxiataSMS robiAxiataSMS = new RobiAxiataSMS();
                    smsStatusDTO = robiAxiataSMS.getDeliveryStatus(brandIdOfMsg, msgDTO.getResponseId());
                    if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.SUCCESS) {
                        msgDTO.setDlStatus(1);
                        deliveryResultReceived = true;
                        //db update
                        updateDB(msgDTO);
                        LOGGER.error("[Second phase successful]  [Confirmation received from ] brandId  : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                    } else if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.ERROR) {
                        msgDTO.setDlStatus(-1);
                        //db update
                        updateDB(msgDTO);
                        LOGGER.error("[Second phase failed] using brandId --> brandID : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                        break loop;
                    }
                    break;
                case Constants.WAVECELL:
                    Wavecell wavecell = new Wavecell();
                    smsStatusDTO = wavecell.getDeliveryStatus(brandIdOfMsg, msgDTO.getResponseId());
                    if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.SUCCESS) {
                        msgDTO.setDlStatus(1);
                        deliveryResultReceived = true;
                        //db update
                        updateDB(msgDTO);
                        LOGGER.error("[Second phase successful]  [Confirmation received from ] brandId  : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                    } else if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.ERROR) {
                        msgDTO.setDlStatus(-1);
                        //db update
                        updateDB(msgDTO);
                        LOGGER.error("[Second phase failed] using brandId --> brandID : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                        break loop;
                    }
                    break;

//                case Constants.GP:
//                    GP gp = new GP();
//                    smsStatusDTO = gp.getDeliveryStatus(brandIdOfMsg, msgDTO);
//                    if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.SUCCESS) {
//                        msgDTO.setDlStatus(1);
//                        deliveryResultReceived = true;
//                        //db update
//                        updateDB(msgDTO);
//
//                        logger.error("[Second phase successful]  [Confirmation received from ] brandId  : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
//                    } else if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.ERROR) {
//                        msgDTO.setDlStatus(-1);
//                        //db update
//                        updateDB(msgDTO);
//
//                        logger.error("[Second phase failed] using brandId --> brandID : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
//                        break loop;
//                    }
//                    break;
                case Constants.SMS_ROUTER:
                case Constants.SMS_CELLENT:
                case Constants.MOBI_SMS:
                case Constants.SMS_COUNTRY:
                    LOGGER.debug("Checking DB : [brandIdOfMsg] --> " + brandIdOfMsg + " Response ID : " + msgDTO.getResponseId());
                    smsStatusDTO = getDeliveryStatus(brandIdOfMsg, msgDTO.getResponseId());
                    if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.SUCCESS) {
                        deliveryResultReceived = true;
                        LOGGER.error("CallBack returned delivery success of brandID : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                    } else if (smsStatusDTO.getStatus() == SmsStatusDTO.STATUS.ERROR) {
                        LOGGER.error("[Second phase failed] using brandId --> brandID : " + brandIdOfMsg + " msgID : " + msgDTO.getResponseId());
                        break loop;
                    }
//                    logger.error("API is not supported for brandId --> " + brandId + ". Please use callBack!");
                    break;
                case Constants.CS_NETWORKS:
                case Constants.ACL:
                case Constants.CLICKN_GO_BD:
                case Constants.PLIVO_API:
                case Constants.TEXT_LOCAL:
                    deliveryResultReceived = true;
                    LOGGER.error("This operator [brandId] --> " + brandIdOfMsg + " shouldn't be used any more! Please contact admin.");
                    break loop;
            }
            count++;
        }

        if (!deliveryResultReceived) {
            LOGGER.error("deliveryResultReceived : false! Message sending failed for  brand : " + msgDTO.getBrandId());
            msgDTO.setDlStatus(-1);
            //db update
            updateDB(msgDTO);
        }
        return deliveryResultReceived;
    }

    private void updateDB(MessagesDTO msDTO) {
        try {
            MessageScheduler.getInstance().updateDeliverySMSStatus(msDTO);
        } catch (Exception e) {
            LOGGER.error("Error occured while updating DB --> " + e);
        }
    }

    private SmsStatusDTO getDeliveryStatus(int brandId, String responseId) {
        LOGGER.debug("brandId --> " + brandId + " responseId --> " + responseId);
        MessagesDTO messagesDTO = MessageScheduler.getInstance().getMessage(brandId, responseId);
        if (messagesDTO == null) {
            try {
                Thread.sleep(5000);
                messagesDTO = MessageScheduler.getInstance().getMessage(brandId, msgId);
            } catch (InterruptedException ex) {
            }
        }
        SmsStatusDTO smsStatusDTO = new SmsStatusDTO();
        if (messagesDTO == null) {
            LOGGER.error("getDeliveryStatus in SMSDeliveryChecker --> messagesDTO is null  for : brandId --> " + brandId + " & responseId --> " + responseId);
            smsStatusDTO.setStatus(SmsStatusDTO.STATUS.ERROR);
        } else if (messagesDTO.getDlStatus() > 0) {
            smsStatusDTO.setStatus(SmsStatusDTO.STATUS.SUCCESS);
        } else if (messagesDTO.getDlStatus() < 0) {
            smsStatusDTO.setStatus(SmsStatusDTO.STATUS.ERROR);
        }
        return smsStatusDTO;
    }
}
