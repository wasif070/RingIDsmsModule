package module.ringid.sms.smsroutes;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import utilsdbconnector.DBConnection;

public class SMSRoutesLoader {

    static Logger logger = Logger.getLogger("com.ringid.sms");
    private static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    public HashMap<Integer, SMSRoutesDTO> sMSRoutesHashMapList;
    public HashMap<Integer, SMSRoutesDTO> voiceRoutesHashMapList;
    //public HashMap<String, ArrayList<SMSRoutesDTO>> sMSRoutesHashMapByCountryCode;
    //public HashMap<String, SMSRoutesDTO> sMSRoutesHashMapByOperatorName;
    //public HashMap<String, SMSRoutesDTO> sMSRoutesHashMapByBrandName;
    //public ArrayList<SMSRoutesDTO> sMSRoutesList;
    static SMSRoutesLoader sMSRoutesLoader = null;

    public SMSRoutesLoader() {
    }

    public static SMSRoutesLoader getInstance() {
        if (sMSRoutesLoader == null) {
            createLoader();
        }
        return sMSRoutesLoader;
    }

    private synchronized static void createLoader() {
        if (sMSRoutesLoader == null) {
            sMSRoutesLoader = new SMSRoutesLoader();
        }
    }

    private String getTableName(String routesType) {
        String tableName = "smsroutes";
        if (routesType != null && routesType.equals("voice")) {
            tableName = " voiceroutes ";
        }
        return tableName;
    }

    private String getRatesName(String routesType) {
        String ratesName = "smsrates";
        if (routesType != null && routesType.equals("voice")) {
            ratesName = "voicerates";
        }
        return ratesName;
    }

    public void getsetdata() {
        sMSRoutesHashMapList = new HashMap<>();
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT smsroutes.id AS id,smsroutes.countryId AS conId ,smsroutes.operatorId AS oprId,smsroutes.brandId AS bndId,  country.countryName AS con,country.countryCode AS conCode,operators.operatorName AS op,operators.numberPattern AS opNumber,smsbrands.brandName AS brand,smsroutes.smsrates AS rate,smsroutes.priority AS priority\n"
                    + "    FROM smsroutes LEFT JOIN country\n"
                    + "	ON smsroutes.countryId = country.id \n"
                    + "		 LEFT JOIN operators\n"
                    + "			ON smsroutes.operatorId = operators.id \n"
                    + "				LEFT JOIN  smsbrands \n"
                    + "					ON smsroutes.brandId= smsbrands.id ORDER BY smsroutes.smsrates ASC;";

            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                SMSRoutesDTO brandDTO = new SMSRoutesDTO();
                brandDTO.setId(rs.getInt("id"));
                brandDTO.setCountryId(rs.getInt("conId"));
                brandDTO.setOperatorId(rs.getInt("oprId"));
                brandDTO.setBrandId(rs.getInt("bndId"));
                brandDTO.setCountryName(rs.getString("con"));
                brandDTO.setCountryCode(rs.getString("conCode"));
                brandDTO.setOperatorName(rs.getString("op"));
                brandDTO.setNumberPattern(rs.getString("opNumber"));
                brandDTO.setBrandName(rs.getString("brand"));
                brandDTO.setSmsRates(rs.getDouble("rate"));
                brandDTO.setPriority(rs.getInt("priority"));
                sMSRoutesHashMapList.put(brandDTO.getId(), brandDTO);
            }
            logger.debug(sql + ":: smsRoutes --> " + sMSRoutesHashMapList.size());
        } catch (Exception e) {
            logger.debug("Exception SMS RoutesLoader-->", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    utilsdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
    }

    public void getsetvoicedata() {
        voiceRoutesHashMapList = new HashMap<>();
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT voiceroutes.id AS id,voiceroutes.countryId AS conId ,voiceroutes.operatorId AS oprId,voiceroutes.brandId AS bndId,  country.countryName AS con,country.countryCode AS conCode,operators.operatorName AS op,operators.numberPattern AS opNumber,smsbrands.brandName AS brand,voiceroutes.voicerates AS rate,voiceroutes.priority AS priority\n"
                    + "    FROM voiceroutes LEFT JOIN country\n"
                    + "         ON voiceroutes.countryId = country.id \n"
                    + "             LEFT JOIN operators\n"
                    + "			ON voiceroutes.operatorId = operators.id \n"
                    + "				LEFT JOIN  smsbrands \n"
                    + "					ON voiceroutes.brandId= smsbrands.id ORDER BY voiceroutes.voicerates ASC;";

            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                SMSRoutesDTO brandDTO = new SMSRoutesDTO();
                brandDTO.setId(rs.getInt("id"));
                brandDTO.setCountryId(rs.getInt("conId"));
                brandDTO.setOperatorId(rs.getInt("oprId"));
                brandDTO.setBrandId(rs.getInt("bndId"));
                brandDTO.setCountryName(rs.getString("con"));
                brandDTO.setCountryCode(rs.getString("conCode"));
                brandDTO.setOperatorName(rs.getString("op"));
                brandDTO.setNumberPattern(rs.getString("opNumber"));
                brandDTO.setBrandName(rs.getString("brand"));
                brandDTO.setSmsRates(rs.getDouble("rate"));
                brandDTO.setPriority(rs.getInt("priority"));
                voiceRoutesHashMapList.put(brandDTO.getId(), brandDTO);
            }
            logger.debug(sql + ":: voiceRoutes --> " + voiceRoutesHashMapList.size());
        } catch (Exception e) {
            logger.debug("[getsetvoicedata] Exception SMS RoutesLoader-->", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    utilsdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            getsetdata();
            getsetvoicedata();
        }
    }

    public synchronized ArrayList<SMSRoutesDTO> getSMSRoutesList(SMSRoutesDTO sdto) {
        checkForReload();
        ArrayList<SMSRoutesDTO> data = new ArrayList<SMSRoutesDTO>();
        Set set;
        if (getTableName(sdto.getRoutesType()).equals("smsroutes")) {
            set = sMSRoutesHashMapList.entrySet();
        } else {
            set = voiceRoutesHashMapList.entrySet();
        }
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            SMSRoutesDTO dto = (SMSRoutesDTO) me.getValue();
            if ((sdto.isSearchByCountryId() && dto.getCountryId() != sdto.getCountryId())
                    || (sdto.isSearchByOperatorId() && dto.getOperatorId() != sdto.getOperatorId())
                    || (sdto.isSearchByBrandId() && dto.getBrandId() != sdto.getBrandId())) {
                continue;
            }
            data.add(dto);
        }
        logger.debug("Data" + data.size());
        return data;
    }

    public synchronized ArrayList<SMSRoutesDTO> getSMSRoutesList(int coloumValue, int sortType, SMSRoutesDTO sdto) {
        checkForReload();
        ArrayList<SMSRoutesDTO> data = new ArrayList<SMSRoutesDTO>();
        Set set;
        if (getTableName(sdto.getRoutesType()).equals("smsroutes")) {
            set = sMSRoutesHashMapList.entrySet();
        } else {
            set = voiceRoutesHashMapList.entrySet();
        }
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            SMSRoutesDTO dto = (SMSRoutesDTO) me.getValue();
            if ((sdto.isSearchByCountryId() && dto.getCountryId() != sdto.getCountryId())
                    || (sdto.isSearchByOperatorId() && dto.getOperatorId() != sdto.getOperatorId())
                    || (sdto.isSearchByBrandId() && dto.getBrandId() != sdto.getBrandId())) {
                continue;
            }
            data.add(dto);
        }
        logger.debug("Data" + data.size());
        return data;
    }

    public synchronized ArrayList<SMSRoutesDTO> getSMSRoutesForDownload() {
        checkForReload();
        ArrayList<SMSRoutesDTO> data = new ArrayList<SMSRoutesDTO>();
        data = new ArrayList<SMSRoutesDTO>(sMSRoutesHashMapList.values());
        return data;
    }

    public synchronized ArrayList<SMSRoutesDTO> getSMSPlanByRoutesForDownload(int id) {
        checkForReload();
        ArrayList<SMSRoutesDTO> data = new ArrayList<SMSRoutesDTO>();
        Set set = sMSRoutesHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            SMSRoutesDTO dto = (SMSRoutesDTO) me.getValue();
            if (dto.getBrandId() == id) {
                data.add(dto);
            }
        }
        logger.debug("Data" + data.size());
        return data;
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        getsetdata();
        getsetvoicedata();
    }

    public synchronized SMSRoutesDTO getSMSRoutesDTO(int id, String routesType) {
        checkForReload();
        if (getTableName(routesType).equals("smsroutes") && sMSRoutesHashMapList.containsKey(id)) {
            return sMSRoutesHashMapList.get(id);
        } else if (voiceRoutesHashMapList.containsKey(id)) {
            return voiceRoutesHashMapList.get(id);
        }
        return null;
    }

    public synchronized Double getSMSRate(int countryId, int brandId) {
        checkForReload();
        Set set = sMSRoutesHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            SMSRoutesDTO dto = (SMSRoutesDTO) me.getValue();
            if ((dto.getCountryId() != countryId) && dto.getBrandId() != brandId) {
                continue;
            }
            return dto.getSmsRates();
        }
        return 0.0;
    }
}
