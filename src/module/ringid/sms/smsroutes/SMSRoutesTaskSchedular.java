package module.ringid.sms.smsroutes;

import module.ringid.sms.dto.BrandListFetchDTO;
import java.util.ArrayList;
import java.util.Map;
import module.ringid.sms.utils.MyAppError;
import java.util.HashMap;

public class SMSRoutesTaskSchedular {

    public static SMSRoutesTaskSchedular scheduler;

    public static SMSRoutesTaskSchedular getInstance() {
        if (scheduler == null) {
            scheduler = new SMSRoutesTaskSchedular();
        }
        return scheduler;
    }

    public MyAppError addSMSRoutesInfo(SMSRoutesDTO p_dto) {
        SMSRoutesDAO countryDAO = new SMSRoutesDAO();
        return countryDAO.addSMSRoutesInfo(p_dto);
    }

    public MyAppError addSMSRoutesInfo(ArrayList<SMSRoutesDTO> maps , String routesType) {
        SMSRoutesDAO countryDAO = new SMSRoutesDAO();
        return countryDAO.addSMSRoutesInfo(maps, routesType);
    }

    public MyAppError addSMSRoutesInfo(Map<Integer, ArrayList<String>> maps) {
        SMSRoutesDAO countryDAO = new SMSRoutesDAO();
        return countryDAO.addSMSRoutesInfo(maps);
    }

    public ArrayList<Map.Entry<Integer, SMSRoutesDTO>> getSMSRoutesList() {
        SMSRoutesDAO brandsDAO = new SMSRoutesDAO();
        return brandsDAO.getSMSRoutesList();
    }

    public MyAppError updateSMSRoutes(SMSRoutesDTO p_dto) {
        SMSRoutesDAO smsRoutesDAO = new SMSRoutesDAO();
        return smsRoutesDAO.updateSMSRoutes(p_dto);
    }

    public MyAppError deleteSMSRoutes(int id, String routesType) {
        SMSRoutesDAO smsRoutesDAO = new SMSRoutesDAO();
        return smsRoutesDAO.deleteSMSRoutes(id, routesType);
    }

    public SMSRoutesDTO getSMSRoutesById(int id, String routesType) {
        return SMSRoutesLoader.getInstance().getSMSRoutesDTO(id, routesType);
    }

    public ArrayList<SMSRoutesDTO> getSmsRoutesList(SMSRoutesDTO dto) {
        return SMSRoutesLoader.getInstance().getSMSRoutesList(dto);
    }

    public ArrayList<SMSRoutesDTO> getSmsRoutesList(int columnVal, int sortType, SMSRoutesDTO dto) {
        return SMSRoutesLoader.getInstance().getSMSRoutesList(columnVal, sortType, dto);
    }

    public ArrayList<SMSRoutesDTO> getSMSRoutesForDownload() {
        return SMSRoutesLoader.getInstance().getSMSRoutesForDownload();
    }

    /*
    public ArrayList<Integer> getBrandId(int countryId, int operatorID) {
        SMSRoutesDAO smsRoutesDAO = new SMSRoutesDAO();
        return smsRoutesDAO.getBrandId(countryId, operatorID);
    }
     */
    public BrandListFetchDTO getBrandId(int countryId) {
        SMSRoutesDAO smsRoutesDAO = new SMSRoutesDAO();
        return smsRoutesDAO.getBrandId(countryId);
    }

    public BrandListFetchDTO getBrandId(int countryId, int operatorId) {
        SMSRoutesDAO smsRoutesDAO = new SMSRoutesDAO();
        return smsRoutesDAO.getBrandId(countryId, operatorId);
    }

    public synchronized ArrayList<SMSRoutesDTO> getSMSPlanByRoutesForDownload(int id) {
        return SMSRoutesLoader.getInstance().getSMSPlanByRoutesForDownload(id);
    }

    public synchronized Double getSMSRate(int countryId, int brandId) {
        return SMSRoutesLoader.getInstance().getSMSRate(countryId, brandId);
    }

    public MyAppError deleteAllSMSRoutesByIds(SMSRoutesDTO sMSRoutesDTO) {
        SMSRoutesDAO smsRoutesDAO = new SMSRoutesDAO();
        return smsRoutesDAO.deleteAllSMSRoutesByIds(sMSRoutesDTO);
    }
}
