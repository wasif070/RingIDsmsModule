package module.ringid.sms.smsroutes;

import module.ringid.sms.BaseDAO;
import module.ringid.sms.dto.BrandListFetchDTO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import module.ringid.sms.utils.MyAppError;

public class SMSRoutesDAO extends BaseDAO {

    static Logger logger = Logger.getLogger("com.ringid.sms");

    private String getClause(String optionalSql) {
        return (optionalSql.length() < 1 ? " WHERE " : " AND ");
    }

    private String getTableName(String routesType) {
        String tableName = "smsroutes";
        if (routesType != null && routesType.equals("voice")) {
            tableName = " voiceroutes ";
        }
        return tableName;
    }

    private String getRatesName(String routesType) {
        String ratesName = "smsrates";
        if (routesType != null && routesType.equals("voice")) {
            ratesName = "voicerates";
        }
        return ratesName;
    }

    public MyAppError addSMSRoutesInfo(SMSRoutesDTO dto) {
        try {
            String sql;
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            sql = "SELECT id FROM " + getTableName(dto.getRoutesType()) + " WHERE countryId = " + dto.getCountryId() + " AND operatorId = " + dto.getOperatorId() + " AND brandId = " + dto.getBrandId(); //"SELECT id FROM smsroutes WHERE countryId = " + dto.getCountryId() + " AND operatorId = " + dto.getOperatorId() + " AND brandId = " + dto.getBrandId() + "";
            logger.debug("[addSMSRoutesInfo] sql --> " + sql);
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                error.setERROR_TYPE(MyAppError.OTHERERROR);
                error.setErrorMessage("Duplicate Contents");
                throw new Exception("Duplicate Contents");
            }

            String query = "INSERT INTO " + getTableName(dto.getRoutesType()) + " (countryId,operatorId,brandId," + getRatesName(dto.getRoutesType()) + ",priority) VALUES(?,?,?,?,?);";
            logger.debug("[addSMSRoutesInfo] query --> " + query);
            ps = db.connection.prepareStatement(query); // create the mysql insert preparedstatement
            ps.setInt(1, dto.getCountryId());
            ps.setInt(2, dto.getOperatorId());
            ps.setInt(3, dto.getBrandId());
            ps.setDouble(4, dto.getSmsRates());
            ps.setInt(5, dto.getPriority());
            ps.execute();  // execute the preparedstatement
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.error("" + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            logger.error("" + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError addSMSRoutesInfo(ArrayList<SMSRoutesDTO> maps, String routesType) {
        MyAppError error = new MyAppError();
        int[] returnVal;
        int errorCount = 0;
        String msg = "";
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();

            String query = " INSERT INTO " + getTableName(routesType) + " (countryId,operatorId,brandId," + getRatesName(routesType) + ",priority)      \n"
                    + "                            SELECT * FROM (SELECT ? AS a,? AS b,? AS c,? AS d,? AS e) AS tmp                       \n"
                    + "                            WHERE NOT EXISTS (                                          \n"
                    + "                           SELECT countryId,operatorId,brandId," + getRatesName(routesType) + ",priority FROM " + getTableName(routesType) + " WHERE countryId = ? AND  operatorId = ? AND brandId = ?\n"
                    + "                            ) LIMIT 1;";
            ps = db.connection.prepareStatement(query); // create the mysql insert preparedstatement
            int i = 1;
            for (SMSRoutesDTO entry : maps) {
                ps.setInt(1, entry.getCountryId());
                ps.setInt(2, entry.getOperatorId());
                ps.setInt(3, entry.getBrandId());
                ps.setDouble(4, entry.getSmsRates());
                ps.setInt(5, entry.getPriority());
                ps.setInt(6, entry.getCountryId());
                ps.setInt(7, entry.getOperatorId());
                ps.setInt(8, entry.getBrandId());
                ps.addBatch();
                i++;
                if ((i + 1) % 1000 == 0) {
                    ps.executeBatch(); // Execute every 1000 items.
                }
            }
            returnVal = ps.executeBatch(); // execute the preparedstatement
            for (int index = 0; index < returnVal.length; index++) {
                if (returnVal[index] == 0) {
                    errorCount++;
                }
            }
            if (errorCount > 0) {
                msg = errorCount + " route-info insertion failed and " + (returnVal.length - errorCount) + " inserted successfully";
            } else {
                msg = returnVal.length + " rows inserted successfully";
            }
            error.setErrorMessage(msg);
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.error("" + e);
        } catch (Exception e) {
        } finally {
            close();
        }
        return error;
    }

    public MyAppError addSMSRoutesInfo(Map<Integer, ArrayList<String>> maps) {
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();

            String query = " INSERT INTO smsroutes(countryId,operatorId,brandId,smsrates,priority)      \n"
                    + "                            SELECT * FROM (SELECT ? AS a,? AS b,? AS c,? AS d,? AS e) AS tmp                       \n"
                    + "                            WHERE NOT EXISTS (                                          \n"
                    + "                           SELECT countryId,operatorId,brandId,smsrates,priority FROM smsroutes WHERE countryId = ? AND  operatorId = ? AND brandId = ?\n"
                    + "                            ) LIMIT 1;";
            ps = db.connection.prepareStatement(query); // create the mysql insert preparedstatement
            int i = 1;
            for (Map.Entry<Integer, ArrayList<String>> entry : maps.entrySet()) {
                ps.setInt(1, Integer.parseInt(entry.getValue().get(0)));
                ps.setInt(2, Integer.parseInt(entry.getValue().get(1)));
                ps.setInt(3, Integer.parseInt(entry.getValue().get(2)));
                ps.setDouble(4, Double.parseDouble(entry.getValue().get(3)));
                ps.setInt(5, Integer.parseInt(entry.getValue().get(4)));
                ps.setInt(6, Integer.parseInt(entry.getValue().get(0)));
                ps.setInt(7, Integer.parseInt(entry.getValue().get(1)));
                ps.setInt(8, Integer.parseInt(entry.getValue().get(2)));
                ps.addBatch();
                i++;
                if ((i + 1) % 1000 == 0) {
                    ps.executeBatch(); // Execute every 1000 items.
                }
            }
            ps.executeBatch();  // execute the preparedstatement
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.error("" + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            logger.error("" + e);
        } finally {
            close();
        }
        return error;
    }

    ArrayList<Map.Entry<Integer, SMSRoutesDTO>> getSMSRoutesList() {
        Map<Integer, SMSRoutesDTO> brandInfMap = new HashMap<>();
        ArrayList<Map.Entry<Integer, SMSRoutesDTO>> entryList = null;
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT smsroutes.id AS id,country.countryName AS con,operators.operatorName AS op,smsbrands.brandName AS brand,smsroutes.brandId AS brandID,smsroutes.smsrates AS rate,smsroutes.priority AS priority\n"
                    + "    FROM smsroutes LEFT JOIN country\n"
                    + "	ON smsroutes.countryId = country.id \n"
                    + "		 LEFT JOIN operators\n"
                    + "			ON smsroutes.operatorId = operators.id \n"
                    + "				LEFT JOIN  smsbrands \n"
                    + "					ON smsroutes.brandId= smsbrands.id ORDER BY smsroutes.smsrates ASC;";

            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                try {
                    SMSRoutesDTO brandDTO = new SMSRoutesDTO();
                    brandDTO.setId(rs.getInt("id"));
                    brandDTO.setCountryName(rs.getString("con"));
                    brandDTO.setOperatorName(rs.getString("op"));
                    brandDTO.setBrandName(rs.getString("brand"));
                    brandDTO.setBrandId(rs.getInt("brandID"));
                    brandDTO.setSmsRates(rs.getDouble("rate"));
                    brandDTO.setPriority(rs.getInt("priority"));
                    brandInfMap.put(brandDTO.getId(), brandDTO);
                } catch (Exception e) {
                    logger.error(e);
                }
            }
            entryList = new ArrayList<>(brandInfMap.entrySet());
            Collections.sort(entryList, new Comparator<Map.Entry<Integer, SMSRoutesDTO>>() {
                @Override
                public int compare(Map.Entry<Integer, SMSRoutesDTO> integerEmployeeEntry,
                        Map.Entry<Integer, SMSRoutesDTO> integerEmployeeEntry2) {
                    return new Double(integerEmployeeEntry.getValue().getSmsRates())
                            .compareTo(integerEmployeeEntry2.getValue().getSmsRates());
                }
            });
        } catch (Exception e) {
            logger.error("Exception [getSMSRoutesList] --> " + e);
        } finally {
            close();
        }
        return entryList;
    }

    public MyAppError updateSMSRoutes(SMSRoutesDTO dto) {
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE " + getTableName(dto.getRoutesType()) + " SET countryId =? ,operatorId =? ,brandId =? ," + getRatesName(dto.getRoutesType()) + " =? ,priority =?  WHERE id=?;";
            logger.debug("[updateSMSRoutes] query --> " + query);
            ps = db.connection.prepareStatement(query);
            ps.setInt(1, dto.getCountryId());
            ps.setInt(2, dto.getOperatorId());
            ps.setInt(3, dto.getBrandId());
            ps.setDouble(4, dto.getSmsRates());
            ps.setInt(5, dto.getPriority());
            ps.setInt(6, dto.getId());
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.error("" + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            logger.error("" + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError deleteSMSRoutes(int id, String routesType) {
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql;
            sql = "DELETE FROM " + getTableName(routesType) + " WHERE id = " + id;
            logger.debug("[deleteSMSRoutes] sql --> " + sql);
            int a = stmt.executeUpdate(sql);
            if (a > 0) {
                logger.debug("delete success");
            }
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.error("" + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            logger.error("" + e);
        } finally {
            close();
        }
        return error;
    }

    /*
    public ArrayList<Integer> getBrandId(int countryId, int operatorID) {
        ArrayList<Integer> brandIdMap = new ArrayList<>();
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql;
            if (operatorID > 0) {
                sql = "SELECT brandId FROM smsroutes WHERE countryId = " + countryId + " AND operatorId =" + operatorID + " ORDER BY priority DESC,smsrates ASC;";
            } else {
                sql = "SELECT brandId FROM smsroutes WHERE countryId = " + countryId + " ORDER BY priority DESC,smsrates ASC;";
            }
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                brandIdMap.add(rs.getInt("brandId"));
            }
        } catch (Exception e) {
            return null;
        } finally {
            close();
        }
        return brandIdMap;
    }
     */
    public BrandListFetchDTO getBrandId(int countryId) {
        BrandListFetchDTO feedBack = new BrandListFetchDTO();
        HashMap<Integer, ArrayList<Integer>> brandIdMap = new HashMap<>();
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();

            String sql = "SELECT smsroutes.brandId as brandId,smsroutes.smsrates,smsroutes.priority as priority, smsbrands.brandName, smsbrands.balence, smsbrands.commercial "
                    + "FROM smsroutes INNER JOIN smsbrands "
                    + "ON smsbrands.id=smsroutes.brandId AND smsroutes.countryId=" + countryId + " AND smsbrands.commercial ORDER BY smsroutes.priority ASC";
            rs = stmt.executeQuery(sql);
            int count = 0;
            while (rs.next()) {
                int priority = rs.getInt("priority");
                if (brandIdMap.containsKey(priority)) {
                    ArrayList<Integer> list = brandIdMap.get(priority);
                    list.add(rs.getInt("brandId"));
                    count++;
                } else {
                    ArrayList<Integer> list = new ArrayList<>();
                    list.add(rs.getInt("brandId"));
                    brandIdMap.put(priority, list);
                    count++;
                }
            }
            feedBack.setMap(brandIdMap);
            feedBack.setCount(count);
        } catch (Exception e) {
            return null;
        } finally {
            close();
        }
        return feedBack;
    }

    public BrandListFetchDTO getBrandId(int countryId, int operatorId) {
        BrandListFetchDTO feedBack = new BrandListFetchDTO();
        HashMap<Integer, ArrayList<Integer>> brandIdMap = new HashMap<>();
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();

            String sql = "SELECT smsroutes.brandId as brandId,smsroutes.smsrates,smsroutes.priority as priority, smsbrands.brandName, smsbrands.balence, smsbrands.commercial "
                    + "FROM smsroutes INNER JOIN smsbrands "
                    + "ON smsbrands.id=smsroutes.brandId AND smsroutes.countryId=" + countryId + " AND smsroutes.operatorId=" + operatorId + " AND smsbrands.commercial ORDER BY smsroutes.priority ASC";
            rs = stmt.executeQuery(sql);
            int count = 0;
            while (rs.next()) {
                int priority = rs.getInt("priority");
                if (brandIdMap.containsKey(priority)) {
                    ArrayList<Integer> list = brandIdMap.get(priority);
                    list.add(rs.getInt("brandId"));
                    count++;
                } else {
                    ArrayList<Integer> list = new ArrayList<>();
                    list.add(rs.getInt("brandId"));
                    brandIdMap.put(priority, list);
                    count++;
                }
            }
            feedBack.setMap(brandIdMap);
            feedBack.setCount(count);
        } catch (Exception e) {
            return null;
        } finally {
            close();
        }
        return feedBack;
    }

    MyAppError deleteAllSMSRoutesByIds(SMSRoutesDTO sMSRoutesDTO) {
        MyAppError error = new MyAppError();
        int[] returnVal;
        int errorCount = 0;
        String msg = "";
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "";
            sql = "DELETE FROM " + getTableName(sMSRoutesDTO.getRoutesType()) + " WHERE id = ?";
            ps = db.connection.prepareStatement(sql);
            int i = 1;
            for (int id : sMSRoutesDTO.getSelectedIDs()) {
                ps.setInt(1, id);
                ps.addBatch();
                i++;
                if ((i + 1) % 1000 == 0) {
                    ps.executeBatch(); // Execute every 1000 items.
                }
            }
            returnVal = ps.executeBatch(); // execute the preparedstatement
            for (int index = 0; index < returnVal.length; index++) {
                if (returnVal[index] == 0) {
                    errorCount++;
                }
            }
            if (errorCount > 0) {
                msg = errorCount + " routes deletion failed and " + (returnVal.length - errorCount) + " deletion successfully";
            } else {
                msg = returnVal.length + " rows delete successfully";
            }
            error.setErrorMessage(msg);
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.error("" + e);
        } catch (Exception e) {
        } finally {
            close();
        }
        return error;
    }
}
