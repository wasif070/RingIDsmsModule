package module.ringid.sms.smsroutes;

import java.util.Comparator;

public class SMSRoutesDTO {

    private int id;
    private int countryId;
    private String countryName;
    private String countryCode;
    private String numberPattern;
    private int brandId;
    private String brandName;
    private int operatorId;
    private String operatorName;
    private double smsRates;
    private int priority;
    private int[] selectedIDs;
    private boolean searchByCountryId = false;
    private boolean searchByOperatorId = false;
    private boolean searchByBrandId = false;
    private boolean searchByAll = false;
    private String routesType;

    public String getRoutesType() {
        return routesType;
    }

    public void setRoutesType(String routesType) {
        this.routesType = routesType;
    }

    public int[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(int[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getNumberPattern() {
        return numberPattern;
    }

    public void setNumberPattern(String numberPattern) {
        this.numberPattern = numberPattern;
    }

    public boolean isSearchByCountryId() {
        return searchByCountryId;
    }

    public void setSearchByCountryId(boolean searchByCountryId) {
        this.searchByCountryId = searchByCountryId;
    }

    public boolean isSearchByOperatorId() {
        return searchByOperatorId;
    }

    public void setSearchByOperatorId(boolean searchByOperatorId) {
        this.searchByOperatorId = searchByOperatorId;
    }

    public boolean isSearchByBrandId() {
        return searchByBrandId;
    }

    public void setSearchByBrandId(boolean searchByBrandId) {
        this.searchByBrandId = searchByBrandId;
    }

    public boolean isSearchByAll() {
        return searchByAll;
    }

    public void setSearchByAll(boolean searchByAll) {
        this.searchByAll = searchByAll;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public int getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(int operatorId) {
        this.operatorId = operatorId;
    }

    public double getSmsRates() {
        return smsRates;
    }

    public void setSmsRates(double smsRates) {
        this.smsRates = smsRates;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
    public static Comparator<SMSRoutesDTO> prioriyCompair = new Comparator<SMSRoutesDTO>() {
        @Override
        public int compare(SMSRoutesDTO s1, SMSRoutesDTO s2) {
            int rollno1 = s1.getPriority();
            int rollno2 = s2.getPriority();
            return rollno1 - rollno2;
            /*For ascending order*/
 /*For descending order*/

            //rollno2-rollno1;
        }
    };
}
