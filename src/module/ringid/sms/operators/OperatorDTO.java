
package module.ringid.sms.operators;

public class OperatorDTO {
    private int         id;
    private int         countryId;
    private String      countryName;
    private String      operatorName;
    private String      numberPattern;
    private boolean searchByName = false;

    public boolean isSearchByName() {
        return searchByName;
    }

    public void setSearchByName(boolean searchByName) {
        this.searchByName = searchByName;
    }
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
    
    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String OperatorName) {
        this.operatorName = OperatorName;
    }

    public String getNumberPattern() {
        return numberPattern;
    }

    public void setNumberPattern(String numberPattern) {
        this.numberPattern = numberPattern;
    }
    
    
    
    
    
}
