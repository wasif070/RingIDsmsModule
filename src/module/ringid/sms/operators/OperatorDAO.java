package module.ringid.sms.operators;

import module.ringid.sms.BaseDAO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import module.ringid.sms.utils.MyAppError;

public class OperatorDAO extends BaseDAO {

    static Logger logger = Logger.getLogger("com.ringid.sms");

    public OperatorDAO() {
    }

    public MyAppError addOperatorInfo(OperatorDTO dto) {
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql;
            sql = "SELECT operatorName,numberPattern FROM operators WHERE operatorName ='" + dto.getOperatorName() + "';";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                error.setERROR_TYPE(MyAppError.OTHERERROR);
                error.setErrorMessage("Duplicate Contents");
                throw new Exception("Duplicate Contents");
            }
            String query = "INSERT INTO operators(operatorName,numberPattern) VALUES(?,?);";
            // create the mysql insert preparedstatement
            ps = db.connection.prepareStatement(query);
            ps.setString(1, dto.getOperatorName());
            ps.setString(2, dto.getNumberPattern());
            // execute the preparedstatement
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.error("" + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            logger.error("" + e);
        } finally {
            close();
        }

        return error;
    }

    public ArrayList<OperatorDTO> getContryNameAndIdList() {
        Map<Integer, OperatorDTO> stickerCollectionMap = new HashMap<>();
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id,countryName FROM country;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                try {
                    OperatorDTO colletionsDTO = new OperatorDTO();
                    colletionsDTO.setCountryId(rs.getInt("id"));
                    colletionsDTO.setCountryName(rs.getString("countryName"));
                    stickerCollectionMap.put(colletionsDTO.getCountryId(), colletionsDTO);
                } catch (Exception e) {
                }
            }
        } catch (Exception e) {
            return null;
        } finally {
            close();
        }
        return new ArrayList<>(stickerCollectionMap.values());
    }

    public ArrayList<OperatorDTO> getOperatorNameAndIdList() {
        Map<Integer, OperatorDTO> operatorNameAndIdMap = new HashMap<>();
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id,operatorName FROM operators ORDER BY operatorName;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                try {
                    OperatorDTO operatorDTO = new OperatorDTO();
                    operatorDTO.setId(rs.getInt("id"));
                    operatorDTO.setOperatorName(rs.getString("operatorName"));
                    operatorNameAndIdMap.put(operatorDTO.getId(), operatorDTO);
                } catch (Exception e) {
                }
            }
        } catch (Exception e) {
            return null;
        } finally {
            close();
        }
        return new ArrayList<>(operatorNameAndIdMap.values());
    }

    public ArrayList<OperatorDTO> getOperatorList() {
        Map<Integer, OperatorDTO> stickerCollectionMap = new HashMap<>();
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT operators.id AS id ,operators.operatorName AS op ,operators.numberPattern AS num,country.countryName  AS con  \n"
                    + "FROM operators                                                              \n"
                    + "LEFT JOIN country                                                           \n"
                    + "ON operators.countryId=country.id                                           \n"
                    + "ORDER BY country.countryName;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                try {
                    OperatorDTO operatorDTO = new OperatorDTO();
                    operatorDTO.setId(rs.getInt("id"));
                    operatorDTO.setOperatorName(rs.getString("op"));
                    operatorDTO.setNumberPattern(rs.getString("num"));
                    operatorDTO.setCountryName(rs.getString("con"));
                    stickerCollectionMap.put(operatorDTO.getId(), operatorDTO);
                } catch (Exception e) {
                }
            }
        } catch (Exception e) {
            return null;
        } finally {
            close();
        }
        return new ArrayList<>(stickerCollectionMap.values());
    }

    public MyAppError updateOperator(OperatorDTO dto) {
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE operators SET countryId = ?,operatorName = ?,numberPattern=? WHERE id=?";
            // create the mysql insert preparedstatement
            ps = db.connection.prepareStatement(query);
            ps.setInt(1, dto.getCountryId());
            ps.setString(2, dto.getOperatorName());
            ps.setString(3, dto.getNumberPattern());
            ps.setInt(4, dto.getId());
            // execute the preparedstatement
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.error("" + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            logger.error("" + e);
        } finally {
            close();
        }

        return error;
    }

    public MyAppError deleteOperator(int id) {
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql;
            sql = "DELETE FROM operators WHERE id = " + id + "";
            int a = stmt.executeUpdate(sql);
            if (a > 0) {
                logger.debug("delete success");
            }
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.error(e);
        } catch (Exception e) {
        } finally {
            close();
        }
        return error;
    }

    public OperatorDTO getOperatorInfoById(int id) {
        OperatorDTO operatorDTO = new OperatorDTO();
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "";
            sql = "SELECT operators.id AS idt ,countryId,country.countryName AS cName ,operatorName,numberPattern FROM operators \n"
                    + "	LEFT JOIN country ON country.id = operators.countryId WHERE operators.id =" + id + ";";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                try {
                    operatorDTO.setId(rs.getInt("idt"));
                    operatorDTO.setCountryId(rs.getInt("countryId"));
                    operatorDTO.setCountryName(rs.getString("cName"));
                    operatorDTO.setOperatorName(rs.getString("operatorName"));
                    operatorDTO.setNumberPattern(rs.getString("numberPattern"));
                } catch (Exception e) {
                }
            }
        } catch (Exception e) {
            return null;
        } finally {
            close();
        }
        return operatorDTO;
    }

    public Map<String, ArrayList<OperatorDTO>> getList(String countryCode) {
        Map<String, ArrayList<OperatorDTO>> mapList = new HashMap<>();
        ArrayList<OperatorDTO> list = new ArrayList<>();
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT operators.id AS id,operators.countryId AS cId ,operators.operatorName AS op ,operators.numberPattern AS num\n"
                    + "                          FROM operators                                                            \n"
                    + "                          LEFT JOIN country                                                        \n"
                    + "                           ON operators.countryId=country.id                                      \n"
                    + "                            WHERE country.countryCode = " + countryCode + ";";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                try {
                    OperatorDTO odto = new OperatorDTO();
                    odto.setId(rs.getInt("id"));
                    odto.setOperatorName(rs.getString("op"));
                    odto.setNumberPattern(rs.getString("num"));
                    odto.setCountryId(rs.getInt("cId"));
                    list.add(odto);
                } catch (Exception e) {
                }
            }
            mapList.put(countryCode, list);
        } catch (Exception e) {
            return null;
        } finally {
            close();
        }
        return mapList;
    }
}
