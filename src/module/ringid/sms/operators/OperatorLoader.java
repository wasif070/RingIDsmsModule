package module.ringid.sms.operators;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import module.ringid.sms.utils.MyAppError;
import utilsdbconnector.DBConnection;

public class OperatorLoader {

    private static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    public HashMap<Integer, OperatorDTO> operatorHashMapList;
    public HashMap<String, OperatorDTO> operatorHashMapByName;
    public ArrayList<OperatorDTO> operatorList;
    static OperatorLoader operatorLoader = null;
    static Logger logger = Logger.getLogger("com.ringid.sms");

    public OperatorLoader() {
    }

    public static OperatorLoader getInstance() {
        if (operatorLoader == null) {
            createLoader();
        }
        return operatorLoader;
    }

    private synchronized static void createLoader() {
        if (operatorLoader == null) {
            operatorLoader = new OperatorLoader();
        }
    }

    public void getsetdata() {
        operatorHashMapList = new HashMap<Integer, OperatorDTO>();
        operatorHashMapByName = new HashMap<String, OperatorDTO>();
        operatorList = new ArrayList<OperatorDTO>();
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT operators.id AS id,operators.operatorName AS op ,operators.numberPattern AS num  \n"
                    + "FROM operators;";

            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                OperatorDTO operatorDTO = new OperatorDTO();
                operatorDTO.setId(rs.getInt("id"));
                operatorDTO.setOperatorName(rs.getString("op"));
                operatorDTO.setNumberPattern(rs.getString("num"));

                operatorHashMapList.put(operatorDTO.getId(), operatorDTO);
                operatorHashMapByName.put(operatorDTO.getOperatorName(), operatorDTO);
                operatorList.add(operatorDTO);
            }

            logger.debug(sql + "::" + operatorHashMapList.size());

        } catch (Exception e) {
            logger.debug("Exception Operators List-->", e);

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    utilsdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            getsetdata();
        }
    }

    public synchronized ArrayList<OperatorDTO> getOperatorList(OperatorDTO sdto) {
        checkForReload();
        ArrayList<OperatorDTO> data = new ArrayList<>();
        Set set = operatorHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            OperatorDTO dto = (OperatorDTO) me.getValue();
            if ((sdto.isSearchByName() && !dto.getCountryName().equals(sdto.getCountryName()))) {
                continue;
            }
            data.add(dto);
        }
        logger.debug("Data" + data.size());
        return data;
    }

    public synchronized ArrayList<OperatorDTO> getOperatorList(int coloumValue, int sortType, OperatorDTO sdto) {
        checkForReload();
        ArrayList<OperatorDTO> data = new ArrayList<OperatorDTO>();
        Set set = operatorHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            OperatorDTO dto = (OperatorDTO) me.getValue();
            if ((sdto.isSearchByName() && !dto.getCountryName().equals(sdto.getCountryName()))) {
                continue;
            }
            data.add(dto);
        }
        logger.debug("Data" + data.size());
        return data;
    }

    public synchronized int getOperatorId(int id, String operatorName, String numberPattrn) {
        checkForReload();
        int operatorId = 0;
        getsetdata();
        for (Map.Entry<Integer, OperatorDTO> entrySet : operatorHashMapList.entrySet()) {
            OperatorDTO value = entrySet.getValue();
            if ((value.getCountryId() == id && value.getOperatorName().contains(operatorName) && value.getNumberPattern().contains(operatorName)) || value.getNumberPattern().contains(numberPattrn)) {
                return operatorId = entrySet.getKey();
            }

        }
        if (operatorId == 0) {
            OperatorDTO odto = new OperatorDTO();
            odto.setCountryId(id);
            odto.setOperatorName(operatorName);
            odto.setNumberPattern(numberPattrn);
            MyAppError error = OperatorTaskScheduler.getInstance().addOperatorInfo(odto);
            if (error.getERROR_TYPE() == 0) {
                getOperatorId(id, operatorName, numberPattrn);
            }
        }
        return operatorId;
    }

    public synchronized OperatorDTO getOperatorIdAndName(String numberPattrn) {
        checkForReload();
        OperatorDTO odto = new OperatorDTO();
        String number = numberPattrn.trim();
        for (Map.Entry<Integer, OperatorDTO> entrySet : operatorHashMapList.entrySet()) {
            OperatorDTO value = entrySet.getValue();
            if (number.equals(value.getNumberPattern())) {
                odto.setId(value.getId());
                odto.setOperatorName(value.getOperatorName());
                return odto;
            }
        }
        if (odto.getId() == 0) {
            odto.setId(1);
        }
        return odto;
    }

    public String getOperator(String countryCode, String phoneNumber) {
        byte phoneNumberBytes[] = phoneNumber.getBytes();
        ArrayList<String> list = new ArrayList<String>();
        Map<String, ArrayList<OperatorDTO>> map = OperatorTaskScheduler.getInstance().getList(countryCode);
        for (Map.Entry<String, ArrayList<OperatorDTO>> entrySet : map.entrySet()) {
            String key = entrySet.getKey();
            ArrayList<OperatorDTO> value = entrySet.getValue();
            for (int i = 0; i < value.size(); i++) {
                OperatorDTO get = value.get(i);
                get.getNumberPattern();
                list.add(get.getNumberPattern());
            }
        }
        int prefixLength = 0;
        String operator = null;
        for (String number : list) {
            byte numberPatternBytes[] = number.getBytes();
            int len = numberPatternBytes.length;
            int i;
            for (i = 0; i < len; i++) {
                if ((numberPatternBytes[i]) != (phoneNumberBytes[i])) {
                    break;
                }
            }
            if (i == len && prefixLength < i) {
                prefixLength = len;
                operator = number;
            }
        }

        return operator;
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        getsetdata();
    }

    public synchronized OperatorDTO getOperatorDTO(int id) {
        checkForReload();
        if (operatorHashMapList.containsKey(id)) {
            return operatorHashMapList.get(id);
        }
        return null;
    }

    public synchronized OperatorDTO getOperatorDTO(String operatorName) {
        checkForReload();
        if (operatorHashMapByName.containsKey(operatorName)) {
            return operatorHashMapByName.get(operatorName);
        }
        return null;
    }

//    public static void main(String[] args) {
//        
//        String  s = OperatorLoader.getInstance().getOperator("+88", "01734578596");
//    }
}
