package module.ringid.sms.operators;

import java.util.ArrayList;
import java.util.Map;
import module.ringid.sms.utils.MyAppError;

public class OperatorTaskScheduler {
    
    public static OperatorTaskScheduler scheduler;

    public static OperatorTaskScheduler getInstance() {
        if (scheduler == null) {
            scheduler = new OperatorTaskScheduler();
        }
        return scheduler;
    }

    public MyAppError addOperatorInfo(OperatorDTO p_dto) {
        OperatorDAO operatorDAO = new OperatorDAO();
        return operatorDAO.addOperatorInfo(p_dto);
    }

    public ArrayList<OperatorDTO> getCountryNameAndIdList() {
        OperatorDAO operatorDAO = new OperatorDAO();
        return operatorDAO.getContryNameAndIdList();
    }

    public ArrayList<OperatorDTO> getOperatorNameAndIdList() {
        OperatorDAO operatorDAO = new OperatorDAO();
        return operatorDAO.getOperatorNameAndIdList();
    }

    public ArrayList<OperatorDTO> getOperatorList() {
        OperatorDAO operatorDAO = new OperatorDAO();
        return operatorDAO.getOperatorList();
    }

    public MyAppError updateOperator(OperatorDTO p_dto) {
        OperatorDAO operatorDAO = new OperatorDAO();
        return operatorDAO.updateOperator(p_dto);
    }

    public MyAppError deleteOperator(int id) {
        OperatorDAO operatorDAO = new OperatorDAO();
        return operatorDAO.deleteOperator(id);
    }

    public OperatorDTO getOperatorInfoById(int id) {
        OperatorDAO operatorDAO = new OperatorDAO();
        return operatorDAO.getOperatorInfoById(id);
    }

    public ArrayList<OperatorDTO> getOperatorList(OperatorDTO dto) {
        return OperatorLoader.getInstance().getOperatorList(dto);
    }

    public int getOperatorIdByName(int id, String name, String numberPattern) {
        return OperatorLoader.getInstance().getOperatorId(id, name, numberPattern);
    }

    public OperatorDTO getOperatorIdAndName(String numberPattrn) {
        return OperatorLoader.getInstance().getOperatorIdAndName(numberPattrn);
    }

    public synchronized OperatorDTO getOperatorDTO(int id) {
        return OperatorLoader.getInstance().getOperatorDTO(id);
    }

//    MyAppError addOperator(OperatorDTO odto) {
//        OperatorDAO operatorDAO = new OperatorDAO();
//        return operatorDAO.addOperator(odto);
//    }

    public ArrayList<OperatorDTO> getOperatorList(int columnVal, int sortType, OperatorDTO dto) {
        return OperatorLoader.getInstance().getOperatorList(columnVal, sortType, dto);
    }
    
     public Map<String,ArrayList<OperatorDTO>>   getList(String countryCode){
      OperatorDAO operatorDAO = new OperatorDAO();
      return operatorDAO.getList(countryCode);
     }
     
}
