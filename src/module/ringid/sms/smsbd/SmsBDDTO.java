package module.ringid.sms.smsbd;

public class SmsBDDTO {

    private String ip;
    private int serverPort;

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public String getIp() {
        return ip;
    }

    public int getServerPort() {
        return serverPort;
    }

}
