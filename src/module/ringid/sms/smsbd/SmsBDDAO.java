package module.ringid.sms.smsbd;

//import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import module.ringid.sms.utils.Constants;
import org.apache.log4j.Logger;

public class SmsBDDAO {

    private static final Logger LOGGER = Logger.getLogger("com.ringid.sms");
    private ArrayList<String> smsBdIpList;
    private int index = 0;
    private long LOADING_TIME;

    public SmsBDDAO() {
        reload();
    }

    public static SmsBDDAO getInstance() {
        return SmsBDDAO_InstanceHolder.INSTANCE;
    }

    private static class SmsBDDAO_InstanceHolder {

        private static SmsBDDAO INSTANCE = new SmsBDDAO();
    }

    private synchronized boolean reload() {
        if (System.currentTimeMillis() - LOADING_TIME < Constants.ONE_MINUTE) {
            return true;
        }

        authdbconnector.DBConnection db = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean reloaded = false;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "select serverIP, serverPort from smsbdservers where serverStatus = 1";
            ps = db.connection.prepareStatement(sql);
            rs = ps.executeQuery();
            ArrayList<String> smsBdIpListLocal = new ArrayList<>();
            StringBuilder sb;
            while (rs.next()) {
                try {
                    sb = new StringBuilder().append("http://").append(rs.getString("serverIP")).append(":").append(rs.getInt("serverPort")).append("/");
                    LOGGER.info("smsbd IP : " + sb);
                    smsBdIpListLocal.add(sb.toString());
                } catch (SQLException e) {
                    LOGGER.error("SQLException <smsbdservers reload> ", e);
                } catch (Exception e) {
                    LOGGER.error("Exception <smsbdservers reload> ", e);
                }
            }
            smsBdIpList = smsBdIpListLocal;
            index = 0;
            LOGGER.debug("<smsbdservers> loaded : @ " + new Date());
            LOADING_TIME = System.currentTimeMillis();
            reloaded = true;
        } catch (Exception ex) {
            LOGGER.error("Exception in <smsbdservers>-->", ex);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception ex) {
            }
        }
        return reloaded;
    }

    public boolean forceReload() {
        LOADING_TIME = 0;
        return reload();
    }

    public synchronized String getUrl() {
        if (smsBdIpList == null || smsBdIpList.isEmpty()) {
            LOGGER.fatal("doLoadBalance true BUT getUrl NULL");
            return null;
        }
        String url = smsBdIpList.get(index);
        if (++index >= smsBdIpList.size()) {
            index = 0;
        }
        return url;
    }

//    public static void main(String[] args) {
//        String s = "http://smsbd.ringid.com/sendSMS";
//        String apiUrl = "http://103.26.115.148/";
//        apiUrl = new StringBuilder(apiUrl).append(s.substring(s.indexOf(".com/") + ".com/".length())).toString();
//              
//        System.out.println(apiUrl);
//    }
}
