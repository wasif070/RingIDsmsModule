/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package module.ringid.sms;

import module.ringid.sms.utils.MyAppError;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.Logger;
import utilsdbconnector.DBConnection;

public abstract class BaseDAO {

    static Logger loggerBaseDAO = Logger.getLogger("com.ringid.sms");

    protected DBConnection db = null;
    protected PreparedStatement ps = null;
    protected Statement stmt = null;
    protected ResultSet rs = null;
    protected MyAppError error = new MyAppError();

    public void close() {

        try {
            if (ps != null) {
                ps.close();
            }
        } catch (Exception e) {
            loggerBaseDAO.error("ps Exception [BaseDAO] --> " + e);

        }

        try {
            if (rs != null) {
                rs.close();
            }
        } catch (Exception e) {
            loggerBaseDAO.error("rs Exception [BaseDAO] --> " + e);
        }

        try {
            if (stmt != null) {
                stmt.close();
            }
        } catch (Exception e) {
            loggerBaseDAO.error("stmt Exception [BaseDAO] --> " + e);
        }

        try {
            if (db != null && db.connection != null) {
                utilsdbconnector.DBConnector.getInstance().freeConnection(db);
            }
        } catch (Exception e) {
            loggerBaseDAO.error("db Exception [BaseDAO] --> " + e);
        }
    }
}
