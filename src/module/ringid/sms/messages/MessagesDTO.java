package module.ringid.sms.messages;

import java.util.ArrayList;
import java.util.Comparator;

public class MessagesDTO extends BaseDTO {

    private int id;
    private String routesName;
    private String destination;
    private String message;
    private String sms;
    private String brandResponse;
    private ArrayList<MessagesDTO> responseList = new ArrayList<>();
    private String date;
    private String receivedDate;
    private int nSuccess;
    private int nFailed;
    private String vCode;
    private String responseId;
    private int verifiedSend;
    private String countryName;
    private int dlStatus = 0;
    private String sendResult;
    private String msgId;
    private String dbInsert;
    private double smsRate;
    private String deliveryStatusText;
    private long addTime;

    public MessagesDTO() {
        this.dlStatus = 0;
    }

    public int getDlStatus() {
        return dlStatus;
    }

    public void setDlStatus(int dlStatus) {
        this.dlStatus = dlStatus;
    }

    public String getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(String receivedDate) {
        this.receivedDate = receivedDate;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getResponseId() {
        return responseId;
    }

    public void setResponseId(String responseId) {
        this.responseId = responseId;
    }

    public int getVerifiedSend() {
        return verifiedSend;
    }

    public void setVerifiedSend(int verifiedSend) {
        this.verifiedSend = verifiedSend;
    }

    public String getVerificationCode() {
        return vCode;
    }

    public void setVerificationCode(String varificationCode) {
        this.vCode = varificationCode;
    }

    public int getnSuccess() {
        return nSuccess;
    }

    public void setnSuccess(int nSuccess) {
        this.nSuccess = nSuccess;
    }

    public int getnFailed() {
        return nFailed;
    }

    public void setnFailed(int nFailed) {
        this.nFailed = nFailed;
    }

    public String getSms() {
        return sms;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setSms(String sms) {
        this.sms = sms;
    }

    public String getRoutesName() {
        return routesName;
    }

    public void setRoutesName(String routesName) {
        this.routesName = routesName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMobileNo() {
        return destination;
    }

    public void setMobileNo(String mobileNo) {
        this.destination = mobileNo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBrandResponse() {
        return brandResponse;
    }

    public void setBrandResponse(String brandResponse) {
        this.brandResponse = brandResponse;
    }

    public ArrayList<MessagesDTO> getResponseList() {
        return responseList;
    }

    public void setResponseList(ArrayList<MessagesDTO> responseList) {
        this.responseList = responseList;
    }

    public static class CompIdDSC implements Comparator<MessagesDTO> {

        @Override
        public int compare(MessagesDTO arg0, MessagesDTO arg1) {
            return arg1.id - arg0.id;
        }
    }

    public static class CompIdASC implements Comparator<MessagesDTO> {

        @Override
        public int compare(MessagesDTO arg0, MessagesDTO arg1) {
            return arg0.id - arg1.id;
        }
    }

    public String getSendResult() {
        return sendResult;
    }

    public void setSendResult(String sendResult) {
        this.sendResult = sendResult;
    }

    public String getMessageId() {
        return msgId;
    }

    public void setMessageId(String messageId) {
        this.msgId = messageId;
    }

    public String isDbInsert() {
        return dbInsert;
    }

    public void setDbInsert(String dbInsert) {
        this.dbInsert = dbInsert;
    }

    public double getSmsRate() {
        return smsRate;
    }

    public void setSmsRate(double smsRate) {
        this.smsRate = smsRate;
    }

    public String getDeliveryStatusText() {
        return deliveryStatusText;
    }

    public void setDeliveryStatusText(String deliveryStatusText) {
        this.deliveryStatusText = deliveryStatusText;
    }

    public long getAddTime() {
        return addTime;
    }

    public void setAddTime(long addTime) {
        this.addTime = addTime;
    }

    @Override
    public String toString() {
        return "MessagesDTO{" + "id=" + id + ", routesName=" + routesName + ", destination=" + destination + ", message=" + message + ", sms=" + sms + ", brandResponse=" + brandResponse + ", responseList=" + responseList + ", date=" + date + ", receivedDate=" + receivedDate + ", nSuccess=" + nSuccess + ", nFailed=" + nFailed + ", vCode=" + vCode + ", responseId=" + responseId + ", verifiedSend=" + verifiedSend + ", countryName=" + countryName + ", dlStatus=" + dlStatus + ", sendResult=" + sendResult + ", msgId=" + msgId + ", dbInsert=" + dbInsert + ", smsRate=" + smsRate + ", deliveryStatusText=" + deliveryStatusText + '}';
    }
}
