package module.ringid.sms.messages;

import java.util.ArrayList;
import module.ringid.sms.dto.SearchDTO;
import module.ringid.sms.dto.SummaryDTO;
import module.ringid.sms.utils.MyAppError;

public class MessageScheduler {

    private static MessageScheduler scheduler;

    public static MessageScheduler getInstance() {
        if (scheduler == null) {
            scheduler = new MessageScheduler();
        }
        return scheduler;
    }

    public int getAllMessageCountBeforeDate(String date) {
        MessagesDAO dao = new MessagesDAO();
        return dao.getAllMessageCountBeforeDate(date);
    }

    public MyAppError deleteAllMessageBeforeDate(String data) {
        MessagesDAO dao = new MessagesDAO();
        return dao.deleteAllMessageBeforeDate(data);
    }

    public MyAppError addMessage(MessagesDTO messagesDTO) {
        MessagesDAO messageDAO = new MessagesDAO();
        return messageDAO.addMessage(messagesDTO);
    }

// Update delivery status of a sms
    public MyAppError updateDeliverySMSStatus(MessagesDTO messagesDTO) throws Exception {
        MessagesDAO messageDAO = new MessagesDAO();
        return messageDAO.updateDeliverySMSStatus(messagesDTO);
    }

    //Called when a message is verified
    public MyAppError updateSuccessfulMessageVerificationStatus(MessagesDTO dto) {
        MessagesDAO messageDAO = new MessagesDAO();
        return messageDAO.updateSuccessfulMessageverificationStatus(dto);
    }

    //returns verification-code which has been sent to this mobile number
    public String verificationCode(String mobileNo) {
        MessagesDAO messagesDAO = new MessagesDAO();
        return messagesDAO.verificationCode(mobileNo);
    }

    public synchronized MessagesDTO getMessage(Integer brandId, String msgId) {
        MessagesDAO messageDAO = new MessagesDAO();
        return messageDAO.getMessage(brandId, msgId);
    }

    //returns messages count
    public synchronized Integer getMessagesCount(SearchDTO sdto) {
        MessagesDAO messageDAO = new MessagesDAO();
        return messageDAO.getMessagesCount(sdto);
    }

    //returns messages list
    public synchronized ArrayList<MessagesDTO> getMessagesList(long start, int limit, SearchDTO sdto) {
        MessagesDAO messageDAO = new MessagesDAO();
        return messageDAO.getMessagesList(start, limit, sdto);
    }

    //returns summary
    public ArrayList<SummaryDTO> getSummaryList(SearchDTO dto) {
        MessagesDAO messageDAO = new MessagesDAO();
        return messageDAO.getNewSummaryList(dto);
    }

    public MessagesDTO getMessageById(int id) {
        MessagesDAO messageDAO = new MessagesDAO();
        return messageDAO.getMessageById(id);
    }

}
