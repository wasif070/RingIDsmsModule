//package module.ringid.sms.messages;
//
//import java.text.SimpleDateFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//
//public class MessageLoader {
//
//    private ArrayList<MessagesDTO> messages;
//
//    public MessageLoader() {
//        reload();
//    }
//
//    public static MessageLoader getInstance() {
//        return MessageLoader_SingletonHolder.INSTANCE;
//    }
//
//    private static class MessageLoader_SingletonHolder {
//
//        private static MessageLoader INSTANCE = new MessageLoader();
//    }
//
//    public boolean reload() {
//        Calendar calendar = Calendar.getInstance();
//        calendar.set(Calendar.HOUR, 0);
//        calendar.set(Calendar.MINUTE, 0);
//        calendar.set(Calendar.SECOND, 0);
//        calendar.set(Calendar.MILLISECOND, 0);
//        calendar.add(Calendar.DATE, -1);
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        String dayBeforeToday = sdf.format(calendar.getTime());
//        System.out.println("" + dayBeforeToday);
//        MessagesDAO messagesDAO = new MessagesDAO();
//        ArrayList<MessagesDTO> messagesList = messagesDAO.getMessagesListSinceAParticularTime(dayBeforeToday, true);
//        messagesList.sort((m1, m2) -> m2.getId() - m1.getId());
//        return false;
//    }
//
//    public static void main(String[] args) {
////        MessageLoader.getInstance();
//        try {
//            String time = "2018-05-19 11:30:57";
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//            Date date_ = sdf.parse(time);
//            System.out.println(""+date_);
//            System.out.println(""+date_.getTime());
//        } catch (Exception e) {
//        }
//    }
//}
