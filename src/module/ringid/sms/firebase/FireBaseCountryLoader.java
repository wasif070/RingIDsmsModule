/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package module.ringid.sms.firebase;

import authdbconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import module.ringid.sms.BaseDAO;
import module.ringid.sms.country.CountryTaskScheduler;
import module.ringid.sms.utils.Constants;
import org.apache.log4j.Logger;

/**
 *
 * @author Rabby
 */
public class FireBaseCountryLoader extends BaseDAO {

    private static FireBaseCountryLoader instance;
    static Logger logger = Logger.getLogger("com.ringid.sms");
    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Integer, FireBaseCountryDTO> fireBaseCountryList;

    public FireBaseCountryLoader() {
        fireBaseCountryList = new HashMap<>();
    }

    public static FireBaseCountryLoader getInstance() {
        if (instance == null) {
            instance = new FireBaseCountryLoader();
        }
        return instance;
    }

    private void loadData() {
        fireBaseCountryList = new HashMap<>();
        DBConnection db = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id, country_code, platform, active FROM firebase_country";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                FireBaseCountryDTO dto = new FireBaseCountryDTO();
                dto.setId(rs.getInt("id"));
                dto.setCountryCode(rs.getString("country_code"));
                dto.setCountryName(CountryTaskScheduler.getInstance().getCountryInfoByCode(dto.getCountryCode()).getName());
                dto.setPlatform(rs.getString("platform"));
                dto.setActive(rs.getInt("active"));
                dto.setPlatforms(dto.getPlatform() != null && dto.getPlatform().length() > 0 ? dto.getPlatform().split(",") : null);
                fireBaseCountryList.put(dto.getId(), dto);
            }
        } catch (Exception e) {
            logger.error("Exception in FireBaseCountryLoader --> " + e);
        } finally {
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
            close();
        }
        logger.info("FireBaseCountryList.size() --> " + fireBaseCountryList.size());
    }

    public void forceLoad() {
        loadingTime = System.currentTimeMillis();
        loadData();
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            loadData();
        }
    }

    public List<FireBaseCountryDTO> getFirebaseCountryList(FireBaseCountryDTO sdto) {
        checkForReload();
        List<FireBaseCountryDTO> baseCountryDTOs = new ArrayList<>();
        for (Map.Entry<Integer, FireBaseCountryDTO> entry : fireBaseCountryList.entrySet()) {
            FireBaseCountryDTO dto = entry.getValue();
            if (sdto != null && sdto.isSearchByCountryCode() && !dto.getCountryCode().contains(sdto.getCountryCode())) {
                continue;
            }
            baseCountryDTOs.add(dto);
        }
        return baseCountryDTOs;
    }

    public List<FireBaseCountryDTO> getFirebaseCountryList(int columnVal, int sortType, FireBaseCountryDTO sdto) {
        checkForReload();
        List<FireBaseCountryDTO> baseCountryDTOs = new ArrayList<>();
        for (Map.Entry<Integer, FireBaseCountryDTO> entry : fireBaseCountryList.entrySet()) {
            FireBaseCountryDTO dto = entry.getValue();
            if (sdto != null && sdto.isSearchByCountryCode() && !dto.getCountryCode().contains(sdto.getCountryCode())) {
                continue;
            }
            baseCountryDTOs.add(dto);
        }
        switch (columnVal) {
            case Constants.COLUMN_ONE:
                if (sortType == Constants.ASC_SORT) {
                    Collections.sort(baseCountryDTOs, new FireBaseCountryDTO.CompIdASC());
                } else {
                    Collections.sort(baseCountryDTOs, new FireBaseCountryDTO.CompIdDSC());
                }
                break;
            default:
                Collections.sort(baseCountryDTOs, new FireBaseCountryDTO.CompIdDSC());
        }
        return baseCountryDTOs;
    }

    public FireBaseCountryDTO getFireBaseCountryById(int id) {
        checkForReload();
        if (fireBaseCountryList.containsKey(id)) {
            return fireBaseCountryList.get(id);
        }
        return null;
    }

}
