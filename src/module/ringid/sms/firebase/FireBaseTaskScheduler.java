    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package module.ringid.sms.firebase;

import java.util.List;
import module.ringid.sms.dto.SearchDTO;
import module.ringid.sms.utils.MyAppError;

/**
 *
 * @author Rabby
 */
public class FireBaseTaskScheduler {

    private static FireBaseTaskScheduler instance;

    public static FireBaseTaskScheduler getInstance() {
        if (instance == null) {
            instance = new FireBaseTaskScheduler();
        }
        return instance;
    }

    public List<FirebaseSmsDTO> getFirebaseSmsList(SearchDTO dto, int start, int limit) {
        FireBaseDAO dao = new FireBaseDAO();
        return dao.getFirebaseSmsList(dto, start, limit);
    }
    
    public Integer getFirebaseSmsCount(SearchDTO dto) {
        FireBaseDAO dao = new FireBaseDAO();
        return dao.getFirebaseSmsCount(dto);
    }

    public List<FirebaseSmsSummaryDTO> getFirebaseSmsSummaryDTO(SearchDTO dto) {
        FireBaseDAO dao = new FireBaseDAO();
        return dao.getFirebaseSmsSummay(dto);
    }

    public List<FireBaseCountryDTO> getFirebaseCountryList(FireBaseCountryDTO dto) {
        return FireBaseCountryLoader.getInstance().getFirebaseCountryList(dto);
    }

    public List<FireBaseCountryDTO> getFirebaseCountryList(int columnVal, int sortType, FireBaseCountryDTO dto) {
        return FireBaseCountryLoader.getInstance().getFirebaseCountryList(columnVal, sortType, dto);
    }

    public FireBaseCountryDTO getFirebaseCountryById(int id) {
        return FireBaseCountryLoader.getInstance().getFireBaseCountryById(id);
    }

    public MyAppError addFireBaseCountry(FireBaseCountryDTO dto) {
        FireBaseDAO dao = new FireBaseDAO();
        return dao.addFirebaseCountry(dto);
    }

    public MyAppError updateFirebaseCountry(FireBaseCountryDTO dto) {
        FireBaseDAO dao = new FireBaseDAO();
        return dao.updateFirebaseCountry(dto);
    }

    public MyAppError deleteFirebaseCountry(int id) {
        FireBaseDAO dao = new FireBaseDAO();
        return dao.deleteFirebaseCountry(id);
    }
}
