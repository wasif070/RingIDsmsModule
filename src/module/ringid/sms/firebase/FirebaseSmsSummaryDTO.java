/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package module.ringid.sms.firebase;

/**
 *
 * @author Rabby
 */
public class FirebaseSmsSummaryDTO {

    private String mobileDialingCode;
    private String countryName;
    private int totalSms;
    private int successSms;
    private int failedSms;
    private int successRate;

    public FirebaseSmsSummaryDTO() {
        totalSms = 0;
        successSms = 0;
        failedSms = 0;
        successRate = 0;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getMobileDialingCode() {
        return mobileDialingCode;
    }

    public void setMobileDialingCode(String mobileDialingCode) {
        this.mobileDialingCode = mobileDialingCode;
    }

    public int getTotalSms() {
        return totalSms;
    }

    public void setTotalSms(int totalSms) {
        this.totalSms = totalSms;
    }

    public int getSuccessSms() {
        return successSms;
    }

    public void setSuccessSms(int successSms) {
        this.successSms = successSms;
    }

    public int getFailedSms() {
        return failedSms;
    }

    public void setFailedSms(int failedSms) {
        this.failedSms = failedSms;
    }

    public int getSuccessRate() {
        return successRate;
    }

    public void setSuccessRate(int successRate) {
        this.successRate = successRate;
    }
}
