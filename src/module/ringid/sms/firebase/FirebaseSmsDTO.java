/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package module.ringid.sms.firebase;

/**
 *
 * @author Rabby
 */
public class FirebaseSmsDTO {

    private String deviceId;
    private String ringId;
    private String mobileDialingCode;
    private String mobileNumber;
    private int status;
    private String entryTime;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getRingId() {
        return ringId;
    }

    public void setRingId(String ringId) {
        this.ringId = ringId;
    }

    public String getMobileDialingCode() {
        return mobileDialingCode;
    }

    public void setMobileDialingCode(String mobileDialingCode) {
        this.mobileDialingCode = mobileDialingCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(String entryTime) {
        this.entryTime = entryTime;
    }

}
