/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package module.ringid.sms.firebase;

import java.util.Comparator;

/**
 *
 * @author Rabby
 */
public class FireBaseCountryDTO {

    private int id;
    private String countryCode;
    private String countryName;
    private String platform;
    private String[] platforms;
    private int active;
    private boolean searchByCountryCode;

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String[] getPlatforms() {
        return platforms;
    }

    public void setPlatforms(String[] platforms) {
        this.platforms = platforms;
    }

    public boolean isSearchByCountryCode() {
        return searchByCountryCode;
    }

    public void setSearchByCountryCode(boolean searchByCountryCode) {
        this.searchByCountryCode = searchByCountryCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public static class CompIdDSC implements Comparator<FireBaseCountryDTO> {

        @Override
        public int compare(FireBaseCountryDTO arg0, FireBaseCountryDTO arg1) {
            return arg1.getCountryCode().compareTo(arg0.getCountryCode());
        }
    }

    public static class CompIdASC implements Comparator<FireBaseCountryDTO> {

        @Override
        public int compare(FireBaseCountryDTO arg0, FireBaseCountryDTO arg1) {
            return arg0.getCountryCode().compareTo(arg1.getCountryCode());
        }
    }

}
