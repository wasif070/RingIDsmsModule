/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package module.ringid.sms.firebase;

import authdbconnector.DBConnection;
import com.google.gson.Gson;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import module.ringid.sms.BaseDAO;
import module.ringid.sms.country.CountryLoader;
import module.ringid.sms.dto.SearchDTO;
import module.ringid.sms.utils.MyAppError;
import org.apache.log4j.Logger;

/**
 *
 * @author Rabby
 */
public class FireBaseDAO extends BaseDAO {

    static Logger logger = Logger.getLogger("com.ringid.sms");

    public List<FirebaseSmsDTO> getFirebaseSmsList(SearchDTO searchDTO, int start, int limit) {
        List<FirebaseSmsDTO> smsList = new ArrayList<>();
        String startDate = searchDTO.getFromDate();
        String endDate = searchDTO.getToDate();
        endDate += " 23:59:59";
        String optStr = "";
        if (searchDTO.isSearchByCountryCode()) {
            optStr += " AND mblDialingCode = '" + searchDTO.getCountryCode() + "' ";
        }
        DBConnection db = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT ringid, deviceid, sms_status, mblDialingCode, mblNumber, entryTime FROM firebase_sms_count WHERE entrytime BETWEEN '" + startDate + "' AND '" + endDate + "' " + optStr + " ORDER BY entryTime DESC LIMIT " + start + "," + limit;
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                FirebaseSmsDTO smsDTO = new FirebaseSmsDTO();
                smsDTO.setDeviceId(rs.getString("deviceid"));
                smsDTO.setRingId(rs.getString("ringid"));
                smsDTO.setMobileDialingCode(rs.getString("mblDialingCode"));
                smsDTO.setMobileNumber(rs.getString("mblNumber"));
                smsDTO.setStatus(rs.getInt("sms_status"));
                smsDTO.setEntryTime(rs.getString("entryTime"));
                smsList.add(smsDTO);
            }
        } catch (Exception e) {
            logger.error("Exception in getFirebaseSmsList --> " + e);
        } finally {
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
            close();
        }
        return smsList;
    }

    public Integer getFirebaseSmsCount(SearchDTO searchDTO) {
        Integer smsCount = 0;
        String startDate = searchDTO.getFromDate();
        String endDate = searchDTO.getToDate();
        endDate += " 23:59:59";
        String optStr = "";
        if (searchDTO.isSearchByCountryCode()) {
            optStr += " AND mblDialingCode = '" + searchDTO.getCountryCode() + "' ";
        }
        //endDate += " 23:59:59";
        DBConnection db = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT count(1) AS total_sms FROM firebase_sms_count WHERE entrytime BETWEEN '" + startDate + "' AND '" + endDate + "' " + optStr;
            logger.debug("FB-SQL02 : " + sql);
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                smsCount = rs.getInt("total_sms");
            }
        } catch (Exception e) {
            logger.error("Exception in getFirebaseSmsList --> " + e);
        } finally {
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
            close();
        }
        return smsCount;
    }

    public List<FirebaseSmsSummaryDTO> getFirebaseSmsSummay(SearchDTO searchDTO) {
        List<FirebaseSmsSummaryDTO> summaryList = new ArrayList<>();
        HashMap<String, FirebaseSmsSummaryDTO> summaryMap = new HashMap<>();
        String startDate = searchDTO.getFromDate();
        String endDate = searchDTO.getToDate();
        endDate += " 23:59:59";
        String optStr = "";
        if (searchDTO.isSearchByCountryCode()) {
            optStr += " AND mblDialingCode = '" + searchDTO.getCountryCode() + "' ";
        }
        DBConnection db = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            //total sms
            String sql = "SELECT mblDialingCode, count(1) AS total_sms FROM firebase_sms_count WHERE entrytime BETWEEN '" + startDate + "' AND '" + endDate + "' " + optStr + " GROUP BY mblDialingCode";
            logger.debug("FB-SQL03 : " + sql);
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                if (summaryMap.containsKey(rs.getString("mblDialingCode"))) {
                    FirebaseSmsSummaryDTO summaryDTO = summaryMap.get(rs.getString("mblDialingCode"));
                    summaryDTO.setMobileDialingCode(rs.getString("mblDialingCode"));
                    summaryDTO.setTotalSms(rs.getInt("total_sms"));
                    summaryMap.put(summaryDTO.getMobileDialingCode(), summaryDTO);
                } else {
                    FirebaseSmsSummaryDTO summaryDTO = new FirebaseSmsSummaryDTO();
                    summaryDTO.setMobileDialingCode(rs.getString("mblDialingCode"));
                    summaryDTO.setCountryName(CountryLoader.getInstance().getCountryDTO(summaryDTO.getMobileDialingCode()).getName());
                    summaryDTO.setTotalSms(rs.getInt("total_sms"));
                    summaryMap.put(summaryDTO.getMobileDialingCode(), summaryDTO);
                }
            }
            //success sms
            sql = "SELECT mblDialingCode, count(1) AS success_sms FROM firebase_sms_count WHERE sms_status = 1 AND entrytime BETWEEN '" + startDate + "' AND '" + endDate + "' " + optStr + " GROUP BY mblDialingCode";
            logger.debug("FB-SQL04 : " + sql);
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                if (summaryMap.containsKey(rs.getString("mblDialingCode"))) {
                    FirebaseSmsSummaryDTO summaryDTO = summaryMap.get(rs.getString("mblDialingCode"));
                    summaryDTO.setMobileDialingCode(rs.getString("mblDialingCode"));
                    summaryDTO.setSuccessSms(rs.getInt("success_sms"));
                    summaryMap.put(summaryDTO.getMobileDialingCode(), summaryDTO);
                } else {
                    FirebaseSmsSummaryDTO summaryDTO = new FirebaseSmsSummaryDTO();
                    summaryDTO.setMobileDialingCode(rs.getString("mblDialingCode"));
                    summaryDTO.setCountryName(CountryLoader.getInstance().getCountryDTO(summaryDTO.getMobileDialingCode()).getName());
                    summaryDTO.setSuccessSms(rs.getInt("success_sms"));
                    summaryMap.put(summaryDTO.getMobileDialingCode(), summaryDTO);
                }
            }
            //failed sms
            sql = "SELECT mblDialingCode, count(1) AS failed_sms FROM firebase_sms_count WHERE sms_status = 0 AND entrytime BETWEEN '" + startDate + "' AND '" + endDate + "' " + optStr + " GROUP BY mblDialingCode";
            logger.debug("FB-SQL05 : " + sql);
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                if (summaryMap.containsKey(rs.getString("mblDialingCode"))) {
                    FirebaseSmsSummaryDTO summaryDTO = summaryMap.get(rs.getString("mblDialingCode"));
                    summaryDTO.setMobileDialingCode(rs.getString("mblDialingCode"));
                    summaryDTO.setFailedSms(rs.getInt("failed_sms"));
                    summaryMap.put(summaryDTO.getMobileDialingCode(), summaryDTO);
                } else {
                    FirebaseSmsSummaryDTO summaryDTO = new FirebaseSmsSummaryDTO();
                    summaryDTO.setMobileDialingCode(rs.getString("mblDialingCode"));
                    summaryDTO.setCountryName(CountryLoader.getInstance().getCountryDTO(summaryDTO.getMobileDialingCode()).getName());
                    summaryDTO.setFailedSms(rs.getInt("failed_sms"));
                    summaryMap.put(summaryDTO.getMobileDialingCode(), summaryDTO);
                }
            }

            for (Map.Entry<String, FirebaseSmsSummaryDTO> entry : summaryMap.entrySet()) {
                summaryList.add(entry.getValue());
            }
        } catch (Exception e) {
            logger.error("Exception in getFirebaseSmsSummay --> " + e);
        } finally {
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
            close();
        }
        return summaryList;
    }

    public MyAppError addFirebaseCountry(FireBaseCountryDTO dto) {
        error = new MyAppError();
        DBConnection db = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT country_code From firebase_country WHERE country_code = '" + dto.getCountryCode() + "'";
            logger.debug("FB-SQL06 : " + sql);
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                error.setERROR_TYPE(MyAppError.OTHERERROR);
                error.setErrorMessage("Country code alreay exits.");
                throw new Exception("Duplicate content");
            }
            sql = "INSERT INTO firebase_country(country_code, platform, active) VALUES(?, ?, ?)";
            logger.debug("FB-SQL07 : " + sql);
            ps = db.connection.prepareStatement(sql);
            int i = 1;
            ps.setString(i++, dto.getCountryCode());
            ps.setString(i++, dto.getPlatform());
            ps.setInt(i++, dto.getActive());
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.error("Exception in addFireBaseCountry --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            logger.error("Exception in addFireBaseCountry --> " + e);
        } finally {
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
            close();
        }
        return error;
    }

    public MyAppError updateFirebaseCountry(FireBaseCountryDTO dto) {
        error = new MyAppError();
        DBConnection db = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "UPDATE firebase_country SET country_code = ?, platform = ?, active = ? WHERE id = ?";
            logger.debug("FB-SQL08 : " + sql);
            ps = db.connection.prepareStatement(sql);
            int i = 1;
            ps.setString(i++, dto.getCountryCode());
            ps.setString(i++, dto.getPlatform());
            ps.setInt(i++, dto.getActive());
            ps.setInt(i++, dto.getId());
            int res = ps.executeUpdate();
            if (res > 0) {
                logger.info("Update successfull -> " + new Gson().toJson(dto));
            }
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.error("Exception in updateFireBaseCountry --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            logger.error("Exception in updateFireBaseCountry --> " + e);
        } finally {
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
            close();
        }
        return error;
    }

    public MyAppError deleteFirebaseCountry(int id) {
        error = new MyAppError();
        DBConnection db = null;
        try {
            db = authdbconnector.DBConnector.getInstance().makeConnection();
            String sql = "DELETE FROM firebase_country WHERE id = ?";
            logger.debug("FB-SQL09 : " + sql);
            ps = db.connection.prepareStatement(sql);
            int i = 1;
            ps.setInt(i++, id);
            int res = ps.executeUpdate();
            if (res > 0) {
                logger.info("Delete successfull -> id : " + id);
            }
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.error("Exception in deleteFirebaseCountry --> " + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            logger.error("Exception in deleteFirebaseCountry --> " + e);
        } finally {
            try {
                if (db != null) {
                    authdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
            close();
        }
        return error;
    }

}
