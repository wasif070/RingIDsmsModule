package module.ringid.sms.routes;

import module.ringid.sms.dto.BalanceFeedBack;
import module.ringid.sms.routes.brands.Nexmo;
import module.ringid.sms.utils.Constants;

public class BalanceChecker {

    public static BalanceFeedBack checkBalance(int id) {
        BalanceFeedBack feedBack;
        switch (id) {
            case Constants.NEXMO:
                feedBack = Nexmo.getRemainingBalance(id);
                break;
            default: {
                feedBack = new BalanceFeedBack();
                feedBack.setReasonCode(404);
            }
        }
        return feedBack;
    }
}
