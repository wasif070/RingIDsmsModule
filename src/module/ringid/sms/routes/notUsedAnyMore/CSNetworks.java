package module.ringid.sms.routes.notUsedAnyMore;

import module.ringid.sms.messages.MessagesDTO;
import module.ringid.sms.routes.SMSSendManager;
import module.ringid.sms.smsBrands.BrandsDTO;
import module.ringid.sms.smsBrands.BrandsTaskScheduler;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import module.ringid.sms.dto.SmsStatusDTO;
import module.ringid.sms.utils.Constants;

public class CSNetworks extends SMSSendManager {

    private static Logger logger = Logger.getLogger("com.ringid.sms");
//    private BrandsDTO brandDTO = new BrandsDTO();

    public String result = Constants.SUCCESS;

    @Override
    public MessagesDTO sendSMS(int countryId, String vCode, int brandId, String mobileNumber, String msg) {
        MessagesDTO messagesDTO = new MessagesDTO();

        try {
            // (1). brandDTO
            brandDTO = BrandsTaskScheduler.getInstance().getBrandDTO(brandId);
            // (2). Construct data
            String data = prepareData(brandDTO, msg, mobileNumber);

            // (3)-1. Preparing messageDTO
            //messagesDTO.//setSmsRate(rate);
            messagesDTO.setCountryId(countryId);
            messagesDTO.setVerificationCode(vCode);
            messagesDTO.setVerifiedSend(0);
            messagesDTO.setBrandId(brandId);
            messagesDTO.setMobileNo(mobileNumber);
//            double balance = brandDTO.getBalance() - rate;

            String sentTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
            logger.debug("sent Date:" + sentTime);
            messagesDTO.setDate(sentTime);
            messagesDTO.setMessage(msg);

            // (4). Send data
            String response = sendData(data, brandDTO.getApiUrl());

            // (3)-2. Preparing messageDTO
            messagesDTO.setBrandResponse(response);

            logger.debug("CSNetworks response : " + response);

            // (5). Response analysis
            if ((response.substring(response.length() - 2, response.length())).equalsIgnoreCase("OK")) {
                result = Constants.SUCCESS;
                final String apiMsgID = response.substring(0, response.length() - 2);
                messagesDTO.setResponseId(apiMsgID);
                logger.info("msg submitted - waiting for confirmation[CSNetwork]");
            } else {
                result = Constants.FAILED;
                messagesDTO.setDlStatus(-1);
                messagesDTO.setVerifiedSend(-1);
                messagesDTO.setResponseId("-1");
                logger.error("Message submission failed : " + response);
                logger.error("Message content : " + msg);
            }
        } catch (Exception e) {
            result = Constants.FAILED;
            messagesDTO.setDlStatus(-1);
            messagesDTO.setVerifiedSend(-1);
            messagesDTO.setResponseId("-1");
            String errorText = e.toString();
            if(errorText == null){
                errorText = "Sms sending failed.";
            }
            logger.error("Error SMS ", e);
            messagesDTO.setBrandResponse(messagesDTO.getBrandResponse() != null && messagesDTO.getBrandResponse().length() < 1 ? errorText : messagesDTO.getBrandResponse());
        }

        // (6). add to DB
        messagesDTO.setSmsStatus(result);
        messagesDTO.setDbInsert(addToDB(messagesDTO));

        return messagesDTO;
    }

//    logger.error("(CSNetwork) DB insert failed (Exception) --> " + e + "\n" + messagesDTO.toString());
    @Override
    public String prepareData(BrandsDTO brandDTO, String msg, String mobileNumber) {
        StringBuilder stringbuilder = new StringBuilder();
        stringbuilder
                .append("USERNAME=").append(brandDTO.getUserName())
                .append("&PASSWORD=").append(brandDTO.getPassword())
                .append("&MESSAGE=").append(msg)
                .append("&SOURCEADDR=").append(brandDTO.getSenderId())
                .append("&DESTADDR=").append(mobileNumber);
        return stringbuilder.toString();
    }

    @Override
    public String prepareDeliveryStatusData(BrandsDTO brandDTO, String messageId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SmsStatusDTO parseResponse(String response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
