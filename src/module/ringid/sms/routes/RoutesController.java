package module.ringid.sms.routes;

import module.ringid.sms.routes.brands.RobiAxiataSMS;
import module.ringid.sms.routes.notUsedAnyMore.CSNetworks;
import module.ringid.sms.routes.brands.InfobipSMS;
import module.ringid.sms.routes.brands.Wavecell;
import module.ringid.sms.routes.brands.ClickatellApi;
import module.ringid.sms.routes.brands.SMSWaalaApi;
import module.ringid.sms.routes.brands.SMSCountry;
import module.ringid.sms.routes.brands.SMSRouter;
import module.ringid.sms.routes.brands.Cellent;
import module.ringid.sms.routes.brands.MobiSMS;
import module.ringid.sms.utils.Constants;
import org.apache.log4j.Logger;
import module.ringid.sms.messages.MessagesDTO;
import module.ringid.sms.routes.brands.GP;
import module.ringid.sms.routes.brands.MobiReachExRobi;
import module.ringid.sms.routes.brands.Nexmo;
import module.ringid.sms.routes.brands.OnnorokomSMS;
import module.ringid.sms.routes.brands.Twilio_;

public class RoutesController {

    private int brandID;
    private String mobileNumber;
    private String message;
    private String response;
//    private String result;
    private MessagesDTO messagesDTO;
    private String varificationCode;
    private int countryId;
//    private double smsRate;
    private static Logger logger = Logger.getLogger("com.ringid.sms");

    public RoutesController() {
    }

    public RoutesController(int countryId, String varificationCode, int brandId, String mobileNumber, String message) {
        this.brandID = brandId;
        this.mobileNumber = mobileNumber;
        this.message = message;
        this.varificationCode = varificationCode;
        this.countryId = countryId;
    }

    public MessagesDTO rController() {
        try {
            switch (brandID) {
                case Constants.CLICKATELL_API:
                    ClickatellApi clickatell = new ClickatellApi();
                    messagesDTO = clickatell.sendSMS(countryId, varificationCode, brandID, mobileNumber, message);
                    logger.debug("Response for clickatell: " + messagesDTO.getSmsStatus());
                    break;
                case Constants.SMS_ROUTER:
                    SMSRouter sMSRouter = new SMSRouter();
                    messagesDTO = sMSRouter.sendSMS(countryId, varificationCode, brandID, mobileNumber, message);
                    logger.debug("Response for sMSRouter: " + messagesDTO.getSmsStatus());
                    break;
                case Constants.SMS_CELLENT:
                    Cellent sCellent = new Cellent();
                    messagesDTO = sCellent.sendSMS(countryId, varificationCode, brandID, mobileNumber, message);
                    logger.debug("Response for SMSCellent: " + messagesDTO.getSmsStatus());
                    break;

                case Constants.MOBI_SMS:
                    MobiSMS mobileObject = new MobiSMS();
                    messagesDTO = mobileObject.sendSMS(countryId, varificationCode, brandID, mobileNumber, message);
                    logger.debug("Response for MobiSMS: " + messagesDTO.getSmsStatus());
                    break;
                case Constants.CS_NETWORKS:
                    CSNetworks csnNetworks = new CSNetworks();
                    messagesDTO = csnNetworks.sendSMS(countryId, varificationCode, brandID, mobileNumber, message);
                    logger.debug("Response for CSNetworks: " + messagesDTO.getSmsStatus());
                    break;

                case Constants.INFOBIP_SMS:
                    InfobipSMS infobipSMS = new InfobipSMS();
                    messagesDTO = infobipSMS.sendSMS(countryId, varificationCode, brandID, mobileNumber, message);
                    logger.debug("Response for InfobipSMS: " + messagesDTO.getSmsStatus());
                    break;
                case Constants.ROBIAXIATA_SMS:
                    RobiAxiataSMS robiAxiataSMS = new RobiAxiataSMS();
                    messagesDTO = robiAxiataSMS.sendSMS(countryId, varificationCode, brandID, mobileNumber, message);
                    logger.debug("Response for RobiAxiataSMS: " + messagesDTO.getSmsStatus());
                    break;
                case Constants.WAVECELL:
                    Wavecell wavecell = new Wavecell();
                    messagesDTO = wavecell.sendSMS(countryId, varificationCode, brandID, mobileNumber, message);
                    logger.debug("Response for Wavecell: " + messagesDTO.getSmsStatus());
                    break;
                case Constants.SMS_WAALA_API:
                    SMSWaalaApi smswaala = new SMSWaalaApi();
                    messagesDTO = smswaala.sendSMS(countryId, varificationCode, brandID, mobileNumber, message);
                    break;
                case Constants.SMS_COUNTRY:
                    SMSCountry smsCountry = new SMSCountry();
                    messagesDTO = smsCountry.sendSMS(countryId, varificationCode, brandID, mobileNumber, message);
                    break;
                case Constants.NEXMO:
                    Nexmo nexmo = new Nexmo();
                    messagesDTO = nexmo.sendSMS(countryId, varificationCode, brandID, mobileNumber, message);
                    logger.debug("Response for Nexmo: " + messagesDTO.getSmsStatus());
                    break;
                case Constants.GP:
                    GP gp = new GP();
                    messagesDTO = gp.sendSMS(countryId, varificationCode, brandID, mobileNumber, message);
                    logger.debug("Response for GP: " + messagesDTO.getSmsStatus());
                    break;
                case Constants.TWILIO:
                    Twilio_ twilio_ = new Twilio_();
                    messagesDTO = twilio_.sendSMS(countryId, varificationCode, brandID, mobileNumber, message);
                    logger.debug("Response for TWILIO : " + messagesDTO.getSmsStatus());
                    break;
                case Constants.MOBI_REACH_EX_ROBI:
                    MobiReachExRobi mobiReachExRobi = new MobiReachExRobi();
                    messagesDTO = mobiReachExRobi.sendSMS(countryId, varificationCode, brandID, mobileNumber, message);
                    logger.debug("Response for MOBI_REACH_EX_ROBI : " + messagesDTO.getSmsStatus());
                    break;
                case Constants.ONNO_ROKOM:
                    OnnorokomSMS onnorokomSMS = new OnnorokomSMS();
                    messagesDTO = onnorokomSMS.sendSMS(countryId, varificationCode, brandID, mobileNumber, message);
                    logger.debug("Response for ONNO_ROKOM : " + messagesDTO.getSmsStatus());
                    break;
            }
        } catch (Exception e) {
            logger.debug("Exception in RoutesController");
            messagesDTO.setSmsStatus("failed");
            return messagesDTO;
        }
        return messagesDTO;
    }

    public void setBrandID(int brandID) {
        this.brandID = brandID;
    }

    public String getMsgId() {
        String msgId = "";
        if (messagesDTO != null) {
            msgId = messagesDTO.getResponseId();
        }
        return msgId;
    }

    public int getCountryId() {
        return countryId;
    }

}
