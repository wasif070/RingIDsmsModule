package module.ringid.sms.routes;

import com.google.gson.Gson;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import module.ringid.sms.SMSDeliveryChecker;
import module.ringid.sms.country.CountryTaskScheduler;
import module.ringid.sms.dto.BrandListFetchDTO;
import module.ringid.sms.dto.ReturnDTO;
import module.ringid.sms.messages.MessageScheduler;
import module.ringid.sms.messages.MessagesDAO;
import module.ringid.sms.messages.MessagesDTO;
import module.ringid.sms.smsroutes.SMSRoutesTaskSchedular;
import module.ringid.sms.utils.Constants;
import module.ringid.sms.utils.MyAppError;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;
import module.ringid.sms.operators.OperatorDTO;
import module.ringid.sms.operators.OperatorLoader;
import module.ringid.voice.scheduler.VoiceTaskScheduler;

public class RoutesHandlerManager {

    private String response;
    private static final Logger LOGGER = Logger.getLogger("com.ringid.sms");

    public RoutesHandlerManager() {
    }

    public ReturnDTO sentSMSByRoutesId(String vCode, int brandId, String countryCode, String mobileNo, String message) {
        ReturnDTO returnDTO = new ReturnDTO();
        String dailNumber = "";
        try {
            dailNumber = countryCode.trim().concat(mobileNo.trim());
            if (dailNumber.startsWith("+")) {
                dailNumber = dailNumber.substring(1, dailNumber.length());
            }
            int countryId = CountryTaskScheduler.getInstance().getCountryId(countryCode);

            if (countryId > 0) {
                RoutesController routesController = new RoutesController(countryId, vCode, brandId, dailNumber, message);
                response = routesController.rController().getSmsStatus();
                LOGGER.debug("message sent by " + brandId + " and response " + response);
                //testing purpose [below code]
                if (response.equals("success")) {
                    SMSDeliveryChecker sMSDeliveryChecker = new SMSDeliveryChecker(brandId, routesController.getMsgId());
                    sMSDeliveryChecker.start();
                }
            } else {
                LOGGER.error("CountryId not found for countryCode --> " + countryCode);
                response = Constants.FAILED;
            }
        } catch (Exception e) {
            LOGGER.debug("execption in sentSMSByRoutesId" + e);
            response = Constants.FAILED;
        }
        returnDTO.setDestinationNumber(dailNumber);
        returnDTO.setDelivered(response.equals("success"));
        return returnDTO;
    }

    public ReturnDTO findRoutes(String vCode, String countryCode, String mobileNo, String message, int smsSendingAttempt) {
        LOGGER.debug("## smsSendingAttempt --> " + smsSendingAttempt + " --> countryCode --> " + countryCode + " --> mobileNo --> " + mobileNo);
        ReturnDTO returnDTO = new ReturnDTO();
        try {
            int countryId = CountryTaskScheduler.getInstance().getCountryId(countryCode);
            LOGGER.debug("## countryId --> " + countryId);
            if (countryId > 0) {
                BrandListFetchDTO feedBack;
                int operatorId = getOperatorId(countryId, mobileNo);
                LOGGER.debug("## operatorId --> " + operatorId);
                if (operatorId == 0) {
                    feedBack = SMSRoutesTaskSchedular.getInstance().getBrandId(countryId);
                    LOGGER.debug("##1 operatorId --> " + new Gson().toJson(feedBack));
                } else {
                    feedBack = SMSRoutesTaskSchedular.getInstance().getBrandId(countryId, operatorId);
                    LOGGER.debug("##2 operatorId --> " + new Gson().toJson(feedBack));
                    if (feedBack.getCount() == 0) {
                        feedBack = SMSRoutesTaskSchedular.getInstance().getBrandId(countryId, -1);
                        LOGGER.debug("##3 operatorId --> " + new Gson().toJson(feedBack));
                    }
                }
                LOGGER.debug(1);
                Map<Integer, ArrayList<Integer>> map = new TreeMap<>(feedBack.getMap());
                LOGGER.debug(2);
                if (!map.isEmpty()) {
                    ArrayList<Integer> keyList = new ArrayList<>();
                    map.entrySet().forEach((entrySet) -> {
                        keyList.add(entrySet.getKey());
                    });
                    int index;
                    switch (smsSendingAttempt) {
                        case 0: {
                            returnDTO = sendSmsInOldStyle(keyList, map, countryCode, mobileNo, countryId, vCode, message);
                            break;
                        }
                        case 1:
                        case 2:
                        case 3:
                        case 4: {
                            LOGGER.debug(3);
                            index = Constants.ATTEMPT_TO_BRANDID_INDEX_MAP[smsSendingAttempt];
                            LOGGER.debug(4);
                            if (feedBack.getCount() < index) {
                                index = 1;
                            }
                            int brandId = getBrandId(keyList, map, index);
                            LOGGER.debug("## smsSendingAttempt --> " + smsSendingAttempt + " --> countryCode --> " + countryCode + " --> brandId --> " + brandId + " --> operatorId --> " + operatorId + " --> mobileNo --> " + mobileNo);
                            returnDTO = sentSMSByRoutesId(vCode, brandId, countryCode, mobileNo, (brandId == Constants.MOBI_REACH_EX_ROBI ? message.replace("ringID", "ring_ID") : message));
                            break;
                        }
                        default: {
                            index = 1;
                            int brandId = getBrandId(keyList, map, index);
                            returnDTO = sentSMSByRoutesId(vCode, brandId, countryCode, mobileNo, message);
                            break;
                        }
                    }

                    if (!returnDTO.isDelivered()) {
                        LOGGER.error("Message sending failed! All attempts failed! for mobileNo --> " + mobileNo + " && message --> " + message + " --> destinationNumber --> " + returnDTO.getDestinationNumber());
                    }

                } else {
                    LOGGER.error("## No routes found for countryCode --> " + countryCode);
                }

            } else {
                LOGGER.error("CountryId not found for countryCode --> " + countryCode);
            }
        } catch (NullPointerException e) {
            LOGGER.error("NullPointerException on find routes" + e);
        } catch (InterruptedException e) {
            LOGGER.error("InterruptedException on find routes" + e);
        } catch (Exception e) {
            LOGGER.error("execption on find routes" + e);
        }
        return returnDTO;
    }

    public int verifiedUpdateBySms(String vCode, String mobileNo, String date) {
        MessagesDTO dto = new MessagesDTO();
        MyAppError error;
        int returnVal;
        if ((vCode != null && vCode.length() > 0) && (mobileNo != null && mobileNo.length() > 0)) {
            dto.setVerificationCode(vCode);
            dto.setMobileNo(mobileNo);
            dto.setReceivedDate(date);
            error = MessageScheduler.getInstance().updateSuccessfulMessageVerificationStatus(dto);

            if (error.getERROR_TYPE() > 0) {
                LOGGER.error("error in verifiedUpdate --> " + error.getErrorMessage());
                returnVal = 0;
            } else {
                returnVal = 1;
            }
        } else {
            returnVal = 0;
        }
        return returnVal;
    }

    public int verifiedUpdateByVoice(String vCode, String mobileNo, String date) {
        MessagesDTO dto = new MessagesDTO();
        MyAppError error;
        int returnVal;
        if ((vCode != null && vCode.length() > 0) && (mobileNo != null && mobileNo.length() > 0)) {
            dto.setVerificationCode(vCode);
            dto.setMobileNo(mobileNo);
            dto.setReceivedDate(date);
            error = VoiceTaskScheduler.getInstance().updateSuccessfulVoiceVerificationStatus(dto);

            if (error.getERROR_TYPE() > 0) {
                LOGGER.error("error in verifiedUpdate [voice] --> " + error.getErrorMessage());
                returnVal = 0;
            } else {
                returnVal = 1;
            }
        } else {
            returnVal = 0;
        }
        return returnVal;
    }

    public String verificationCode(String mobileNo) {
        return MessageScheduler.getInstance().verificationCode(mobileNo);
    }

//    public static void main(String[] args) {
//        String message = "Please enter __PASSCODE__ into ringID-Free Video Call, Voice and Chat";
//        System.out.println("" + message.replace("ringID", "ring_ID"));
//    }
    private ReturnDTO sendSmsInOldStyle(ArrayList<Integer> keyList, Map<Integer, ArrayList<Integer>> map, String countryCode, String mobileNo, int countryId, String vCode, String message) throws InterruptedException {
        ReturnDTO returnDTO = new ReturnDTO();
        boolean delivered = false;
        String destinationNumber = null;
        for (int i = keyList.size() - 1; i > -1; i--) {
            int priority = keyList.get(i);
            ArrayList<Integer> brandList = map.get(priority);
            destinationNumber = countryCode.trim().concat(mobileNo.trim());
            if (destinationNumber.startsWith("+")) {
                destinationNumber = destinationNumber.substring(1, destinationNumber.length());
            }
            ArrayList<String> responseIdList = new ArrayList<>();

            for (Integer brandID : brandList) {
                if (brandID > 0) {
                    RoutesController routesController = new RoutesController(countryId, vCode, brandID, destinationNumber, message);
                    MessagesDTO messagesDTO = routesController.rController();
                    response = messagesDTO.getSmsStatus();
                    responseIdList.add(messagesDTO.getResponseId());
                    LOGGER.debug("message sent by " + brandID + " and response " + response + " destinationNumber --> " + destinationNumber);
                    if (response.equals("success")) {
                        SMSDeliveryChecker sMSDeliveryChecker = new SMSDeliveryChecker(brandID, routesController.getMsgId());
                        sMSDeliveryChecker.start();
                        Thread.sleep(3000);
                    }
                    break; //Only single sms.
                }
            }
            // Wait for DB status
            Thread.sleep(45000);
            String idStr = "'-1'";
            for (String responseId : responseIdList) {
                idStr += ",'" + responseId + "'";
            }
            MessagesDAO messagesDAO = new MessagesDAO();
            if (messagesDAO.isDelivered(idStr)) {
                LOGGER.debug("Delivery Confirmation found for : " + idStr);
                delivered = true;
                break;
            }
            LOGGER.debug("priority --> " + priority + " --> All Brands failed to deliver sms -> " + Arrays.toString(brandList.toArray()) + " --> destinationNumber --> " + destinationNumber);
            break; //Only single sms.
        }
        returnDTO.setDelivered(delivered);
        returnDTO.setDestinationNumber(destinationNumber);
        return returnDTO;
    }

    private int getBrandId(ArrayList<Integer> keyList, Map<Integer, ArrayList<Integer>> map, int index) {
        int brandId = 0;
        boolean searchMore = true;
        int currentBrandSerial = 1;
        for (int i = keyList.size() - 1; i > -1 && searchMore; i--) {
            int priority = keyList.get(i);
            ArrayList<Integer> brandList = map.get(priority);
            for (int j : brandList) {
                if (index == currentBrandSerial) {
                    brandId = j;
                    searchMore = false;
                    break;
                } else {
                    currentBrandSerial++;
                }
            }
        }
        return brandId;
    }

    private int getOperatorId(int countryId, String mobileNo) {
        int operatorId = 0;
        String numberPattern;
        switch (countryId) {
            case 18:
                numberPattern = mobileNo.substring(0, 2);
                break;
            default:
                numberPattern = "";
        }
        OperatorDTO operator = OperatorLoader.getInstance().getOperatorIdAndName(numberPattern);
        if (operator != null) {
            operatorId = operator.getId();
        }
        return operatorId;
    }
}
