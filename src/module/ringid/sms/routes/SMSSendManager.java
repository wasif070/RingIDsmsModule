/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package module.ringid.sms.routes;

import module.ringid.sms.dto.SmsStatusDTO;
import module.ringid.sms.smsBrands.BrandsDTO;
import module.ringid.sms.utils.MyAppError;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.log4j.Logger;
import module.ringid.sms.messages.MessageScheduler;
import module.ringid.sms.messages.MessagesDTO;
import module.ringid.sms.utils.Constants;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public abstract class SMSSendManager {

    private static Logger logger = Logger.getLogger("com.ringid.sms");

    protected BrandsDTO brandDTO;

    public abstract MessagesDTO sendSMS(int countryId, String vCode, int brandId, String mobileNumber, String msg);

    public abstract String prepareData(BrandsDTO brandDTO, String msg, String mobileNumber);

    public abstract String prepareDeliveryStatusData(BrandsDTO brandDTO, String messageId);

    public abstract SmsStatusDTO parseResponse(String response);

    protected static String sendData(String data, String apiUrl) {
        StringBuilder stringBuffer = new StringBuilder();

        if (apiUrl.contains("https")) {
            HttpsURLConnection conn = null;
            BufferedReader rd = null;
            try {
                // Create a trust manager that does not validate certificate chains
                TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    @Override
                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    }
                }
                };

                // Install the all-trusting trust manager
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

                // Create all-trusting host name verifier
                HostnameVerifier allHostsValid = new HostnameVerifier() {
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                };

                // Install the all-trusting host verifier
                HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

                conn = (HttpsURLConnection) new URL(apiUrl).openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
                conn.getOutputStream().write(data.getBytes("UTF-8"));
                rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String line;
                while ((line = rd.readLine()) != null) {
                    stringBuffer.append(line);
                }
            } catch (Exception e) {
                logger.error("Exception [https] --> " + e);
            } finally {
                if (rd != null) {
                    try {
                        rd.close();
                    } catch (Exception e) {
                    }
                }
                if (conn != null) {
                    try {
                        conn.disconnect();
                    } catch (Exception e) {
                    }
                }
            }

        } else {
            HttpURLConnection conn = null;
            BufferedReader rd = null;
            try {
                conn = (HttpURLConnection) new URL(apiUrl).openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
                conn.getOutputStream().write(data.getBytes("UTF-8"));
                rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String line;
                while ((line = rd.readLine()) != null) {
                    stringBuffer.append(line);
                }
            } catch (Exception e) {
                logger.error("Exception [http] --> " + e);
            } finally {
                if (rd != null) {
                    try {
                        rd.close();
                    } catch (Exception e) {
                    }
                }
                if (conn != null) {
                    try {
                        conn.disconnect();
                    } catch (Exception e) {
                    }
                }
            }
        }
        return stringBuffer.toString();
    }

    /*
     protected final String sendData(String data, BrandsDTO brandDTO) throws MalformedURLException, IOException {
     if (brandDTO.getApiUrl().contains("https")) {
     HttpsURLConnection conn = (HttpsURLConnection) new URL(brandDTO.getApiUrl()).openConnection();
     conn.setDoOutput(true);
     conn.setRequestMethod("GET");
     conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
     conn.getOutputStream().write(data.getBytes("UTF-8"));
     final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
     final StringBuffer stringBuffer = new StringBuffer();
     String line;
     while ((line = rd.readLine()) != null) {
     stringBuffer.append(line);
     }
     rd.close();
     conn.disconnect();
     return stringBuffer.toString();
     } else {
     HttpURLConnection conn = (HttpURLConnection) new URL(brandDTO.getApiUrl()).openConnection();
     //            String data = user + pass + msisdn + message + sender + messageType;
     conn.setDoOutput(true);
     conn.setRequestMethod("GET");
     conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
     conn.getOutputStream().write(data.getBytes("UTF-8"));
     final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
     final StringBuffer stringBuffer = new StringBuffer();
     String line;
     while ((line = rd.readLine()) != null) {
     stringBuffer.append(line);
     }
     rd.close();
     conn.disconnect();
     return stringBuffer.toString();
     }
     }
     */
    protected String addToDB(MessagesDTO messagesDTO) {
        String response = Constants.FAILED;
        try {
            MyAppError error = MessageScheduler.getInstance().addMessage(messagesDTO);
            if (error.getERROR_TYPE() > 0) {
                logger.error("DB insert failed --> " + error.getErrorMessage() + "\n" + messagesDTO.toString());
                response = Constants.FAILED;
            } else {
                response = Constants.SUCCESS;
            }
        } catch (Exception e) {

        }
        return response;
    }

    public static void main(String[] args) {
//        try {
//            HttpsURLConnection conn = (HttpsURLConnection) new URL("https://104.193.36.228/rac/comports?lt=1&ringID=2110063638").openConnection();
//            conn.setDoOutput(true);
//            conn.setRequestMethod("GET");
//            conn.setRequestProperty("Content-Length", Integer.toString("".length()));
//            conn.getOutputStream().write("".getBytes("UTF-8"));
//            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//            final StringBuffer stringBuffer = new StringBuffer();
//            String line;
//            while ((line = rd.readLine()) != null) {
//                stringBuffer.append(line);
//            }
//            rd.close();
//            conn.disconnect();
//            System.out.println("out --> " + stringBuffer.toString());
//        } catch (Exception e) {
//            System.err.println("exc --> " + e);
//        }

        //-------
    }
}
