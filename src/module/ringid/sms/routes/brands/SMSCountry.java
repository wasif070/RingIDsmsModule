/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package module.ringid.sms.routes.brands;

import module.ringid.sms.dto.SmsStatusDTO;
import module.ringid.sms.messages.MessagesDTO;
import module.ringid.sms.routes.SMSSendManager;
import module.ringid.sms.smsBrands.BrandsDTO;
import module.ringid.sms.smsBrands.BrandsTaskScheduler;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;

import module.ringid.sms.utils.Constants;

/**
 *
 * @author sujon
 */
public class SMSCountry extends SMSSendManager {

    private static Logger logger = Logger.getLogger("com.ringid.sms");
    private MessagesDTO messagesDTO = new MessagesDTO();
    String startTime = "";
    String endTime = "";

    public String result = "";

    @Override
    public MessagesDTO sendSMS(int countryId, String vCode, final int brandId, String mobileNumber, String msg) {
        try {
            // (1). brandDTO
            brandDTO = BrandsTaskScheduler.getInstance().getBrandDTO(brandId);
            // (2). Construct data
            String data = prepareData(brandDTO, msg, mobileNumber);

            // (3)-1. Preparing messageDTO            
            //messagesDTO.//setSmsRate(rate);
            messagesDTO.setCountryId(countryId);
            messagesDTO.setVerificationCode(vCode);
            messagesDTO.setVerifiedSend(0);
            messagesDTO.setBrandId(brandId);
            messagesDTO.setMobileNo(mobileNumber);

            String sentTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
            //logger.debug("sent Date:" + sentTime);

            messagesDTO.setDate(sentTime);
            messagesDTO.setMessage(msg);

            // (4). Send data
            String response = sendData(data, brandDTO.getApiUrl());

            // (3)-2. Preparing messageDTO
            messagesDTO.setBrandResponse(response);

            if ((response.startsWith("OK:"))) {
                result = Constants.SUCCESS;
                final String apiMsgID = response.substring(response.indexOf(":") + 1);
                messagesDTO.setResponseId(apiMsgID);
                
                logger.info("msg submitted - waiting for confirmation[SMSCountry]");
            } else {
                result = Constants.FAILED;
                messagesDTO.setDlStatus(-1);
                messagesDTO.setVerifiedSend(-1);
                messagesDTO.setResponseId("-1");
                logger.error("Message submission failed [SMSCountry] --> response : " + response);
            }

        } catch (Exception e) {
            result = Constants.FAILED;
            messagesDTO.setDlStatus(-1);
            messagesDTO.setVerifiedSend(-1);
            messagesDTO.setResponseId("-1");
            String errorText = e.toString();
            if(errorText == null){
                errorText = "Sms sending failed.";
            }
            logger.error("Error SMS " , e);
            messagesDTO.setBrandResponse(messagesDTO.getBrandResponse() != null && messagesDTO.getBrandResponse().length() < 1 ? errorText : messagesDTO.getBrandResponse());
        }

        // (6). add to DB
        messagesDTO.setSmsStatus(result);
        messagesDTO.setDbInsert(addToDB(messagesDTO));

        return messagesDTO;
    }

    @Override
    public String prepareData(BrandsDTO brandDTO, String msg, String mobileNumber) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("User=").append(brandDTO.getUserName())
                .append("&passwd=").append(brandDTO.getPassword())
                .append("&message=").append(msg)
                .append("&sid=").append(brandDTO.getSenderId())
                .append("&mobilenumber=").append(mobileNumber)
                .append("&mtype=N&DR=Y");
        return stringBuilder.toString();
    }

    @Override
    public String prepareDeliveryStatusData(BrandsDTO brandDTO, String messageId) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("user=").append(brandDTO.getUserName())
                .append("&passwd=").append(brandDTO.getPassword())
                .append("&fromdate=").append(startTime)
                .append("&todate=").append(endTime);
        return stringBuilder.toString();
    }

    public SmsStatusDTO getDeliveryStatus(int brandId, String smsid) {
        SmsStatusDTO smsStatusDTO = new SmsStatusDTO();
        brandDTO = BrandsTaskScheduler.getInstance().getBrandDTO(brandId);
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL("http://api.smscountry.com/smscwebservices_bulk_reports.aspx?").openConnection();
            String data = prepareDeliveryStatusData(brandDTO, smsid);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
            conn.getOutputStream().write(data.getBytes("UTF-8"));
            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            final StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                stringBuilder.append(line);
            }
            rd.close();
            String str = stringBuilder.toString();
            //System.err.println("########### --> " + str);
            logger.info("return value : " + str);
            smsStatusDTO = parseResponse(str);
        conn.disconnect();
        } catch (Exception e) {
            logger.error("error in getStatus() " + e);
            smsStatusDTO.setStatus(SmsStatusDTO.STATUS.ERROR);
        }
        return smsStatusDTO;
    }

    @Override
    public SmsStatusDTO parseResponse(String response) {
        SmsStatusDTO smsStatusDTO = new SmsStatusDTO();
        //System.out.println("response ==> " + response);

        return smsStatusDTO;
    }

    public static void main(String[] args) {
        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/YYYY hh:mm:ss");
        //System.out.println("" + ft.format(new Date()));
    }
}
