package module.ringid.sms.routes.brands;

import java.net.URLEncoder;
import module.ringid.sms.country.CountryTaskScheduler;
import module.ringid.sms.dto.SmsStatusDTO;
import module.ringid.sms.messages.MessagesDTO;
import module.ringid.sms.routes.SMSSendManager;
import module.ringid.sms.smsBrands.BrandsDTO;
import module.ringid.sms.smsBrands.BrandsTaskScheduler;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;

import module.ringid.sms.utils.Constants;

public class OnnorokomSMS extends SMSSendManager {

    private static final Logger logger = Logger.getLogger("com.ringid.sms");
    private final MessagesDTO messagesDTO = new MessagesDTO();
    public String result = Constants.SUCCESS;
    String countryCode;

    @Override
    public MessagesDTO sendSMS(int countryId, String vCode, int brandId, String mobileNumber, String msg) {
        try {
            //---------------- Only for GP [start] -------------------            
            countryCode = CountryTaskScheduler.getInstance().getCountryDTO(countryId).getCode();
            if (countryCode.startsWith("+")) {
                countryCode = countryCode.substring(1, countryCode.length());
            }

            String tempMobileNumber = mobileNumber;
            if (mobileNumber.startsWith(countryCode)) {
                tempMobileNumber = mobileNumber.substring(2);
            }

            //---------------- Only for GP [end] -------------------
            // (1). brandDTO
            brandDTO = BrandsTaskScheduler.getInstance().getBrandDTO(brandId);
            // (2). Construct data
            String data = prepareData(brandDTO, msg, tempMobileNumber);
            if (data.length() < 1) {
                throw new Exception("OnnoRokom URL formation failed for GP --> & [msg] --> " + msg + " & [mobileNumber] --> " + mobileNumber);
            }

            // (3)-1. Preparing messageDTO
            //messagesDTO.//setSmsRate(rate);
            messagesDTO.setCountryId(countryId);
            messagesDTO.setVerificationCode(vCode);
            messagesDTO.setVerifiedSend(0);
            messagesDTO.setBrandId(brandId);
            messagesDTO.setMobileNo(mobileNumber);
            messagesDTO.setMessage(msg);

            String sentTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
            //logger.debug("sent Date:" + sentTime);

            messagesDTO.setDate(sentTime);

            // (4). Send data
            String response = sendData(data, brandDTO.getApiUrl()); //"8801913367007-201510144028748"

            // (3)-2. Preparing messageDTO
            messagesDTO.setBrandResponse(response);

            logger.debug("OnnoRokom response " + response);

            // (5). Response analysis
            String msgId = null;
            Integer status = null;
            String statusText = "";
            try {
                String[] responseArray = response.split("\\|\\|");
                status = Integer.parseInt(responseArray[0]);
                if (status == 1900) {
                    msgId = responseArray[2];
                } else {
                    statusText = responseArray[1];
                }
                logger.debug("OnnoRokom status --> " + status);
                logger.debug("OnnoRokom msgId --> " + msgId);
            } catch (Exception e) {
                logger.error("Exception while parsing OnnoRokom response --> response --> " + response + " --> exception --> ", e);
            }

            if (msgId != null && status == 1900) {
                result = Constants.SUCCESS;
                messagesDTO.setResponseId(msgId);
                logger.info("msg submitted - waiting for confirmation [OnnoRokom]");
            } else {
                result = Constants.FAILED;
                messagesDTO.setDlStatus(-1);
                messagesDTO.setVerifiedSend(-1);
                messagesDTO.setResponseId("-1");
                logger.error("OnnoRokom Message submission failed : " + statusText);
                logger.error("OnnoRokom Message content : " + msg);
            }
        } catch (Exception e) {
            result = Constants.FAILED;
            messagesDTO.setDlStatus(-1);
            messagesDTO.setVerifiedSend(-1);
            messagesDTO.setResponseId("-1");
            String errorText = e.toString();
            if (errorText == null) {
                errorText = "Sms sending failed(OnnoRokom).";
            }
            logger.error("Error SMS in OnnoRokom ", e);
            messagesDTO.setBrandResponse(messagesDTO.getBrandResponse() != null && messagesDTO.getBrandResponse().length() < 1 ? errorText : messagesDTO.getBrandResponse());
        }

        // (6). add to DB
        messagesDTO.setSmsStatus(result);
        messagesDTO.setDbInsert(addToDB(messagesDTO));
        return messagesDTO;
    }

    public SmsStatusDTO getDeliveryStatus(int brandId, MessagesDTO messagesDTO) {
        SmsStatusDTO smsStatusDTO = new SmsStatusDTO();
        smsStatusDTO.setResponseCode("200");
        smsStatusDTO.setStatus(SmsStatusDTO.STATUS.SUCCESS);
//        
//        brandDTO = BrandsTaskScheduler.getInstance().getBrandDTO(brandId);
//        try {
//            String data = prepareDeliveryStatusData(brandDTO, messagesDTO.getResponseId());
//
//            countryCode = CountryTaskScheduler.getInstance().getCountryDTO(messagesDTO.getCountryId()).getCode();
//
//            if (countryCode.startsWith("+")) {
//                countryCode = countryCode.substring(1, countryCode.length());
//            }
//
//            String tempMobileNumber = messagesDTO.getMobileNo();
//
//            if (messagesDTO.getMobileNo().startsWith(countryCode)) {
//                tempMobileNumber = messagesDTO.getMobileNo().substring(2);
//            }
//
//            data += "&msisdn=" + tempMobileNumber;
//            logger.debug("data delivery --> " + data);
//
//            String str = sendData(data, "http://smsbd.ringid.com/verifySMS");
//            logger.info("Delivery API response [GP] : " + str);
//            smsStatusDTO = parseResponse(str);
//        } catch (Exception e) {
//            logger.error("error in getStatus() " + e);
//            smsStatusDTO.setStatus(SmsStatusDTO.STATUS.ERROR);
//        }
        return smsStatusDTO;

//        <?xml version="1.0" encoding="utf-8"?><DataSet xmlns="http://wavecell.com/">  <xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">    <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">      <xs:complexType>        <xs:choice minOccurs="0" maxOccurs="unbounded">          <xs:element name="Table">            <xs:complexType>              <xs:sequence>                <xs:element name="Status" type="xs:string" minOccurs="0" />                <xs:element name="UMID" type="xs:string" minOccurs="0" />              </xs:sequence>            </xs:complexType>          </xs:element>        </xs:choice>      </xs:complexType>    </xs:element>  </xs:schema>  <diffgr:diffgram xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" xmlns:diffgr="urn:schemas-microsoft-com:xml-diffgram-v1" /></DataSet>
    }

    @Override
    public String prepareData(BrandsDTO brandDTO, String msg, String mobileNumber) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            stringBuilder.append("op=").append("OneToOne")
                    //                    .append("&brandId=").append(Constants.ONNO_ROKOM)
                    //                    .append("&countrycode=").append(countryCode)
                    .append("&type=TEXT")
                    .append("&mobile=").append(mobileNumber)
                    .append("&smsText=").append(URLEncoder.encode(msg, "UTF-8"))
                    .append("&username=").append(brandDTO.getUserName())
                    .append("&password=").append(brandDTO.getPassword())
                    .append("&maskName=").append(brandDTO.getMask())
                    .append("&campaignName=").append("");
        } catch (Exception ex) {
            logger.error("Exception ONNO_ROKOM --> ", ex);
        }
        return stringBuilder.toString();
    }

    @Override
    public String prepareDeliveryStatusData(BrandsDTO brandDTO, String messageId) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            stringBuilder
                    .append("brandId=").append(Constants.GP)
                    .append("&messagetype=3")
                    .append("&messageid=").append(messageId);
        } catch (Exception ex) {
            logger.error("Exception [prepareDeliveryStatusData] GP --> " + ex);
        }
        return stringBuilder.toString();
    }

    @Override
    public SmsStatusDTO parseResponse(String response) {
        SmsStatusDTO smsStatusDTO = new SmsStatusDTO();
        Integer status;
        String statusText;
        try {
            String[] responseArray = response.split(",");
            status = Integer.parseInt(responseArray[0]);
            statusText = responseArray[1];

            if (status == 200) {
                smsStatusDTO.setStatus(SmsStatusDTO.STATUS.SUCCESS);
            } else {
                smsStatusDTO.setStatus(SmsStatusDTO.STATUS.ERROR);
            }
            logger.debug("status [delvr status check : ] --> " + status + " --> statusText --> " + statusText);
        } catch (Exception e) {
            logger.error("Exception while parsing GP response --> response --> " + response + " --> exception --> " + e);
        }

        return smsStatusDTO;
    }

    public static void main(String[] args) {
        OnnorokomSMS onnorokomSMS = new OnnorokomSMS();
        onnorokomSMS.sendSMS(18, "123456", 19, "1717634317", "onnorokom mask ringID india");
    }
}
