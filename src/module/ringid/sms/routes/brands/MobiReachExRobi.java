package module.ringid.sms.routes.brands;

import module.ringid.sms.dto.SmsStatusDTO;
import module.ringid.sms.messages.MessagesDTO;
import module.ringid.sms.routes.SMSSendManager;
import module.ringid.sms.smsBrands.BrandsDTO;
import module.ringid.sms.smsBrands.BrandsTaskScheduler;
import module.ringid.sms.utils.Utils;
import java.text.SimpleDateFormat;
import java.util.Date;
import module.ringid.sms.smsbd.SmsBDDAO;
import org.apache.log4j.Logger;

import module.ringid.sms.utils.Constants;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class MobiReachExRobi extends SMSSendManager {

    private static Logger logger = Logger.getLogger("com.ringid.sms");

    private MessagesDTO messagesDTO = new MessagesDTO();
    public String result = Constants.SUCCESS;

    @Override
    public MessagesDTO sendSMS(int countryId, String vCode, final int brandId, String mobileNumber, String msg) {
        String response = null;
        try {
            // (1). brandDTO
            brandDTO = BrandsTaskScheduler.getInstance().getBrandDTO(brandId);
            // (2). Construct data
            String data = prepareData(brandDTO, msg, mobileNumber);

            // (3)-1. Preparing messageDTO
            //messagesDTO.//setSmsRate(rate);
            messagesDTO.setCountryId(countryId);
            messagesDTO.setVerificationCode(vCode);
            messagesDTO.setVerifiedSend(0);
            messagesDTO.setBrandId(brandId);
            messagesDTO.setMobileNo(mobileNumber);

            String sentTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
            //logger.debug("sent Date:" + sentTime);
            messagesDTO.setDate(sentTime);
            messagesDTO.setMessage(msg);

            logger.debug("data -> " + data + " -> ApiUrl -> " + brandDTO.getApiUrl());

            // (4). Send data
            String apiUrl;
            if (brandDTO.isDoLoadBalance()) {
                apiUrl = SmsBDDAO.getInstance().getUrl();
                if (apiUrl == null || !brandDTO.getApiUrl().contains(".com/")) {
                    apiUrl = brandDTO.getApiUrl();
                } else {
                    apiUrl = new StringBuilder(apiUrl).append(brandDTO.getApiUrl().substring(brandDTO.getApiUrl().indexOf(".com/") + ".com/".length())).toString();
                }
                logger.info("doLoadBalance true for Mobireach Ex Robi " + apiUrl);
            } else {
                apiUrl = brandDTO.getApiUrl();
            }
            response = sendData(data, apiUrl);
            //String response = sendData(data, brandDTO.getApiUrl());

            // (3)-2. Preparing messageDTO
            messagesDTO.setBrandResponse(response);

            Document doc = Utils.convertStringToDocument(response);
            doc.getDocumentElement().normalize();
//            Double balance = brandDTO.getBalance() - rate;
            NodeList nList = doc.getElementsByTagName("ServiceClass");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    messagesDTO.setResponseId(eElement.getElementsByTagName("MessageId").item(0).getTextContent());  //////////////////////add responseId
                    String msgId = eElement.getElementsByTagName("MessageId").item(0).getTextContent();
                    Integer errorCode = Integer.valueOf(eElement.getElementsByTagName("ErrorCode").item(0).getTextContent());
                    if (errorCode == 0) {
                        result = Constants.SUCCESS;
                        final String apiMsgID = eElement.getElementsByTagName("MessageId").item(0).getTextContent();
                        messagesDTO.setResponseId(apiMsgID);
                        logger.info("msg submitted - waiting for confirmation [MobiReach - ExRobi] --> " + msgId);
                    } else {
                        result = Constants.FAILED;
                        messagesDTO.setDlStatus(-1);
                        messagesDTO.setVerifiedSend(-1);
                        messagesDTO.setResponseId("-1");
                        logger.error("Message submission failed [MobiReach - ExRobi] : " + response);
                        logger.error("Message content : " + msg);
                    }
//                    messagesDTO.setSmsStatus(response);
                    logger.debug("ErrorCode : " + errorCode);
                    if (errorCode != 0) {
                        logger.debug("ErrorText : " + eElement.getElementsByTagName("ErrorText").item(0).getTextContent());
                    }
                }
            }

        } catch (Exception e) {
            result = Constants.FAILED;
            messagesDTO.setDlStatus(-1);
            messagesDTO.setVerifiedSend(-1);
            messagesDTO.setResponseId("-1");
            String errorText = e.toString();
            if (errorText == null) {
                errorText = "Sms sending failed.";
            }
            logger.error("Error SMS response : " + response, e);
            messagesDTO.setBrandResponse(messagesDTO.getBrandResponse() != null && messagesDTO.getBrandResponse().length() < 1 ? errorText : messagesDTO.getBrandResponse());
        }

        // (6). add to DB
        messagesDTO.setSmsStatus(result);
        messagesDTO.setDbInsert(addToDB(messagesDTO));

        return messagesDTO;
    }

    //Username=testuser&Password=XXXX XX&From=88018XXXXXXXX&To=8801XXXXXXXXX&Message=testmessage
    @Override
    public String prepareData(BrandsDTO brandDTO, String msg, String mobileNumber) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("brandId=").append(Constants.MOBI_REACH_EX_ROBI)
                .append("&Message=").append(msg)
                .append("&From=").append(brandDTO.getSenderId())
                .append("&To=").append(mobileNumber)
                .append("&Username=").append(brandDTO.getUserName())
                .append("&Password=").append(brandDTO.getPassword());
        return stringBuilder.toString();
    }

    @Override
    public String prepareDeliveryStatusData(BrandsDTO brandDTO, String messageId) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("brandId=").append(Constants.MOBI_REACH_EX_ROBI)
                .append("&MessageId=").append(messageId);
        return stringBuilder.toString();
    }

    public SmsStatusDTO getDeliveryStatus(int brandId, String smsid) {
        SmsStatusDTO smsStatusDTO = new SmsStatusDTO();
        brandDTO = BrandsTaskScheduler.getInstance().getBrandDTO(brandId);
        try {
            String data = prepareDeliveryStatusData(brandDTO, smsid);
            String str = sendData(data, "http://smsbd.ringid.com/verifySMS");
            logger.info("Delivery API response [MobiReach - ExRobi] : " + str);
            smsStatusDTO = parseResponse(str);
        } catch (Exception e) {
            logger.error("error in getStatus() " + e);
            smsStatusDTO.setStatus(SmsStatusDTO.STATUS.ERROR);
        }
        return smsStatusDTO;
    }

    @Override
    public SmsStatusDTO parseResponse(String response) {
        SmsStatusDTO smsStatusDTO = new SmsStatusDTO();
        //System.out.println("response ==> " + response);
        Document doc = Utils.convertStringToDocument(response);
        doc.getDocumentElement().normalize();
        NodeList nList = doc.getElementsByTagName("ServiceClass");
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                String status = eElement.getElementsByTagName("Status").item(0).getTextContent();
                if (status.equals("1")) {
                    smsStatusDTO.setStatus(SmsStatusDTO.STATUS.SUCCESS);
                    smsStatusDTO.setResponseCode(status);
                } else if (status.equals("-1")) {
                    smsStatusDTO.setStatus(SmsStatusDTO.STATUS.ERROR);
                    smsStatusDTO.setResponseText(eElement.getElementsByTagName("Status Text").item(0).getTextContent());
                    smsStatusDTO.setResponseCode(status);
                    smsStatusDTO.setResponseDetails(eElement.getElementsByTagName("Status Text").item(0).getTextContent());
                }
                //System.out.println("smsStatus --> " + smsStatusDTO.toString());
            }
        }

        return smsStatusDTO;
    }

    public static void main(String[] args) {
        MobiReachExRobi robi = new MobiReachExRobi();
        String response = "<?xml version=\"1.0\" encoding=\"utf-8\"?><ServiceClass xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://cmp.aktel.com/\">  <MessageId>0</MessageId>  <Status>-1</Status>  <StatusText>N/A</StatusText>  <ErrorCode>-3</ErrorCode>  <ErrorText>Insufficient credit</ErrorText>  <SMSCount>1</SMSCount>  <CurrentCredit>0</CurrentCredit></ServiceClass>";

        response = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><ServiceClass xmlns=\"http://cmp.aktel.com/\"><MessageId>54281820</MessageId><Status>0</Status><StatusText>pending</StatusText><ErrorCode>0</ErrorCode><SMSCount>1</SMSCount><CurrentCredit>198339</CurrentCredit></ServiceClass>";
        robi.parseResponse(response);
    }
}
