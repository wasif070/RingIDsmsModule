package module.ringid.sms.routes.brands;

import module.ringid.sms.dto.SmsStatusDTO;
import module.ringid.sms.messages.MessagesDTO;
import module.ringid.sms.routes.SMSSendManager;
import module.ringid.sms.smsBrands.BrandsDTO;
import module.ringid.sms.smsBrands.BrandsTaskScheduler;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;

import module.ringid.sms.utils.Constants;

public class ClickatellApi extends SMSSendManager {

    private String apiURL;
    private static Logger logger = Logger.getLogger("com.ringid.sms");
//    private BrandsDTO brandDTO = new BrandsDTO();
    private MessagesDTO messagesDTO = new MessagesDTO();
    public String result = Constants.SUCCESS;

    @Override
    public MessagesDTO sendSMS(int countryId, String vCode, int brandId, String mobileNumber, String msg) {
        try {
            // (1). brandDTO
            brandDTO = BrandsTaskScheduler.getInstance().getBrandDTO(brandId);
            // (2). Construct data
            String data = prepareData(brandDTO, msg, mobileNumber);

            // (3)-1. Preparing messageDTO
            //messagesDTO.//setSmsRate(rate);
            messagesDTO.setCountryId(countryId);
            messagesDTO.setVerificationCode(vCode);
            //////////////////////add responseId
            messagesDTO.setVerifiedSend(0);
            messagesDTO.setBrandId(brandId);
            messagesDTO.setMobileNo(mobileNumber);
            messagesDTO.setMessage(msg);

            String sentTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
            //logger.debug("sent Date:" + sentTime);

            messagesDTO.setDate(sentTime);

            // (4). Send data
            String response = sendData(data, brandDTO.getApiUrl());

            // (3)-2. Preparing messageDTO            
            messagesDTO.setBrandResponse(response);

            //ID: fe6e68a4eafe1dc236a74121dd2f838b
            //ERR: 001, Authentication failed
            if (response.startsWith("ID")) {
                result = Constants.SUCCESS;
                String[] respData = response.split(" ");
                messagesDTO.setResponseId(respData[1]);
                
                logger.info("msg submitted - waiting for confirmation[ClickATel]");
            } else if (response.startsWith("ERR")) {
                result = Constants.FAILED;
                messagesDTO.setDlStatus(-1);
                messagesDTO.setVerifiedSend(-1);
                messagesDTO.setResponseId("-1");
                logger.error("Message submission failed [clickAtel]: response --> " + response);
                logger.error("Message content : " + msg);
                
                String[] respData = response.split(",");
                String str = respData[1].substring(1);
                logger.error("Error --> " + response + " --> " + str);
            } else {
                logger.error("Unknown Response from ClickatellAPI");
            }

        } catch (Exception e) {
            result = Constants.FAILED;
            messagesDTO.setDlStatus(-1);
            messagesDTO.setVerifiedSend(-1);
            messagesDTO.setResponseId("-1");
            String errorText = e.toString();
            if(errorText == null){
                errorText = "Sms sending failed.";
            }
            logger.error("Error SMS " , e);
            messagesDTO.setBrandResponse(messagesDTO.getBrandResponse() != null && messagesDTO.getBrandResponse().length() < 1 ? errorText : messagesDTO.getBrandResponse());
        }

        // (6). add to DB
        messagesDTO.setSmsStatus(result);
        messagesDTO.setDbInsert(addToDB(messagesDTO));

        return messagesDTO;
    }

    //ID: 0c7c0b4dea52767e8ad5328a891407b7 Status: 004
    public SmsStatusDTO getDeliveryStatus(int brandId, String smsid) {
        SmsStatusDTO smsStatusDTO = new SmsStatusDTO();
        brandDTO = BrandsTaskScheduler.getInstance().getBrandDTO(brandId);
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL("http://api.clickatell.com/http/querymsg?").openConnection();
            String data = prepareDeliveryStatusData(brandDTO, smsid);
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
            conn.getOutputStream().write(data.getBytes("UTF-8"));
            final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            final StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                stringBuilder.append(line);
            }
            rd.close();
            String str = stringBuilder.toString();
            //System.err.println("########### --> " + str);
            logger.info("Delivery API response [ClickATell] : " + str);
            smsStatusDTO = parseResponse(str);
        conn.disconnect();
        } catch (Exception e) {
            logger.error("error in getStatus() " + e);
            smsStatusDTO.setStatus(SmsStatusDTO.STATUS.ERROR);
        }
        return smsStatusDTO;
    }

    @Override
    public String prepareDeliveryStatusData(BrandsDTO brandDTO, String messageId) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("api_id=").append(brandDTO.getApiId())
                .append("&user=").append(brandDTO.getUserName())
                .append("&password=").append(brandDTO.getPassword())
                .append("&apimsgid=").append(messageId);

        return stringBuilder.toString();
    }

    @Override
    public String prepareData(BrandsDTO brandDTO, String msg, String mobileNumber) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("api_id=").append(brandDTO.getApiId())
                .append("&user=").append(brandDTO.getUserName())
                .append("&password=").append(brandDTO.getPassword())
                .append("&to=").append(mobileNumber)
                .append("&text=").append(msg)
                .append("&from=").append(brandDTO.getSenderId());

        return stringBuilder.toString();
    }

    @Override
    public SmsStatusDTO parseResponse(String response) {
        SmsStatusDTO smsStatusDTO = new SmsStatusDTO();
        /**
         * Response: ID: xxxx Status: xxxx Or: ERR: Error number, error
         * description
         */
        String[] msg = response.split(" ");
        if (response.startsWith("ID")) {
            smsStatusDTO.setResponseCode(msg[3]);
            if (msg[3].equals("004")) {
                smsStatusDTO.setStatus(SmsStatusDTO.STATUS.SUCCESS);
            }
            smsStatusDTO.setResponseText(getResponseTextFromCode(msg[3]));
        } else if (response.startsWith("ERR")) {
            response = response.substring(response.indexOf(":"));
            msg = response.split(",");
            smsStatusDTO.setStatus(SmsStatusDTO.STATUS.ERROR);
            smsStatusDTO.setResponseCode(msg[1]);
            smsStatusDTO.setResponseText(msg[2]);
        }
        //System.out.println("smsStatus --> " + smsStatusDTO.toString());
        return smsStatusDTO;
    }

    private String getResponseTextFromCode(String code) {
        String text = "";
        switch (code) {
            case "001":
                text = "Message unknown : The message ID is incorrect or reporting is delayed.";
                break;
            case "002":
                text = "Message queued";
                break;
            case "003":
                text = "Delivered to gateway";
                break;
            case "004":
                text = "Received by recipient";
                break;
            case "005":
                text = "There was an error with the message, probably caused by the content of the message itself.";
                break;
            case "006":
                text = "The message was terminated by a user (stop message command) or by our staff.";
                break;
            case "007":
                text = "Error delivering message";
                break;
            case "008":
                text = "OK : Message received by gateway.";
                break;
            case "009":
                text = "Routing error";
                break;
            case "010":
                text = "Message expired";
                break;
            case "011":
                text = "Message queued for later delivery";
                break;
            case "012":
                text = "Out of credit";
                break;
            case "014":
                text = "aximum MT limit exceeded ";
                break;
        }
        return text;
    }
}
