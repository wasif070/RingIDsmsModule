package module.ringid.sms.routes.brands;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import java.io.IOException;
import module.ringid.sms.dto.SmsStatusDTO;
import module.ringid.sms.messages.MessagesDTO;
import module.ringid.sms.routes.SMSSendManager;
import module.ringid.sms.smsBrands.BrandsDTO;
import module.ringid.sms.smsBrands.BrandsTaskScheduler;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;

import module.ringid.sms.utils.Constants;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import module.ringid.sms.dto.BalanceFeedBack;
import module.ringid.sms.utils.HTTPSURLVisitor;
import org.json.JSONObject;

public class Nexmo extends SMSSendManager {

    private static final Logger logger = Logger.getLogger("com.ringid.sms");
    private MessagesDTO messagesDTO = new MessagesDTO();
    public String result = Constants.SUCCESS;

    @Override
    public MessagesDTO sendSMS(int countryId, String vCode, int brandId, String mobileNumber, String msg) {
        try {
            // (1). brandDTO
            brandDTO = BrandsTaskScheduler.getInstance().getBrandDTO(brandId);
            // (2). Construct data
            String data = prepareData(brandDTO, msg, mobileNumber);
            if (data.length() < 1) {
                throw new Exception("URL formation failed for Nexmo --> & [msg] --> " + msg + " & [mobileNumber] --> " + mobileNumber);
            }

            // (3)-1. Preparing messageDTO
            //messagesDTO.//setSmsRate(rate);
            messagesDTO.setCountryId(countryId);
            messagesDTO.setVerificationCode(vCode);
            messagesDTO.setVerifiedSend(0);
            messagesDTO.setBrandId(brandId);
            messagesDTO.setMobileNo(mobileNumber);
            messagesDTO.setMessage(msg);

            String sentTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
            //logger.debug("sent Date:" + sentTime);

            messagesDTO.setDate(sentTime);

            // (4). Send data
            String response = sendData(data, brandDTO.getApiUrl()); //"8801913367007-201510144028748"

            // (3)-2. Preparing messageDTO
            messagesDTO.setBrandResponse(response);

            logger.debug("Nexmo response " + response);

//{
//  "message-count": "1",
//  "messages": [
//    {
//      "to": "8801913367007",
//      "message-id": "0500000033E8BD67",
//      "status": "0",
//      "remaining-balance": "6.91800000",
//      "message-price": "0.04100000",
//      "network": "47003"
//    }
//  ]
//}            
            // (5). Response analysis
            String msgId = null;
            Integer status = null;
            String statusText = "";
            try {
                JsonObject jobject = new JsonParser().parse(response).getAsJsonObject();
                JsonArray jarray = jobject.getAsJsonArray("messages");
                jobject = jarray.get(0).getAsJsonObject();
                msgId = jobject.get("message-id").getAsString();
                status = jobject.get("status").getAsInt();
                if (status > 0) {
                    statusText = jobject.get("error-text").getAsString();
                }
                logger.debug("status --> " + status);
                logger.debug("msgId --> " + msgId);
            } catch (Exception e) {
                logger.error("Exception while parsing Nexmo response --> " + e);
            }

            if (msgId != null && status == 0) {
                result = Constants.SUCCESS;
                messagesDTO.setResponseId(msgId);
                logger.info("msg submitted - waiting for confirmation [Nexmo]");
            } else {
                result = Constants.FAILED;
                messagesDTO.setDlStatus(-1);
                messagesDTO.setVerifiedSend(-1);
                messagesDTO.setResponseId("-1");
                logger.error("Message submission failed : " + statusText);
                logger.error("Message content : " + msg);
            }
        } catch (Exception e) {
            result = Constants.FAILED;
            messagesDTO.setDlStatus(-1);
            messagesDTO.setVerifiedSend(-1);
            messagesDTO.setResponseId("-1");
            String errorText = e.toString();
            if(errorText == null){
                errorText = "Sms sending failed.";
            }
            logger.error("Error SMS " , e);
            messagesDTO.setBrandResponse(messagesDTO.getBrandResponse() != null && messagesDTO.getBrandResponse().length() < 1 ? errorText : messagesDTO.getBrandResponse());
        }

        // (6). add to DB
        messagesDTO.setSmsStatus(result);
        messagesDTO.setDbInsert(addToDB(messagesDTO));
        return messagesDTO;
    }

    @Override
    public String prepareData(BrandsDTO brandDTO, String msg, String mobileNumber) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            stringBuilder
                    .append("api_key=").append(brandDTO.getUserName())
                    .append("&api_secret=").append(brandDTO.getPassword())
                    .append("&text=").append(URLEncoder.encode(msg, "UTF-8"))
                    .append("&from=").append(brandDTO.getSenderId())
                    .append("&to=").append(mobileNumber);
        } catch (UnsupportedEncodingException ex) {
            logger.error("UnsupportedEncodingException [Nexmo] : ", ex);
        } catch (Exception ex) {
            logger.error("Exception [Nexmo] : ", ex);
        }
        return stringBuilder.toString();
    }

    @Override
    public String prepareDeliveryStatusData(BrandsDTO brandDTO, String messageId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SmsStatusDTO parseResponse(String response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static BalanceFeedBack getRemainingBalance(int brandId) {
        BalanceFeedBack feedBack = new BalanceFeedBack();
        try {
            BrandsDTO brandDTO = BrandsTaskScheduler.getInstance().getBrandDTO(brandId);
            if (brandDTO.getBalanceCheckUrl() != null && !brandDTO.getBalanceCheckUrl().isEmpty()) {
                String url = brandDTO.getBalanceCheckUrl() + brandDTO.getUserName() + "/" + brandDTO.getPassword();
                String response = HTTPSURLVisitor.visitHTTPSUrl(url, "GET", "");
                JsonParser parser = new JsonParser();
                feedBack.setBalance(parser.parse(response).getAsJsonObject().get("value").getAsDouble());
            }
        } catch (JsonSyntaxException | IOException e) {
            feedBack.setReasonCode(13);
        } catch (Exception e) {
            feedBack.setReasonCode(13);
        }
        return feedBack;
    }

    private static void printSuccessfulResponse() {
        System.out.println("{\n"
                + "  \"message-count\": \"1\",\n"
                + "  \"messages\": [\n"
                + "    {\n"
                + "      \"to\": \"8801913367007\",\n"
                + "      \"message-id\": \"0500000033E8BD67\",\n"
                + "      \"status\": \"0\",\n"
                + "      \"remaining-balance\": \"6.91800000\",\n"
                + "      \"message-price\": \"0.04100000\",\n"
                + "      \"network\": \"47003\"\n"
                + "    }\n"
                + "  ]\n"
                + "}");
    }

    private static void printFailedResponse() {
        System.out.println("{\n"
                + "  \"message-count\": \"1\",\n"
                + "  \"messages\": [\n"
                + "    {\n"
                + "      \"status\": \"2\",\n"
                + "      \"error-text\": \"Missing username\"\n"
                + "    }\n"
                + "  ]\n"
                + "}");
    }

    public static void main(String[] args) {
        /*
        String jsonLine = "{\n"
                + "  \"message-count\": \"1\",\n"
                + "  \"messages\": [\n"
                + "    {\n"
                + "      \"to\": \"8801913367007\",\n"
                + "      \"message-id\": \"0500000033E8BD67\",\n"
                + "      \"status\": \"0\",\n"
                + "      \"remaining-balance\": \"6.91800000\",\n"
                + "      \"message-price\": \"0.04100000\",\n"
                + "      \"network\": \"47003\"\n"
                + "    }\n"
                + "  ]\n"
                + "}";

        //JsonElement jelement = new JsonParser().parse(jsonLine);
        JsonObject jobject = new JsonParser().parse(jsonLine).getAsJsonObject();
        JsonArray jarray = jobject.getAsJsonArray("messages");
        jobject = jarray.get(0).getAsJsonObject();
        System.out.println("message-id --> " + jobject.get("message-id"));
        System.out.println("resp : \n\n" + jsonLine);
         */
        getRemainingBalance(15);
    }
}
