package module.ringid.sms.routes.brands;

import module.ringid.sms.dto.SmsStatusDTO;
import module.ringid.sms.messages.MessagesDTO;
import module.ringid.sms.routes.SMSSendManager;
import module.ringid.sms.smsBrands.BrandsDTO;
import module.ringid.sms.smsBrands.BrandsTaskScheduler;
import module.ringid.sms.utils.Constants;
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import java.util.StringTokenizer;

//1701|8801913367007|d32719e1-d1f8-4d3a-b3ee-771cdf3dbe86

public class SMSRouter extends SMSSendManager {

    private static Logger logger = Logger.getLogger("com.ringid.sms");
    InetAddress destIPAddress;
    private MessagesDTO messagesDTO = new MessagesDTO();
    private String result;

    public SMSRouter() {
    }

    @Override
    public MessagesDTO sendSMS(int countryId, String vCode, int brandId, String mobileNumber, String msg) {
        try {

            // (1). brandDTO
            brandDTO = BrandsTaskScheduler.getInstance().getBrandDTO(brandId);
            // (2). Construct data
            String data = prepareData(brandDTO, msg, mobileNumber);

            // (3)-1. Preparing messageDTO
            //messagesDTO.//setSmsRate(rate);
            messagesDTO.setCountryId(countryId);
            messagesDTO.setVerificationCode(vCode);
//            messagesDTO.setResponseId(brandDTO.getSenderId());  //////////////////////add responseId
            messagesDTO.setVerifiedSend(0);
            messagesDTO.setBrandId(brandId);
            messagesDTO.setMobileNo(mobileNumber);

            String sentTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
            //logger.debug("sent Date:" + sentTime);

            messagesDTO.setDate(sentTime);
            messagesDTO.setMessage(msg);

            // (4). Send data
            String response = sendData(data, brandDTO.getApiUrl());

            // (3)-2. Preparing messageDTO
            messagesDTO.setBrandResponse(response);

//            response : 1701|8801913367007|d32719e1-d1f8-4d3a-b3ee-771cdf3dbe86
            StringTokenizer token = new StringTokenizer(response, "|");
            String[] parts = new String[token.countTokens()];
            int ind = 0;
            while (token.hasMoreElements()) {
                parts[ind++] = token.nextElement().toString();
            }

            if (!response.startsWith("1701")) {
                result = Constants.FAILED;
                messagesDTO.setDlStatus(-1);
                messagesDTO.setVerifiedSend(-1);
                messagesDTO.setResponseId("-1");
                String errorMessage = getErrorMessage(Integer.valueOf(parts[0]));
                logger.error("SMSRouter errorMessage --> " + errorMessage);
            } else {
                result = Constants.SUCCESS;
                String apimsgId = parts[2];
                messagesDTO.setResponseId(apimsgId);
                
                logger.debug("Message submitted successfully : [SMSRouter]response : " + response);
            }
        } catch (Exception e) {
            result = Constants.FAILED;
            messagesDTO.setDlStatus(-1);
            messagesDTO.setVerifiedSend(-1);
            messagesDTO.setResponseId("-1");
            String errorText = e.toString();
            if(errorText == null){
                errorText = "Sms sending failed.";
            }
            logger.error("Error SMS " , e);
            messagesDTO.setBrandResponse(messagesDTO.getBrandResponse() != null && messagesDTO.getBrandResponse().length() < 1 ? errorText : messagesDTO.getBrandResponse());
        }

        // (6). add to DB
        messagesDTO.setSmsStatus(result);
        messagesDTO.setDbInsert(addToDB(messagesDTO));

        return messagesDTO;
    }

    @Override
    public String prepareData(BrandsDTO brandDTO, String msg, String mobileNumber) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("username=").append(brandDTO.getUserName())
                .append("&password=").append(brandDTO.getPassword())
                .append("&type=").append(0)
                .append("&dlr=").append(1)
                .append("&destination=").append(mobileNumber)
                .append("&source=").append(brandDTO.getSenderId())
                .append("&message=").append(msg);
        return stringBuilder.toString();
    }

    @Override
    public String prepareDeliveryStatusData(BrandsDTO brandDTO, String messageId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SmsStatusDTO parseResponse(String response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private String getErrorMessage(int responseCode) {
        String errorMessage = "";
        switch (responseCode) {
            case 1701:
                errorMessage = "Message Submitted Successfully";
                break;
            case 1702:
                errorMessage = "one of the parameters was not provided or left blank";
                break;
            case 1703:
                errorMessage = "Invalid value in username or password field";
                break;
            case 1704:
                errorMessage = "Invalid value in \"type\" field";
                break;
            case 1705:
                errorMessage = "Invalid Message";
                break;
            case 1706:
                errorMessage = "Invalid Destination";
                break;
            case 1707:
                errorMessage = "MInvalid Source (Sender)";
                break;
            case 1708:
                errorMessage = "Invalid value for \"dlr\" field";
                break;
            case 1709:
                errorMessage = "User validation failed";
                break;
            case 1710:
                errorMessage = "Internal Error";
                break;
            case 1025:
                errorMessage = "Insufficient Credit";
                break;
            case 1032:
                errorMessage = "Destination in DND";
                break;
            case 1033:
                errorMessage = "Sender / Template Mismatch";
                break;
            default:
                errorMessage = "Error occured";
                break;
        }
        return errorMessage;
    }
}
