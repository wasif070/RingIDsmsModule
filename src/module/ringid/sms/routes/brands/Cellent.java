package module.ringid.sms.routes.brands;

import module.ringid.sms.dto.SmsStatusDTO;
import module.ringid.sms.messages.MessagesDTO;
import module.ringid.sms.routes.SMSSendManager;
import module.ringid.sms.smsBrands.BrandsDTO;
import module.ringid.sms.smsBrands.BrandsTaskScheduler;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;

import module.ringid.sms.utils.Constants;

public class Cellent extends SMSSendManager {

    private static Logger logger = Logger.getLogger("com.ringid.sms");
//    private BrandsDTO brandDTO = new BrandsDTO();
    private MessagesDTO messagesDTO = new MessagesDTO();
    public String result = Constants.SUCCESS;

    @Override
    public MessagesDTO sendSMS(int countryId, String vCode, int brandId, String mobileNumber, String msg) {
        try {
            // (1). brandDTO
            brandDTO = BrandsTaskScheduler.getInstance().getBrandDTO(brandId);
            // (2). Construct data
            String data = prepareData(brandDTO, msg, mobileNumber);

            // (3)-1. Preparing messageDTO
            //messagesDTO.//setSmsRate(rate);
            messagesDTO.setCountryId(countryId);
            messagesDTO.setVerificationCode(vCode);
            messagesDTO.setVerifiedSend(0);
            messagesDTO.setBrandId(brandId);
            messagesDTO.setMobileNo(mobileNumber);
            messagesDTO.setMessage(msg);

            String sentTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
            //logger.debug("sent Date:" + sentTime);

            messagesDTO.setDate(sentTime);

            // (4). Send data
            String response = sendData(data, brandDTO.getApiUrl()); //"8801913367007-201510144028748"

            // (3)-2. Preparing messageDTO
            messagesDTO.setBrandResponse(response);

            logger.debug("Cellent response " + response);

            // (5). Response analysis
            if (response.substring(0, response.indexOf("-")).trim().equals(mobileNumber)) {
                result = Constants.SUCCESS;
                String[] respData = response.split("-");
                messagesDTO.setResponseId(respData[1]);

                logger.info("msg submitted - waiting for confirmation [Cellent]");
            } else {
                result = Constants.FAILED;
                messagesDTO.setDlStatus(-1);
                messagesDTO.setVerifiedSend(-1);
                messagesDTO.setResponseId("-1");
                logger.error("Message submission failed : " + response);
                logger.error("Message content : " + msg);
            }
        } catch (Exception e) {
            result = Constants.FAILED;
            messagesDTO.setDlStatus(-1);
            messagesDTO.setVerifiedSend(-1);
            messagesDTO.setResponseId("-1");
            String errorText = e.toString();
            if(errorText == null){
                errorText = "Sms sending failed.";
            }
            logger.error("Error SMS ", e);
            messagesDTO.setBrandResponse(messagesDTO.getBrandResponse() != null && messagesDTO.getBrandResponse().length() < 1 ? errorText : messagesDTO.getBrandResponse());
        }

        // (6). add to DB
        messagesDTO.setSmsStatus(result);
        messagesDTO.setDbInsert(addToDB(messagesDTO));
        return messagesDTO;
    }

    @Override
    public String prepareData(BrandsDTO brandDTO, String msg, String mobileNumber) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("usr=").append(brandDTO.getUserName())
                .append("&pass=").append(brandDTO.getPassword())
                .append("&msg=").append(msg)
                .append("&sid=").append(brandDTO.getSenderId())
                .append("&msisdn=").append(mobileNumber)
                .append("&mt=0");
        return stringBuilder.toString();
    }

    @Override
    public String prepareDeliveryStatusData(BrandsDTO brandDTO, String messageId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SmsStatusDTO parseResponse(String response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
