package module.ringid.sms.routes.brands;

import module.ringid.sms.dto.SmsStatusDTO;
import module.ringid.sms.messages.MessagesDTO;
import module.ringid.sms.routes.SMSSendManager;
import module.ringid.sms.smsBrands.BrandsDTO;
import module.ringid.sms.smsBrands.BrandsTaskScheduler;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;

import module.ringid.sms.utils.Constants;
import com.twilio.Twilio;
import com.twilio.type.PhoneNumber;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class Twilio_ extends SMSSendManager {

    private static final Logger logger = Logger.getLogger("com.ringid.sms");
    private MessagesDTO messagesDTO = new MessagesDTO();
    public String result = Constants.SUCCESS;

    @Override
    public MessagesDTO sendSMS(int countryId, String vCode, int brandId, String mobileNumber, String msg) {
        try {
            // (1). brandDTO
            brandDTO = BrandsTaskScheduler.getInstance().getBrandDTO(brandId);

            messagesDTO.setCountryId(countryId);
            messagesDTO.setVerificationCode(vCode);
            messagesDTO.setVerifiedSend(0);
            messagesDTO.setBrandId(brandId);
            messagesDTO.setMobileNo(mobileNumber);
            messagesDTO.setMessage(msg);

            String sentTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
            //logger.debug("sent Date:" + sentTime);

            messagesDTO.setDate(sentTime);

            Twilio.init(brandDTO.getApiId(), brandDTO.getPassword());
            com.twilio.rest.api.v2010.account.Message message = null;
            String errorResponse = "";
            try {
                message = com.twilio.rest.api.v2010.account.Message.creator(new PhoneNumber(mobileNumber),
                        brandDTO.getSenderId(), msg
                ).create();
            } catch (Exception e) {
                errorResponse = e.toString();
            }

            String response = message != null ? message.toString() : errorResponse;
            // (3)-2. Preparing messageDTO
            messagesDTO.setBrandResponse(response);

            logger.debug("Twilio_ response " + response);

            // (5). Response analysis
            String msgId = message!= null ? message.getSid() : "";
            String statusText = "";

            if (msgId != null) {
                result = Constants.SUCCESS;
                messagesDTO.setResponseId(msgId);
                logger.info("msg submitted - waiting for confirmation [Twilio]");
            } else {
                result = Constants.FAILED;
                messagesDTO.setDlStatus(-1);
                messagesDTO.setVerifiedSend(-1);
                messagesDTO.setResponseId("-1");
                logger.error("Message submission failed : " + statusText);
                logger.error("Message content : " + msg);
            }
        } catch (Exception e) {
            result = Constants.FAILED;
            messagesDTO.setDlStatus(-1);
            messagesDTO.setVerifiedSend(-1);
            messagesDTO.setResponseId("-1");
            String errorText = e.toString();
            if (errorText == null) {
                errorText = "Sms sending failed.";
            }
            logger.error("Error SMS ", e);
            messagesDTO.setBrandResponse(messagesDTO.getBrandResponse() != null && messagesDTO.getBrandResponse().length() < 1 ? errorText : messagesDTO.getBrandResponse());
        }

        // (6). add to DB
        messagesDTO.setSmsStatus(result);
        messagesDTO.setDbInsert(addToDB(messagesDTO));
        return messagesDTO;
    }

    @Override
    public String prepareData(BrandsDTO brandDTO, String msg, String mobileNumber) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            stringBuilder
                    .append("api_key=").append(brandDTO.getUserName())
                    .append("&api_secret=").append(brandDTO.getPassword())
                    .append("&text=").append(URLEncoder.encode(msg, "UTF-8"))
                    .append("&from=").append(brandDTO.getSenderId())
                    .append("&to=").append(mobileNumber);
        } catch (UnsupportedEncodingException ex) {
            logger.error("UnsupportedEncodingException [Twilio_] : ", ex);
        } catch (Exception ex) {
            logger.error("Exception [Twilio_] : ", ex);
        }
        return stringBuilder.toString();
    }

    @Override
    public String prepareDeliveryStatusData(BrandsDTO brandDTO, String messageId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SmsStatusDTO parseResponse(String response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static void printSuccessfulResponse() {
        System.out.println("{\n"
                + "  \"message-count\": \"1\",\n"
                + "  \"messages\": [\n"
                + "    {\n"
                + "      \"to\": \"8801913367007\",\n"
                + "      \"message-id\": \"0500000033E8BD67\",\n"
                + "      \"status\": \"0\",\n"
                + "      \"remaining-balance\": \"6.91800000\",\n"
                + "      \"message-price\": \"0.04100000\",\n"
                + "      \"network\": \"47003\"\n"
                + "    }\n"
                + "  ]\n"
                + "}");
    }

    private static void printFailedResponse() {
        System.out.println("{\n"
                + "  \"message-count\": \"1\",\n"
                + "  \"messages\": [\n"
                + "    {\n"
                + "      \"status\": \"2\",\n"
                + "      \"error-text\": \"Missing username\"\n"
                + "    }\n"
                + "  ]\n"
                + "}");
    }

    public static void main(String[] args) {
        String jsonLine = "{\n"
                + "  \"message-count\": \"1\",\n"
                + "  \"messages\": [\n"
                + "    {\n"
                + "      \"to\": \"8801913367007\",\n"
                + "      \"message-id\": \"0500000033E8BD67\",\n"
                + "      \"status\": \"0\",\n"
                + "      \"remaining-balance\": \"6.91800000\",\n"
                + "      \"message-price\": \"0.04100000\",\n"
                + "      \"network\": \"47003\"\n"
                + "    }\n"
                + "  ]\n"
                + "}";

        Twilio_ tiTwilio_ = new Twilio_();
        int countryId = 18;
        String vCode = "5678";
        int brandId = 16;
        String mobileNumber = "+8801913367007";
        String msg = "Test Twilio বাংলা";

        tiTwilio_.sendSMS(countryId, vCode, brandId, mobileNumber, msg);
        //JsonElement jelement = new JsonParser().parse(jsonLine);
//        JsonObject jobject = new JsonParser().parse(jsonLine).getAsJsonObject();
//        JsonArray jarray = jobject.getAsJsonArray("messages");
//        jobject = jarray.get(0).getAsJsonObject();
//        System.out.println("message-id --> " + jobject.get("message-id"));
//        System.out.println("resp : \n\n" + jsonLine);

    }
}
