package module.ringid.sms.routes.brands;

import module.ringid.sms.dto.SmsStatusDTO;
import module.ringid.sms.messages.MessagesDTO;
import module.ringid.sms.routes.SMSSendManager;
import module.ringid.sms.smsBrands.BrandsDTO;
import module.ringid.sms.smsBrands.BrandsTaskScheduler;
import module.ringid.sms.utils.Constants;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;


public class MobiSMS extends SMSSendManager {

    private static Logger logger = Logger.getLogger("com.ringid.sms");
    private MessagesDTO messagesDTO = new MessagesDTO();
    public String result = "";

    @Override
    public MessagesDTO sendSMS(int countryId, String vCode, int brandId, String mobileNumber, String msg) {
        try {
            // (1). brandDTO
            brandDTO = BrandsTaskScheduler.getInstance().getBrandDTO(brandId);
            // (2). Construct data
            String data = prepareData(brandDTO, msg, mobileNumber);

            // (3)-1. Preparing messageDTO
            //messagesDTO.//setSmsRate(rate);
            messagesDTO.setCountryId(countryId);
            messagesDTO.setVerificationCode(vCode);
//            messagesDTO.setResponseId(brandDTO.getSenderId());  //////////////////////add responseId
            messagesDTO.setVerifiedSend(0);
            messagesDTO.setBrandId(brandId);
            messagesDTO.setMobileNo(mobileNumber);

            String sentTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
            //logger.debug("sent Date:" + sentTime);

            messagesDTO.setDate(sentTime);
            messagesDTO.setMessage(msg);

            // (4). Send data
            String response = sendData(data, brandDTO.getApiUrl());
//OK|0.0050|448 
//OK228944099
            // (3)-2. Preparing messageDTO
            messagesDTO.setBrandResponse(response);
//            messagesDTO.setResponseId(response);
            if (!response.startsWith("OK")) {
                result = Constants.FAILED;
                messagesDTO.setDlStatus(-1);
                messagesDTO.setVerifiedSend(-1);
                messagesDTO.setResponseId("-1");
                logger.error("Message submission failed [mobiSMS] : " + getErrorMessage(response));
                logger.error("Message content : " + msg);
            } else {
                result = Constants.SUCCESS;
                final String apiMsgID = response.substring("OK".length());
                messagesDTO.setResponseId(apiMsgID);
                
                logger.info("msg submitted - waiting for confirmation[MobiSMS]");
            }
        } catch (Exception e) {
            result = Constants.FAILED;
            messagesDTO.setDlStatus(-1);
            messagesDTO.setVerifiedSend(-1);
            messagesDTO.setResponseId("-1");
            String errorText = e.toString();
            if(errorText == null){
                errorText = "Sms sending failed.";
            }
            logger.error("Error SMS " , e);
            messagesDTO.setBrandResponse(messagesDTO.getBrandResponse() != null && messagesDTO.getBrandResponse().length() < 1 ? errorText : messagesDTO.getBrandResponse());
        }

        // (6). add to DB
        messagesDTO.setSmsStatus(result);
        messagesDTO.setDbInsert(addToDB(messagesDTO));

        return messagesDTO;
    }

    @Override
    public String prepareData(BrandsDTO brandDTO, String msg, String mobileNumber) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder
                .append("username=").append(brandDTO.getUserName())
                .append("&password=").append(brandDTO.getPassword())
                .append("&msgtext=").append(msg)
                .append("&originator=").append(brandDTO.getSenderId())
                .append("&phone=").append(mobileNumber)
                .append("&showDLR=1");
//        &showCOST=1&showOPERATORID=1
        return stringBuilder.toString();
    }

    @Override
    public String prepareDeliveryStatusData(BrandsDTO brandDTO, String messageId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SmsStatusDTO parseResponse(String response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private String getErrorMessage(String responseCode) {
        String errorMessage = "";
        switch (responseCode) {
            case "ERROR100":
                errorMessage = "Temporary Internal Server Error. Try again later";
                break;
            case "ERROR101":
                errorMessage = "Authentication Error (Not valid login Information)";
                break;
            case "ERROR102":
                errorMessage = "No credits available";
                break;
            case "ERROR103":
                errorMessage = "MSIDSN (phone parameter) is invalid or prefix is not supported";
                break;
            case "ERROR104":
                errorMessage = "Tariff Error";
                break;
            case "ERROR105":
                errorMessage = "You are not allowed to send to that destination/country";
                break;
            case "ERROR106":
                errorMessage = "Not Valid Route number or you are not allowed to use this route";
                break;
            case "ERROR107":
                errorMessage = "No proper Authentication (IP restriction is activated)";
                break;
            case "ERROR108":
                errorMessage = "You have no permission to send messages through HTTP API";
                break;
            case "ERROR109":
                errorMessage = "Not Valid Originator";
                break;
            case "ERROR110":
                errorMessage = "You are not allowed to send (Routing not available) or Reseller is trying to send while not allowed";
                break;
            case "ERROR111":
                errorMessage = "Invalid Expiration date or Expiration Date is less than 30 minutes than the date of SMS submission";
                break;
            case "ERROR999":
                errorMessage = "Invalid HTTP Request";
                break;
            default:
                errorMessage = "Error occured";
                break;
        }
        return errorMessage;
    }
}
