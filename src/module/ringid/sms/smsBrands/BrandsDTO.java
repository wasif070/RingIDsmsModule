package module.ringid.sms.smsBrands;

import java.util.Comparator;

public class BrandsDTO {

    private int id;
    private String brandName;
    private String userName;
    private String password;
    private String apiUrl;
    private String balanceCheckUrl;
    private String apiId;
    private String contactEmail;
    private String senderId;
    private int brandId;
    private Integer priority;
    private double balance;
    private boolean commercial;
    private String brandsType;
    private boolean smsEnabled;
    private boolean voiceEnabled;
    private String mask;
    private boolean doLoadBalance;

    public boolean isSmsEnabled() {
        return smsEnabled;
    }

    public void setSmsEnabled(boolean smsEnabled) {
        this.smsEnabled = smsEnabled;
    }

    public boolean isVoiceEnabled() {
        return voiceEnabled;
    }

    public void setVoiceEnabled(boolean voiceEnabled) {
        this.voiceEnabled = voiceEnabled;
    }

    public String getBrandsType() {
        return brandsType;
    }

    public void setBrandsType(String brandsType) {
        this.brandsType = brandsType;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public boolean isCommercial() {
        return commercial;
    }

    public void setCommercial(boolean commercial) {
        this.commercial = commercial;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getBalanceCheckUrl() {
        return balanceCheckUrl;
    }

    public void setBalanceCheckUrl(String balanceCheckUrl) {
        this.balanceCheckUrl = balanceCheckUrl;
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public boolean isDoLoadBalance() {
        return doLoadBalance;
    }

    public void setDoLoadBalance(boolean doLoadBalance) {
        this.doLoadBalance = doLoadBalance;
    }

    public static class CompIdDSC implements Comparator<BrandsDTO> {

        @Override
        public int compare(BrandsDTO arg0, BrandsDTO arg1) {
            return arg1.id - arg0.id;
        }
    }

    public static class CompIdASC implements Comparator<BrandsDTO> {

        @Override
        public int compare(BrandsDTO arg0, BrandsDTO arg1) {
            return arg0.id - arg1.id;
        }
    }

    public static class CompBrandNameDSC implements Comparator<BrandsDTO> {

        @Override
        public int compare(BrandsDTO arg0, BrandsDTO arg1) {
            return arg1.brandName.compareToIgnoreCase(arg0.brandName);
        }
    }

    public static class CompBrandNameASC implements Comparator<BrandsDTO> {

        @Override
        public int compare(BrandsDTO arg0, BrandsDTO arg1) {
            return arg0.brandName.compareToIgnoreCase(arg1.brandName);
        }
    }

    public static class CompCommercialDSC implements Comparator<BrandsDTO> {

        @Override
        public int compare(BrandsDTO arg0, BrandsDTO arg1) {
            return Boolean.compare(arg1.commercial, arg0.commercial);
        }
    }

    public static class CompCommercialASC implements Comparator<BrandsDTO> {

        @Override
        public int compare(BrandsDTO arg0, BrandsDTO arg1) {
            return Boolean.compare(arg0.commercial, arg1.commercial);
        }
    }

}
