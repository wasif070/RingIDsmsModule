package module.ringid.sms.smsBrands;

import java.util.ArrayList;
import java.util.HashMap;
import module.ringid.sms.utils.MyAppError;

public class BrandsTaskScheduler {

    public static BrandsTaskScheduler scheduler;

    public static BrandsTaskScheduler getInstance() {
        if (scheduler == null) {
            scheduler = new BrandsTaskScheduler();
        }
        return scheduler;
    }

    public ArrayList<BrandsDTO> getBrandsInfo() {
        BrandsDAO brandsDAO = new BrandsDAO();
        return brandsDAO.getBrandsInfo();
    }

    public MyAppError addOperatorInfo(BrandsDTO p_dto) {
        BrandsDAO operatorDAO = new BrandsDAO();
        return operatorDAO.addBrandsInfo(p_dto);
    }

    public MyAppError updateBrand(BrandsDTO p_dto) {
        BrandsDAO brandDAO = new BrandsDAO();
        return brandDAO.updateBrand(p_dto);
    }

    public MyAppError deleteBrand(int id) {
        BrandsDAO brandDAO = new BrandsDAO();
        return brandDAO.deleteBrand(id);
    }

    public BrandsDTO getBrandInfoById(int id) {
        BrandsDAO brandDAO = new BrandsDAO();
        return brandDAO.getBrandInfoById(id);
    }

    public ArrayList<BrandsDTO> getBrandList() {
        return BrandLoader.getInstance().getBrandList();
    }

    public BrandsDTO getBrandDTO(int id) {
        return BrandLoader.getInstance().getBrandDTO(id);
    }

    public synchronized String getBrandNameById(int id) {
        return BrandLoader.getInstance().getBrandNameById(id);
    }

    public ArrayList<BrandsDTO> getBrandNameAndIdList() {
        return BrandLoader.getInstance().getBrandList();
    }

    public synchronized int getBrandIdByName(String name) {
        return BrandLoader.getInstance().getBrandIdByName(name);
    }

    public MyAppError updateBrandBalance(BrandsDTO dto) {
        BrandsDAO brandDAO = new BrandsDAO();
        return brandDAO.updateBrandBalance(dto);
    }

    public ArrayList<BrandsDTO> getBrandList(int column, int sort, String brandsType) {
        return BrandLoader.getInstance().getBrandList(column, sort, brandsType);
    }

    public ArrayList<BrandsDTO> getBrandList(int column, int sort, boolean type, String brandsType) {
        return BrandLoader.getInstance().getBrandList(column, sort, type, brandsType);
    }
    public HashMap<Integer, String> getBrandNameMap() {
        return BrandLoader.getInstance().getBrandNameMap();
    }
    
}
