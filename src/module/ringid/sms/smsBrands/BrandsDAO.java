package module.ringid.sms.smsBrands;

import module.ringid.sms.BaseDAO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import module.ringid.sms.utils.Constants;
import org.apache.log4j.Logger;
import module.ringid.sms.utils.MyAppError;

public class BrandsDAO extends BaseDAO {

    static Logger logger = Logger.getLogger("com.ringid.sms");

    public BrandsDAO() {
    }

    public MyAppError addBrandsInfo(BrandsDTO dto) {
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT brandName FROM smsbrands WHERE brandName ='" + dto.getBrandName() + "';";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                error.setERROR_TYPE(MyAppError.OTHERERROR);
                error.setErrorMessage("Duplicate Contents");
                throw new Exception("Duplicate Contents");
            }
            String query = "INSERT INTO smsbrands(brandName,userName,PASSWORD,apiUrl,apiId,contactEmail,senderId,balence,commercial, smsEnabled, voiceEnabled) VALUES(?,?,?,?,?,?,?,?,?, ?, ?);";
            // create the mysql insert preparedstatement
            ps = db.connection.prepareStatement(query);
            ps.setString(1, dto.getBrandName());
            ps.setString(2, dto.getUserName());
            ps.setString(3, dto.getPassword());
            ps.setString(4, dto.getApiUrl());
            ps.setString(5, dto.getApiId());
            ps.setString(6, dto.getContactEmail());
            ps.setString(7, dto.getSenderId());
            ps.setDouble(8, dto.getBalance());
            ps.setBoolean(9, dto.isCommercial());
            ps.setBoolean(10, dto.isSmsEnabled());
            ps.setBoolean(11, dto.isVoiceEnabled());
            // execute the preparedstatement
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.error("" + e);
        } catch (Exception e) {
            logger.error(e, e);
        } finally {
            close();
        }

        return error;
    }

    public MyAppError addCountriesInfo(Map<String, String> maps) {
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String query = "INSERT INTO country (countryName,countryCode)        \n"
                    + "                            SELECT * FROM (SELECT ?,?) AS tmp                       \n"
                    + "                            WHERE NOT EXISTS (                                          \n"
                    + "                           SELECT countryName,countryCode FROM country WHERE countryName = ? AND  countryCode = ? \n"
                    + "                            ) LIMIT 1;";
            // create the mysql insert preparedstatement
            ps = db.connection.prepareStatement(query);
            int i = 1;
            for (Map.Entry<String, String> entry : maps.entrySet()) {
                ps.setString(1, entry.getKey());
                ps.setString(2, entry.getValue());
                ps.setString(3, entry.getKey());
                ps.setString(4, entry.getValue());
                ps.addBatch();
                i++;
                if ((i + 1) % 1000 == 0) {
                    ps.executeBatch(); // Execute every 1000 items.
                }
            }
            // execute the preparedstatement
            ps.executeBatch();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.error("" + e);
        } catch (Exception e) {
            logger.error(e, e);
        } finally {
            close();
        }
        return error;
    }

    public ArrayList<BrandsDTO> getBrandsInfo() {
        Map<Integer, BrandsDTO> brandInfMap = new HashMap<>();
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id,brandName,userName,PASSWORD,apiUrl,balance_check_url,apiId,contactEmail,senderId,brandMask,balence,commercial, smsEnabled, voiceEnabled FROM smsbrands ORDER BY brandName;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                try {
                    BrandsDTO brandDTO = new BrandsDTO();
                    brandDTO.setId(rs.getInt("id"));
                    brandDTO.setBrandName(rs.getString("brandName"));
                    brandDTO.setUserName(rs.getString("userName"));
                    brandDTO.setPassword(rs.getString("PASSWORD"));
                    brandDTO.setApiUrl(rs.getString("apiUrl"));
                    brandDTO.setBalanceCheckUrl(rs.getString("balance_check_url"));
                    brandDTO.setApiId(rs.getString("apiId"));
                    brandDTO.setContactEmail(rs.getString("contactEmail"));
                    brandDTO.setSenderId(rs.getString("senderId"));
                    brandDTO.setMask(rs.getString("brandMask"));
                    brandDTO.setBalance(rs.getDouble("balence"));
                    brandDTO.setCommercial(rs.getBoolean("commercial"));
                    brandDTO.setSmsEnabled(rs.getBoolean("smsEnabled"));
                    brandDTO.setVoiceEnabled(rs.getBoolean("voiceEnabled"));
                    if (brandDTO.isSmsEnabled() == true && brandDTO.isVoiceEnabled() == true) {
                        brandDTO.setBrandsType("both");
                    } else if (brandDTO.isSmsEnabled() == true) {
                        brandDTO.setBrandsType("sms");
                    } else if (brandDTO.isVoiceEnabled() == true) {
                        brandDTO.setBrandsType("voice");
                    }
                    switch (brandDTO.getId()) {
                        case Constants.MOBI_REACH_EX_ROBI:
                        case Constants.GP:
                            brandDTO.setDoLoadBalance(true);
                            break;
                    }
                    brandInfMap.put(brandDTO.getId(), brandDTO);
                } catch (SQLException e) {
                    logger.error(e, e);
                }
            }
        } catch (Exception e) {
            logger.error(e, e);
            return null;
        } finally {
            close();
        }
        return new ArrayList<>(brandInfMap.values());
    }

    public MyAppError updateBrand(BrandsDTO dto) {
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE smsbrands SET brandName = ?,userName = ?,PASSWORD = ?,apiUrl = ?,apiId=?,contactEmail = ?,senderId=?,balence=?,commercial=?, smsEnabled=?, voiceEnabled=? WHERE id =?;";
            ps = db.connection.prepareStatement(query);
            ps.setString(1, dto.getBrandName());
            ps.setString(2, dto.getUserName());
            ps.setString(3, dto.getPassword());
            ps.setString(4, dto.getApiUrl());
            ps.setString(5, dto.getApiId());
            ps.setString(6, dto.getContactEmail());
            ps.setString(7, dto.getSenderId());
            ps.setDouble(8, dto.getBalance());
            ps.setBoolean(9, dto.isCommercial());
            ps.setBoolean(10, dto.isSmsEnabled());
            ps.setBoolean(11, dto.isVoiceEnabled());
            ps.setInt(12, dto.getId());
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.error(e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            logger.error(e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError deleteBrand(int id) {
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "";
            sql = "DELETE FROM smsbrands WHERE id = " + id + "";
            int a = stmt.executeUpdate(sql);
            if (a > 0) {
                logger.debug("delete success");
            }
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.error(e);
        } catch (Exception e) {
        } finally {
            close();
        }
        return error;
    }

    public MyAppError updateBrandBalance(BrandsDTO dto) {
        MyAppError error = new MyAppError();
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE smsbrands SET balence=? WHERE id =?;";
            ps = db.connection.prepareStatement(query);
            ps.setDouble(1, dto.getBalance());
            ps.setInt(2, dto.getId());
            ps.execute();
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.error(e);
        } catch (Exception e) {
        } finally {
            close();
        }
        return error;
    }

    public BrandsDTO getBrandInfoById(int id) {
        BrandsDTO brandDTO = new BrandsDTO();
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql;
            sql = "SELECT id,brandName,userName,PASSWORD,apiUrl,apiId,contactEmail,senderId,brandMask,balence,commercial, smsEnabled, voiceEnabled FROM smsbrands  WHERE id =" + id + ";";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                try {
                    brandDTO.setId(rs.getInt("id"));
                    brandDTO.setBrandName(rs.getString("brandName"));
                    brandDTO.setUserName(rs.getString("userName"));
                    brandDTO.setPassword(rs.getString("PASSWORD"));
                    brandDTO.setApiUrl(rs.getString("apiUrl"));
                    brandDTO.setApiId(rs.getString("apiId"));
                    brandDTO.setContactEmail(rs.getString("contactEmail"));
                    brandDTO.setSenderId(rs.getString("senderId"));
                    brandDTO.setMask(rs.getString("brandMask"));
                    brandDTO.setBalance(rs.getDouble("balence"));
                    brandDTO.setCommercial(rs.getBoolean("commercial"));
                    brandDTO.setSmsEnabled(rs.getBoolean("smsEnabled"));
                    brandDTO.setVoiceEnabled(rs.getBoolean("voiceEnabled"));
                } catch (Exception e) {
                }
            }
        } catch (Exception e) {
            return null;
        } finally {
            close();
        }
        return brandDTO;
    }
}
