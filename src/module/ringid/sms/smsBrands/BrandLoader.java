package module.ringid.sms.smsBrands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import module.ringid.sms.utils.Constants;

public class BrandLoader {

    static Logger logger = Logger.getLogger("com.ringid.sms");
    private static final long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    static BrandLoader brandLoader = null;
    private HashMap<Integer, BrandsDTO> brandsHashMapList = new HashMap<>();
    private ArrayList<BrandsDTO> list;

    public BrandLoader() {
    }

    public static BrandLoader getInstance() {
        if (brandLoader == null) {
            createLoader();
        }
        return brandLoader;
    }

    private synchronized static void createLoader() {
        if (brandLoader == null) {
            brandLoader = new BrandLoader();
        }
    }

    public boolean getsetdata() {
        try {
            brandsHashMapList = new HashMap<>();
            BrandsDTO bdto = new BrandsDTO();
            BrandsDAO brandsDAO = new BrandsDAO();
            list = brandsDAO.getBrandsInfo();
            for (BrandsDTO next : list) {
                bdto.setId(next.getId());
                brandsHashMapList.put(next.getId(), next);
            }
            logger.debug("size " + brandsHashMapList.size());
            return true;
        } catch (Exception e) {
            logger.debug("Exception brandLoader", e);
            return false;
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            getsetdata();
        }
    }

    public synchronized ArrayList<BrandsDTO> getBrandList() {
        checkForReload();
        ArrayList<BrandsDTO> data = new ArrayList<>();
        Set set = brandsHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            BrandsDTO dto = (BrandsDTO) me.getValue();
            data.add(dto);
        }
        Collections.sort(data, new BrandsDTO.CompIdDSC());
        logger.debug("Data" + data.size());
        return data;
    }

    public synchronized ArrayList<BrandsDTO> getBrandNameAndIdList() {
        checkForReload();
        ArrayList<BrandsDTO> data = new ArrayList<>();
        Set set = brandsHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            BrandsDTO dto = (BrandsDTO) me.getValue();
            data.add(dto);
        }
        Collections.sort(data, new BrandsDTO.CompIdDSC());
        logger.debug("Data" + data.size());
        return data;
    }

    public synchronized HashMap<Integer, String> getBrandNameMap() {
        checkForReload();
        HashMap<Integer, String> data = new HashMap<>();
        brandsHashMapList.entrySet().forEach((entry) -> {
            data.put(entry.getKey(), entry.getValue().getBrandName());
        });
        return data;
    }

    public synchronized String getBrandNameById(int id) {
        checkForReload();
        String brandName = "";
        Set set = brandsHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            BrandsDTO dto = (BrandsDTO) me.getValue();
            if (dto.getId() == id) {
                brandName = dto.getBrandName();
                return brandName;
            }
        }
        return brandName;
    }

    public synchronized BrandsDTO getBrandDTO(int id) {
        checkForReload();
        if (brandsHashMapList.containsKey(id)) {
            return brandsHashMapList.get(id);
        }
        return null;
    }

    public synchronized int getBrandIdByName(String name) {
        checkForReload();
        int brandId = 0;
        Set set = brandsHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            BrandsDTO dto = (BrandsDTO) me.getValue();
            if (dto.getBrandName() == null ? name == null : dto.getBrandName().equals(name)) {
                brandId = dto.getId();
                return brandId;
            }
        }
        return brandId;
    }

    public synchronized boolean forceReload() {
        loadingTime = System.currentTimeMillis();
        return getsetdata();
    }

    ArrayList<BrandsDTO> getBrandList(int column, int sort, String brandsType) {
        checkForReload();
        ArrayList<BrandsDTO> data = new ArrayList<>();
        Set set = brandsHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            BrandsDTO dto = (BrandsDTO) me.getValue();
            if (brandsType.equals("both")) {
                data.add(dto);
            } else if (brandsType.equals("sms") && dto.isSmsEnabled() == true) {
                data.add(dto);
            } else if (brandsType.equals("voice") && dto.isVoiceEnabled() == true) {
                data.add(dto);
            }
        }
        switch (column) {
            case Constants.COLUMN_ONE:
                Collections.sort(data, (sort == Constants.ASC_SORT ? new BrandsDTO.CompBrandNameASC() : new BrandsDTO.CompBrandNameDSC()));
                break;
            case Constants.COLUMN_FIVE:
                Collections.sort(data, (sort == Constants.ASC_SORT ? new BrandsDTO.CompCommercialASC() : new BrandsDTO.CompCommercialDSC()));
                break;
            default:
                Collections.sort(data, new BrandsDTO.CompIdDSC());
                break;
        }
        logger.debug("Data" + data.size());
        return data;
    }

    ArrayList<BrandsDTO> getBrandList(int column, int sort, boolean type, String brandsType) {
        checkForReload();
        ArrayList<BrandsDTO> data = new ArrayList<>();
        Set set = brandsHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            BrandsDTO dto = (BrandsDTO) me.getValue();
            if (dto.isCommercial() == type && brandsType.equals("both")) {
                data.add(dto);
            } else if (dto.isCommercial() == type && brandsType.equals("sms") && dto.isSmsEnabled() == true) {
                data.add(dto);
            } else if (dto.isCommercial() == type && brandsType.equals("voice") && dto.isVoiceEnabled() == true) {
                data.add(dto);
            }
        }
        switch (column) {
            case Constants.COLUMN_ONE:
                Collections.sort(data, (sort == Constants.ASC_SORT ? new BrandsDTO.CompBrandNameASC() : new BrandsDTO.CompBrandNameDSC()));
                break;
            case Constants.COLUMN_FIVE:
                Collections.sort(data, (sort == Constants.ASC_SORT ? new BrandsDTO.CompCommercialASC() : new BrandsDTO.CompCommercialDSC()));
                break;
            default:
                Collections.sort(data, new BrandsDTO.CompIdDSC());
                break;
        }
        logger.debug("Data" + data.size());
        return data;
    }
}
