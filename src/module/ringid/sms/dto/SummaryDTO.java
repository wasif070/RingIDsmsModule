/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package module.ringid.sms.dto;

/**
 *
 * @author reefat
 */
public class SummaryDTO {

    private String countryName;
    private String routesName;
    private int totalVerified;
    private int totalConfirmedByOperator;
    private int totalFailedByOperator;
    private int totalSms;
    private float asr;

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getRoutesName() {
        return routesName;
    }

    public void setRoutesName(String routesName) {
        this.routesName = routesName;
    }

    public int getTotalVerified() {
        return totalVerified;
    }

    public void setTotalVerified(int totalVerified) {
        this.totalVerified = totalVerified;
    }

    public int getTotalConfirmedByOperator() {
        return totalConfirmedByOperator;
    }

    public void setTotalConfirmedByOperator(int totalConfirmedByOperator) {
        this.totalConfirmedByOperator = totalConfirmedByOperator;
    }

    public int getTotalFailedByOperator() {
        return totalFailedByOperator;
    }

    public void setTotalFailedByOperator(int totalFailedByOperator) {
        this.totalFailedByOperator = totalFailedByOperator;
    }

    public int getTotalSms() {
        return totalSms;
    }

    public void setTotalSms(int totalSms) {
        this.totalSms = totalSms;
    }

    public void setAsr(float asr) {
        this.asr = asr;
    }
}
