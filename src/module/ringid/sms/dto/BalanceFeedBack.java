package module.ringid.sms.dto;

public class BalanceFeedBack {

    private double balance = 0.0;
    private int reasonCode;

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public int getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(int reasonCode) {
        this.reasonCode = reasonCode;
    }
}
