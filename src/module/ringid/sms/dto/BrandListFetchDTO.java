
package module.ringid.sms.dto;

import java.util.ArrayList;
import java.util.HashMap;

public class BrandListFetchDTO {

    private HashMap<Integer, ArrayList<Integer>> map;
    private int count;

    public HashMap<Integer, ArrayList<Integer>> getMap() {
        return map;
    }

    public void setMap(HashMap<Integer, ArrayList<Integer>> map) {
        this.map = map;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
