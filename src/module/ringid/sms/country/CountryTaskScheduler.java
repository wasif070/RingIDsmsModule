package module.ringid.sms.country;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import module.ringid.sms.utils.MyAppError;

public class CountryTaskScheduler {

    private static CountryTaskScheduler scheduler = null;

    public static CountryTaskScheduler getInstance() {
        if (scheduler == null) {
            scheduler = new CountryTaskScheduler();
        }
        return scheduler;
    }

    public MyAppError addCountiesInfo(CountryDTO p_dto) {
        CountryDAO countryDAO = new CountryDAO();
        return countryDAO.addCountriesInfo(p_dto);
    }

    public MyAppError addCountriesInfo(Map<Integer, CountryDTO> maps) {
        CountryDAO countryDAO = new CountryDAO();
        return countryDAO.addCountriesInfo(maps);
    }

    public MyAppError updateCountries(CountryDTO p_dto) {
        CountryDAO countryDAO = new CountryDAO();
        return countryDAO.updateCountry(p_dto);
    }

    public MyAppError deleteCountry(int id) {
        CountryDAO countryDAO = new CountryDAO();
        return countryDAO.deleteCountry(id);
    }

    public CountryDTO getCountryInfoById(int id) {
        return CountryLoader.getInstance().getCountryDTO(id);
    }
    public CountryDTO getCountryInfoByCode(String countryCode) {
        return CountryLoader.getInstance().getCountryDTO(countryCode);
    }

    public ArrayList<CountryDTO> getCountryList(CountryDTO dto) {
        return CountryLoader.getInstance().getCountryList(dto);
    }

    public ArrayList<CountryDTO> getCountryListForDownload() {
        return CountryLoader.getInstance().getCountryListForDownload();
    }

    public int getCountryId(String countryName, String countryCode) {
        return CountryLoader.getInstance().getCountryId(countryName, countryCode);
    }

    public int getCountryId(String countryCode) {
        return CountryLoader.getInstance().getCountryId(countryCode);
    }
    
    public int getCountryNameAndId(String countryCode) {
        return CountryLoader.getInstance().getCountryNameAndId(countryCode);
    }

    public ArrayList<CountryDTO> getCountryCodeList() {
        return CountryLoader.getInstance().getCountryCodeList();
    }

    public ArrayList<CountryDTO> getCountriesList(int columnVal, int sortType, CountryDTO dto) {
        return CountryLoader.getInstance().getCountryList(columnVal, sortType, dto);
    }

    public synchronized CountryDTO getCountryDTO(int id) {
        return CountryLoader.getInstance().getCountryDTO(id);
    }

    public HashMap<Integer, String> getCountryNameMap() {
        return CountryLoader.getInstance().getCountryNameMap();
    }
}
