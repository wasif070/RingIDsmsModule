/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package module.ringid.sms.country;

import module.ringid.sms.BaseDAO;
import module.ringid.sms.utils.MyAppError;
import java.sql.SQLException;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author Rejuan
 */
public class CountryDAO extends BaseDAO {

    static Logger logger = Logger.getLogger("com.ringid.sms");

    public CountryDAO() {
    }

    public MyAppError addCountriesInfo(CountryDTO dto) {
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "";
            sql = "SELECT countryCode FROM country WHERE countryCode ='" + dto.getCode() + "';";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                error.setERROR_TYPE(MyAppError.OTHERERROR);
                error.setErrorMessage("Country code is already exists");
                throw new Exception("Duplicate Contents");
            }
            String query = "INSERT INTO country(countrySymbol,countryName,countryCode) VALUES(?,?,?);";
            ps = db.connection.prepareStatement(query);   // create the mysql insert preparedstatement
            ps.setString(1, dto.getCountrySymbol());
            ps.setString(2, dto.getName());
            ps.setString(3, dto.getCode());
            ps.execute();// execute the preparedstatement
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("" + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            logger.error("Error in addCountriesInfo --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError addCountriesInfo(Map<Integer, CountryDTO> maps) {
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            String query = "INSERT INTO country (countrySymbol,countryName,countryCode)                         \n"
                    + "                            SELECT * FROM (SELECT ?,?,?) AS tmp                          \n"
                    + "                            WHERE NOT EXISTS (                                           \n"
                    + "                           SELECT countryName,countryCode FROM country WHERE countryName = ? AND  countryCode = ? OR countryCode =? \n"
                    + "                            ) LIMIT 1;";
            ps = db.connection.prepareStatement(query);  // create the mysql insert preparedstatement  
            int i = 1;
            for (Map.Entry<Integer, CountryDTO> entry : maps.entrySet()) {
                ps.setString(1, entry.getValue().getCountrySymbol());
                ps.setString(2, entry.getValue().getName());
                ps.setString(3, entry.getValue().getCode());
                ps.setString(4, entry.getValue().getName());
                ps.setString(5, entry.getValue().getCode());
                ps.setString(5, entry.getValue().getCode());
                ps.addBatch();
                i++;
                if ((i + 1) % 1000 == 0) {
                    ps.executeBatch(); // Execute every 1000 items.
                }
            }
            ps.executeBatch(); // execute the preparedstatement
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("" + e);
        } catch (Exception e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            logger.error("Error in addCountriesInfo --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError updateCountry(CountryDTO dto) {
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            String query = "UPDATE country SET countrySymbol=?,countryName = ?,countryCode = ? WHERE id = ?;";
            ps = db.connection.prepareStatement(query); // create the mysql insert preparedstatement
            ps.setString(1, dto.getCountrySymbol());
            ps.setString(2, dto.getName());
            ps.setString(3, dto.getCode());
            ps.setInt(4, dto.getId());
            ps.execute(); // execute the preparedstatement
        } catch (SQLException e) {
            error.setERROR_TYPE(MyAppError.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("" + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            logger.error("Error in updateCountry --> " + e);
        } finally {
            close();
        }
        return error;
    }

    public MyAppError deleteCountry(int id) {
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "DELETE FROM country WHERE id = " + id;
            int a = stmt.executeUpdate(sql);
            if (a > 0) {
                logger.debug("delete success");
            }
        } catch (SQLException e) {
            error.setERROR_TYPE(error.DBERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("" + e);
        } catch (Exception e) {
            error.setERROR_TYPE(error.DBERROR);
            logger.error("Error in updateCountry --> " + e);
        } finally {
            close();
        }
        return error;
    }

}
