package module.ringid.sms.country;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CountryDTO {

    private int id;
    private String name;
    private String code;
    private String countrySymbol;
    private ArrayList<CountryDTO> countriesList = null;
    private List<Integer> catIds;
    private Boolean searchByName = false;

    public boolean isSearchByName() {
        return searchByName;
    }

    public void setSearchByName(boolean searchByName) {
        this.searchByName = searchByName;
    }

    public ArrayList<CountryDTO> getCountriesList() {
        return countriesList;
    }

    public void setCountriesList(ArrayList<CountryDTO> countriesList) {
        this.countriesList = countriesList;
    }

    public String getCountrySymbol() {
        return countrySymbol;
    }

    public void setCountrySymbol(String countrySymbol) {
        this.countrySymbol = countrySymbol;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Integer> getCategoryList() {
        return catIds;
    }

    public void setCategoryList(List<Integer> categoryList) {
        this.catIds = categoryList;
    }

    public static class CompIdDSC implements Comparator<CountryDTO> {

        @Override
        public int compare(CountryDTO arg0, CountryDTO arg1) {
            return arg1.id - arg0.id;
        }
    }

    public static class CompIdASC implements Comparator<CountryDTO> {

        @Override
        public int compare(CountryDTO arg0, CountryDTO arg1) {
            return arg0.id - arg1.id;
        }
    }
}
