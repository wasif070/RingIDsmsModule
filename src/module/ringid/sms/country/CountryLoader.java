package module.ringid.sms.country;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import module.ringid.sms.utils.Constants;
import module.ringid.sms.utils.MyAppError;

public class CountryLoader {

    private final static long LOADING_INTERVAL = 60 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Integer, CountryDTO> countryHashMapList;
    private HashMap<String, CountryDTO> countryMap;

    private static CountryLoader countryLoader = null;
    static Logger logger = Logger.getLogger("com.ringid.sms");

    public CountryLoader() {
    }

    public static CountryLoader getInstance() {
        if (countryLoader == null) {
            createLoader();
        }
        return countryLoader;
    }

    private synchronized static void createLoader() {
        if (countryLoader == null) {
            countryLoader = new CountryLoader();
        }
    }

    private void getsetdata() {
        countryHashMapList = new HashMap<>();
        countryMap = new HashMap<>();
        utilsdbconnector.DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            db = utilsdbconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "SELECT id,countrySymbol,countryName,countryCode FROM country;";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                CountryDTO dto = new CountryDTO();
                dto.setId(rs.getInt("id"));
                dto.setCountrySymbol(rs.getString("countrySymbol"));
                dto.setName(rs.getString("countryName"));
                dto.setCode(rs.getString("countryCode"));
                countryHashMapList.put(dto.getId(), dto);
                countryMap.put(dto.getCode(), dto);
            }
            logger.debug(sql + "::" + countryHashMapList.size());
        } catch (Exception e) {
            logger.debug("Country List Exception...", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db != null) {
                    utilsdbconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            getsetdata();
        }
    }

    public synchronized ArrayList<CountryDTO> getCountryList(CountryDTO sdto) {
        checkForReload();
        ArrayList<CountryDTO> data = new ArrayList<CountryDTO>();
        Set set = countryHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            CountryDTO dto = (CountryDTO) me.getValue();
            if ((sdto.isSearchByName() && !dto.getName().contains(sdto.getName()))) {
                continue;
            }
            data.add(dto);
        }
        Collections.sort(data, new CountryDTO.CompIdDSC());
        logger.debug("Data" + data.size());
        return data;
    }

    public synchronized ArrayList<CountryDTO> getCountryList(int coloumValue, int sortType, CountryDTO sdto) {
        checkForReload();
        ArrayList<CountryDTO> data = new ArrayList<>();
        Set set = countryHashMapList.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            CountryDTO dto = (CountryDTO) me.getValue();
            if ((sdto.isSearchByName() && !dto.getName().contains(sdto.getName()))) {
                continue;
            }
            data.add(dto);
        }
        switch (coloumValue) {
            case Constants.COLUMN_ONE:
                if (sortType == Constants.ASC_SORT) {
                    Collections.sort(data, new CountryDTO.CompIdASC());
                } else {
                    Collections.sort(data, new CountryDTO.CompIdDSC());
                }
                break;
            default:
                Collections.sort(data, new CountryDTO.CompIdDSC());
                return data;
        }
        logger.debug("Data" + data.size());
        return data;
    }

    public synchronized ArrayList<CountryDTO> getCountryListForDownload() {
        checkForReload();
        ArrayList<CountryDTO> data = new ArrayList<CountryDTO>(countryHashMapList.values());
        return data;
    }

    public synchronized int getCountryNameAndId(String countryCode) {
        checkForReload();
        int id = 0;
        try {
            for (Map.Entry<Integer, CountryDTO> entrySet : countryHashMapList.entrySet()) {
                CountryDTO value = entrySet.getValue();
                if (value.getCode().equals(countryCode)) {
                    id = value.getId();
                    return id;
                }
            }
        } catch (Exception e) {
        }
        return id;
    }

    public synchronized int getCountryId(String countryCode) {
        checkForReload();
        int id = 0;
        try {
            for (Map.Entry<Integer, CountryDTO> entry : countryHashMapList.entrySet()) {
                CountryDTO value = entry.getValue();
                if (value.getCode().equals(countryCode)) {
                    id = value.getId();
                    return id;
                }
            }
        } catch (Exception e) {
            logger.error("Error in getCountryNameAndId(String countryCode) --> " + e);
        }
        return id;
    }

    public synchronized int getCountryId(String countryName, String countryCode) {
        checkForReload();
        int id = 0;
        for (Map.Entry<Integer, CountryDTO> entrySet : countryHashMapList.entrySet()) {
            Integer key = entrySet.getKey();
            CountryDTO value = entrySet.getValue();
            if ((value.getName().contains(countryName) && value.getCode().contains(countryCode)) || value.getCode().contains(countryCode)) {
                logger.debug("key : " + key);
                return id = key;
            }
        }
        int index = 1;
        if (id == 0) {
            Map<Integer, CountryDTO> maps = new HashMap<>();
            CountryDTO coutryCountryDTO = new CountryDTO();
            coutryCountryDTO.setCountrySymbol("");
            coutryCountryDTO.setCode(countryCode);
            coutryCountryDTO.setName(countryName);
            maps.put(index, coutryCountryDTO);
            MyAppError error = CountryTaskScheduler.getInstance().addCountriesInfo(maps);
            if (error.getERROR_TYPE() == 0) {
                getCountryId(countryName, countryCode);
            }
        }
        return id;
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        getsetdata();
    }

    public CountryDTO getCountryDTO(String countryCode) {
        checkForReload();
        CountryDTO countryDTO = null;
        try {
            for (Map.Entry<Integer, CountryDTO> entry : countryHashMapList.entrySet()) {
                CountryDTO value = entry.getValue();
                if (value.getCode().equals(countryCode)) {
                    return value;
                }
            }
        } catch (Exception e) {
            logger.error("Error in getCountryDTO(String countryCode) --> " + e);
        }
        return countryDTO;
    }

    public synchronized String getCountrySymbol(String mobilePhoneDialingCode) {
        checkForReload();
        if (countryMap.containsKey(mobilePhoneDialingCode)) {
            return countryMap.get(mobilePhoneDialingCode).getCountrySymbol();
        }
        return null;
    }

    public synchronized CountryDTO getCountryDTO(int id) {
        checkForReload();
        if (countryHashMapList.containsKey(id)) {
            return countryHashMapList.get(id);
        }
        return null;
    }

    public synchronized ArrayList<CountryDTO> getCountryCodeList() {
        checkForReload();
        ArrayList<CountryDTO> cCodeList = new ArrayList<>();
        for (Map.Entry<Integer, CountryDTO> entrySet : countryHashMapList.entrySet()) {
            CountryDTO cdto = new CountryDTO();
            CountryDTO value = entrySet.getValue();
            if (value.getName().equals("")) {
                continue;
            }
            cdto.setId(value.getId());
            cdto.setCode(value.getCode());
            cdto.setName(value.getName());
            cCodeList.add(cdto);
        }
        return cCodeList;
    }

    HashMap<Integer, String> getCountryNameMap() {
        checkForReload();
        HashMap<Integer, String> data = new HashMap<>();
        countryHashMapList.entrySet().forEach((entry) -> {
            data.put(entry.getKey(), entry.getValue().getName());
        });
        return data;
    }
}
