package module.ringid.sms.utils;

import java.io.StringReader;
import java.security.SecureRandom;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class Utils {

    public static String base64ToImage(String arg) {
        String userImage = arg;
        userImage = userImage.replaceAll("-", "+");
        userImage = userImage.replaceAll("_", "/");
        switch (userImage.length() % 4) {
            case 0:
                break;
            case 2:
                userImage += "==";
                break;
            case 3:
                userImage += "=";
                break;
        }
        return userImage;
    }

    public static String getRandomPacketID(String userIdentity) {
        SecureRandom random = new SecureRandom();
        char[] chars = new char[8];
        for (int i = 0; i < chars.length; i++) {
            int v = random.nextInt(10 + 26 + 26);
            char c;
            if (v < 10) {
                c = (char) ('0' + v);
            } else if (v < 36) {
                c = (char) ('a' - 10 + v);
            } else {
                c = (char) ('A' - 36 + v);
            }
            chars[i] = c;
        }

        String key = new String(chars);
        if (userIdentity != null) {
            key = key + userIdentity;
        }
        return key;
    }

    public static String traduceUtf8(String _normal) {
        if (_normal != null) {
            try {
                _normal = new String(_normal.getBytes(), "UTF-8");
            } catch (java.io.UnsupportedEncodingException e) {
                //System.err.println(e);
            }
        }
        return _normal;
    }

    public static byte[] makeReponse(int status, String uploadUrl) {
        byte[] uploadUrlByte = uploadUrl.getBytes();
        int totalDataLenght = 2 + uploadUrlByte.length;
        byte[] data = new byte[totalDataLenght];
        int i = 0;
        data[i++] = (byte) status;
        data[i++] = (byte) uploadUrlByte.length;
        for (int n = 0; n < uploadUrlByte.length; n++) {
            data[i++] = uploadUrlByte[n];
        }
        return data;
    }
    
    public static Document convertStringToDocument(String xmlStr) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(xmlStr)));
            return doc;
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return null;
    }
}
