/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package module.ringid.sms.utils;

/**
 *
 * @author RingID Inc.
 */
public class Constants {

    public final static int ASC_SORT = 0;
    public final static int COLUMN_ONE = 1;
    public final static int COLUMN_FIVE = 5;

    public static final String FAILED = "failed";
    public static final String SUCCESS = "success";

    public static final int VERIFIED = 1;
    public static final int DELIVERED = 2;

    public static final int NOT_VERIFIED = 1;
    public static final int UN_DELIVERED = 2;
    public static final int BOTH = 3;

    public static final short CLICKATELL_API = 1;
    public static final short SMS_ROUTER = 2;
    public static final short TEXT_LOCAL = 3;
    public static final short SMS_CELLENT = 4;
    public static final short MOBI_SMS = 5;
    public static final short CS_NETWORKS = 6;
    public static final short INFOBIP_SMS = 7;
    public static final short ROBIAXIATA_SMS = 8;
    public static final short CLICKN_GO_BD = 9;
    public static final short WAVECELL = 10;
    public static final short PLIVO_API = 11;
    public static final short SMS_WAALA_API = 12;
    public static final short ACL = 13;
    public static final short SMS_COUNTRY = 14;
    public static final short NEXMO = 15;
    public static final short GP = 16;
    public static final short TWILIO = 17;
    public static final int MOBI_REACH_EX_ROBI = 18;
    public static final int ONNO_ROKOM = 19;

    public static final int[] ATTEMPT_TO_BRANDID_INDEX_MAP = {0, 1, 1, 2, 3};

    public static final int ONE_MINUTE = 60000;
    public static final int TWO_MINUTE = 2 * ONE_MINUTE;
    public static final int FIVE_MINUTE = 5 * ONE_MINUTE;
    public static final int THIRTY_MINUTE = 30 * ONE_MINUTE;
    public static final long ONE_HOUR = 60 * ONE_MINUTE;
    public static final long ONE_DAY = 24 * ONE_HOUR;
    public static final long THREE_DAY = 3 * ONE_DAY;
}
