package module.ringid.sms.utils;

import java.util.Comparator;

public class BrandsDTO {

    private int id;
    private String brandName;
    private String userName;
    private String password;
    private String apiUrl;
    private String apiId;
    private String contactEmail;
    private String senderId;
    private int brandId;
    private double balance;
    private boolean commercial;

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public boolean isCommercial() {
        return commercial;
    }

    public void setCommercial(boolean commercial) {
        this.commercial = commercial;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public static class CompIdDSC implements Comparator<BrandsDTO> {

        @Override
        public int compare(BrandsDTO arg0, BrandsDTO arg1) {
            return arg1.id - arg0.id;
        }
    }

    public static class CompIdASC implements Comparator<BrandsDTO> {

        @Override
        public int compare(BrandsDTO arg0, BrandsDTO arg1) {
            return arg0.id - arg1.id;
        }
    }

}
