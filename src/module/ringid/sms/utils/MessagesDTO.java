package module.ringid.sms.utils;

import java.util.ArrayList;
import java.util.Comparator;

public class MessagesDTO {

    private int id;
    private int routesId;
    private String routesName;
    private String destination;
    private String message;
    private String sms;
    private String brandResponse;
    private String smsStatus;
    private String fromDate;
    private String toDate;
    private ArrayList<MessagesDTO> responseList = new ArrayList<MessagesDTO>();
    private boolean searchByNumber = false;
    private boolean searchByCountryId = false;
    private boolean searchByRoutesId = false;
    private boolean searchByStatus = false;
    private boolean serachByFromDate = false;
    private boolean serachByToDate = false;
    private String date;
    private String receivedDate;
    private int nSuccess;
    private int nFailed;
    private String vCode;
    private String responseId;
    private int verifiedSend;
    private String countryName;
    private int countryId;
    private int dlStatus;

    public MessagesDTO() {
        this.dlStatus = 0;
    }

    public int getDlStatus() {
        return dlStatus;
    }

    public void setDlStatus(int dlStatus) {
        this.dlStatus = dlStatus;
    }

    public String getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(String receivedDate) {
        this.receivedDate = receivedDate;
    }

    public boolean isSearchByCountryId() {
        return searchByCountryId;
    }

    public void setSearchByCountryId(boolean searchByCountryId) {
        this.searchByCountryId = searchByCountryId;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getResponseId() {
        return responseId;
    }

    public void setResponseId(String responseId) {
        this.responseId = responseId;
    }

    public int getVerifiedSend() {
        return verifiedSend;
    }

    public void setVerifiedSend(int verifiedSend) {
        this.verifiedSend = verifiedSend;
    }

    public String getVarificationCode() {
        return vCode;
    }

    public void setVarificationCode(String varificationCode) {
        this.vCode = varificationCode;
    }

    public int getnSuccess() {
        return nSuccess;
    }

    public void setnSuccess(int nSuccess) {
        this.nSuccess = nSuccess;
    }

    public int getnFailed() {
        return nFailed;
    }

    public void setnFailed(int nFailed) {
        this.nFailed = nFailed;
    }

    public boolean isSerachByFromDate() {
        return serachByFromDate;
    }

    public void setSerachByFromDate(boolean serachByFromDate) {
        this.serachByFromDate = serachByFromDate;
    }

    public boolean isSerachByToDate() {
        return serachByToDate;
    }

    public void setSerachByToDate(boolean serachByToDate) {
        this.serachByToDate = serachByToDate;
    }

    public String getSms() {
        return sms;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setSms(String sms) {
        this.sms = sms;
    }

    public String getRoutesName() {
        return routesName;
    }

    public void setRoutesName(String routesName) {
        this.routesName = routesName;
    }

    public boolean isSearchByNumber() {
        return searchByNumber;
    }

    public void setSearchByNumber(boolean searchByNumber) {
        this.searchByNumber = searchByNumber;
    }

    public boolean isSearchByRoutes() {
        return searchByRoutesId;
    }

    public void setSearchByRoutesId(boolean searchByRoutes) {
        this.searchByRoutesId = searchByRoutes;
    }

    public boolean isSearchByStatus() {
        return searchByStatus;
    }

    public void setSearchByStatus(boolean searchByStatus) {
        this.searchByStatus = searchByStatus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBrandId() {
        return routesId;
    }

    public void setBrandId(int brandId) {
        this.routesId = brandId;
    }

    public String getMobileNo() {
        return destination;
    }

    public void setMobileNo(String mobileNo) {
        this.destination = mobileNo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBrandResponse() {
        return brandResponse;
    }

    public void setBrandResponse(String brandResponse) {
        this.brandResponse = brandResponse;
    }

    public String getSmsStatus() {
        return smsStatus;
    }

    public void setSmsStatus(String smsStatus) {
        this.smsStatus = smsStatus;
    }

    public ArrayList<MessagesDTO> getResponseList() {
        return responseList;
    }

    public void setResponseList(ArrayList<MessagesDTO> responseList) {
        this.responseList = responseList;
    }

    public static class CompIdDSC implements Comparator<MessagesDTO> {

        @Override
        public int compare(MessagesDTO arg0, MessagesDTO arg1) {
            return arg1.id - arg0.id;
        }
    }

    public static class CompIdASC implements Comparator<MessagesDTO> {

        @Override
        public int compare(MessagesDTO arg0, MessagesDTO arg1) {
            return arg0.id - arg1.id;
        }
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }
}
